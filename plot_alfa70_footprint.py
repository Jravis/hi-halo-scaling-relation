from __future__ import print_function, division
import numpy as np
import sys
import time
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec



mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rc('font', weight='bold')
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Ubuntu'
mpl.rcParams['font.monospace'] = 'Ubuntu Mono'
mpl.rcParams['axes.labelweight'] = 'bold'

mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'

import palettable

#set some plotting parameters
plt.style.use("classic")


from matplotlib.patches import Rectangle 

ra  = np.genfromtxt('/mnt/data2/saili/guovol_alfa70/code1_alfa70.dat', usecols=1)
dec = np.genfromtxt('/mnt/data2/saili/guovol_alfa70/code1_alfa70.dat', usecols=2)

ind = (ra<=250)*(ra>=100)
ra_NGC = ra[ind] 
dec_NGC = dec[ind] 



ind = (ra>=0)*(ra<=60)
ra_SGC1 = ra[ind]
dec_SGC1 = dec[ind]

ind = (ra>=250)*(ra<=360)
ra_SGC2 = ra[ind]-360
dec_SGC2 = dec[ind]






fig= plt.figure(1, figsize=(10, 4.5))

gs = gridspec.GridSpec(1, 2, wspace=0.35)#, hspace=0.000)
ax1 = plt.subplot(gs[0, 0])

ax1.plot(ra_NGC, dec_NGC, linestyle='', lw=0, marker='.', ms=2, color='k', label=r"")
ax1.text(110, 35, r"$\rm NGC$", fontsize=18)

ax1.add_patch( Rectangle((114, 23.8), 138, 6.5, fc ='none',  ec ='r', lw = 2.) ) 

ax1.hlines(y=-0.1, xmin=114. , xmax=248.5 , linestyle ='-' , linewidth=2., color='r') 
ax1.vlines(x=114, ymin=-0.1 , ymax=18.2 , linestyle ='-' , linewidth=2., color='r') 
ax1.vlines(x=248.5, ymin=-0.1 , ymax=17.9 , linestyle ='-' , linewidth=2., color='r') 


ax1.hlines(y=22.3, xmin=140. , xmax=231 , linestyle ='-' , linewidth=2., color='r') 
ax1.hlines(y=17.9, xmin=231 , xmax=248.5 , linestyle ='-' , linewidth=2., color='r') 
ax1.vlines(x=231, ymin=17.9 , ymax=22.3 , linestyle ='-' , linewidth=2., color='r') 

ax1.hlines(y=18.2, xmin=114 , xmax=128.9 , linestyle ='-' , linewidth=2., color='r') 
ax1.vlines(x=140, ymin=20 , ymax=22.3 , linestyle ='-' , linewidth=2., color='r') 
ax1.vlines(x=128.9, ymin=18.2 , ymax=20 , linestyle ='-' , linewidth=2., color='r') 
ax1.hlines(y=20, xmin=128 , xmax=140 , linestyle ='-' , linewidth=2., color='r') 


ax1.set_xlabel(r'$\rm RA$$\rm (degree)$', fontsize=20, fontweight='bold')
ax1.set_ylabel(r'$\rm DEC$$\rm (degree)$', fontsize=20, fontweight='bold')
ax1.set_xlim(100, 280)
ax1.set_ylim(-2, 40)
ax1.set_yticks([0, 10, 20, 30, 40])
ax1.set_xticks([100, 150, 200, 250])
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=18, loc=1, numpoints=1)
ax1.tick_params(axis='both', which='minor', length=5,
      width=2, labelsize=18)
ax1.tick_params(axis='both', which='major', length=8,
       width=2, labelsize=18)
ax1.spines['bottom'].set_linewidth(1.2)
ax1.spines['left'].set_linewidth(1.2)
ax1.spines['top'].set_linewidth(1.2)
ax1.spines['right'].set_linewidth(1.2)

ax1 = plt.subplot(gs[0, 1])

ax1.plot(ra_SGC1, dec_SGC1, linestyle='', lw=0, marker='.', ms=2, color='k', label=r"")
ax1.plot(ra_SGC2, dec_SGC2, linestyle='', lw=0, marker='.', ms=2, color='k', label=r"")


ax1.text(0, 35, r"$\rm N_{gal}^{code1}=18984$", fontsize=18, color='k')
ax1.text(-36, 35, r"$\rm SGC$", fontsize=18)

ax1.add_patch( Rectangle((-30, 21.8), 75.8, 10.2, fc ='none',  ec ='r', lw = 2.) ) 

ax1.add_patch( Rectangle((-30, 13.7) , 75.8, 2.5, fc ='none',  ec ='r', lw = 2.) ) 
ax1.add_patch( Rectangle((-30, 5.8)  , 75.8, 4.5, fc ='none',  ec ='r', lw = 2.) ) 
ax1.add_patch( Rectangle((-30, -0.22), 75.8, 2.4, fc ='none',  ec ='r', lw = 2.) ) 

#ax1.text(125, 35, r"$\rm NGC$", fontsize=18)

ax1.set_xlabel(r'$\rm RA$$\rm (degree)$', fontsize=20, fontweight='bold')
ax1.set_ylabel(r'$\rm DEC$$\rm (degree)$', fontsize=20, fontweight='bold')
ax1.set_xlim(-40, 60)
ax1.set_ylim(-2, 40)
ax1.set_yticks([0, 10, 20, 30, 40])
ax1.set_xticks([-40, -20, 0, 20, 40, 60])
ax1.minorticks_on()
#ax1.legend(frameon=False, ncol=1, fontsize=18, loc=2, numpoints=0)
ax1.tick_params(axis='both', which='minor', length=5,
      width=2, labelsize=18)
ax1.tick_params(axis='both', which='major', length=8,
       width=2, labelsize=18)
ax1.spines['bottom'].set_linewidth(1.2)
ax1.spines['left'].set_linewidth(1.2)
ax1.spines['top'].set_linewidth(1.2)
ax1.spines['right'].set_linewidth(1.2)

fig.savefig("alfa70_footprint.png", bbox_inches='tight', dpi=400, edgecolor='none', frameon='False')


