
#####################################################
#Copyright (c) 2019, Sandeep Rana                   #
# This routine needs Python 3                       #
# This routine is to compute the effect of          #
# scatter into HI-halo abundace matching relation   #
# Following Behroozi 2010 and Ren 2019.             #
#This routine is for creating mock and computing    #
#clustering. Here we explore paramter space         #
#and it's effect on HI clustering with and without  #
#the scatter.                                       #
                                                    #                                                   
#####################################################

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""




from __future__ import print_function, division
import numpy as np
import sys
import os
import time
from scipy import integrate
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
from abundance import fit_abundance_halo, raw_abundance, HI_Mass_func, AM_nonparam, schechter_function
from astropy.cosmology import LambdaCDM
import datatable as dt
import lmfit
import argparse
from Corrfunc.theory.xi import xi as xi_corr
import Corrfunc
from Corrfunc.theory.wp import wp
from multiprocessing import Process
from astropy.io import ascii
import palettable
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from collections import OrderedDict
from matplotlib.transforms import blended_transform_factory



mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rc('font', weight='bold')
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Ubuntu'
mpl.rcParams['font.monospace'] = 'Ubuntu Mono'
mpl.rcParams['axes.labelweight'] = 'bold'

mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'
mpl.rcParams['legend.numpoints'] = 1

#set some plotting parameters
plt.style.use("classic")
lnstyles = OrderedDict(
    [('solid',               (0, ())),
     ('loosely dotted',      (0, (1, 10))),
     ('dotted',              (0, (1, 5))),
     ('densely dotted',      (0, (1, 1))),

     ('loosely dashed',      (0, (5, 10))),
     ('dashed',              (0, (5, 5))),
     ('densely dashed',      (0, (5, 1))),

     ('loosely dashdotted',  (0, (3, 10, 1, 10))),
     ('dashdotted',          (0, (3, 5, 1, 5))),
     ('densely dashdotted',  (0, (3, 1, 1, 1))),

     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))])



#---------------- Global Values ---------------------------
from hmf import MassFunction
from hmf import cosmo

# Multidark 2 cosmology hmf model
OmegaMatter = 0.307115
OmegaLambda = 0.692885
H0 = 0.6777*100.
OmegaBaryon = 0.048206
new_model   = LambdaCDM(H0 = 0.6777*100, Om0 = OmegaMatter, 
			Tcmb0=2.73, Ob0= OmegaBaryon, Ode0=OmegaLambda)

#-----------------------------------------------------------


def wp_corr(x, y, z, mHI_mock, vz, sigma, fname):
   
    cutoff      = np.array([9.4, 9.6, 9.8, 10.0, 10.2])
    cutoff_name = np.array(['9pt4', '9pt6', '9pt8', '10pt0', '10pt2'])
    nthreads= 32
    rmin  = 0.1
    rmax  = 25.11886431509582
    log_delta_r = 0.2
    rbins = 10**np.arange(np.log10(rmin), np.log10(rmax), log_delta_r)

    rbin_mid = np.zeros(12)
    for i in range(len(rbins)-1):
        rbin_mid[i] = (rbins[i]+rbins[i+1])/2. 

    boxsize=400 # h^-1 Mpc
    redshift=0.0
    H_z = new_model.H(redshift).value
    HubbleParam=0.6777
    #SMDPL is in uniths of Mpch^-1
    boost_factor = np.sqrt(1.+ redshift)*1./H_z
    pimax = 20.0
    
    for i in range(len(cutoff)): 
        keep = (mHI_mock>cutoff[i])
        x1 = x[keep]
        y1 = y[keep]
        z1 = z[keep]
        #z_rsd = z[keep]+vz[keep]*boost_factor*HubbleParam
        #z_rsd= z_rsd%boxsize
        weights = np.ones_like(x1)   


        fname1 ="../pipeline_result_data_alfa70_Mvir/clust_data/real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], sigma)
        table = [x1, y1, z1]             
        names = ['X', 'Y', 'Z']        
        Formats = {'X':'%2.14f', 'Y':'%2.14f', 'Z':'%2.14f'}
        ascii.write(table, fname1, names=names, delimiter='\t', formats=Formats, overwrite=True)

    #    results = wp(boxsize, pimax, nthreads, rbins, x1, y1, z_rsd, weights=weights, 
        results = wp(boxsize, pimax, nthreads, rbins, x1, y1, z1, weights=weights, 
                     weight_type='pair_product', verbose=True, output_rpavg=True)
                     
        f = open(fname+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], sigma),"w")
        f.write("#rbin\trpavg\twp\tnpairs\tweightavg\n")
        count=0
        for r in results:
            f.write("{0:10.6f}\t{1:10.6f}\t{2:10.6f}\t{3:10d}\t{4:10.6f}\n".
            format(rbin_mid[count], r['rpavg'], r['wp'], r['npairs'], r['weightavg']))
            count+=1
        f.close()

#-----------------------------------------------------------

def make_HI_MF_plots(Mock_HI, Mock_HI_scatter, frac, mstar, beta, Lbox, sigma, fname):


    NBINS = 100
    xlim  = [9., 11]    # log10(MHI)
    delta_mf=(xlim[1]-xlim[0])/float(NBINS)
    G_new  = np.arange(xlim[0],xlim[1]+delta_mf, delta_mf)
    G_mid = np.zeros(len(G_new)-1)
    
    for i in range(len(G_new)-1):
        G_mid[i] = (G_new[i]+G_new[i+1])*0.5 

    dn_dG_new   = 10**HI_Mass_func(G_mid)         # Alfa-alfa HI mass function
    nG = np.histogram(Mock_HI[1:], G_new)[0]
    nG = (nG/delta_mf/Lbox**3)

    nG_scatt = np.histogram(Mock_HI_scatter[1:], G_new)[0]
    nG_scatt = (nG_scatt/delta_mf/Lbox**3)


    props = dict(boxstyle='round', facecolor='none', linewidth=1.5)
    
    plt.figure(1, figsize=(8, 8))
    
    gs = gridspec.GridSpec(2, 1, hspace=0.0, height_ratios=[3,1])
    ax1 = plt.subplot(gs[0])

    ax1.plot(G_mid, np.log10(dn_dG_new), linestyle='-', color='b', linewidth=2.5, label=r'$\rm \Phi^{(\alpha.70)}(M_{HI})$')
    ax1.plot(G_mid, np.log10(nG), linestyle='',marker="o", mfc='none', mew=1.5,  mec='b', ms=8, label=r'$\rm \Phi^{Mock, \sigma=0.00}(M_{HI})$')
    ax1.plot(G_mid, np.log10(nG_scatt), linestyle='',marker="s", mfc='none', mew=1.5, mec='r', ms=8, label=r'$\rm \Phi^{Mock, \sigma=%0.2f}(M_{HI})$'%sigma)
    
#    ax1.text(9.1, 10**(-4), r"$\rm f$={0:0.2f}" "\n" r"$\rm M_*$={1:0.2f}" "\n" r"$\beta$={2:0.2f}".format(frac, mstar, beta), 
#             fontsize=18, bbox=props)

    ax1.set_ylabel(r'$\rm \log_{10} (\Phi(M_{HI}))$ [$\rm Mpc^{-3}$$\rm dex^{-1}$]', fontsize=20)
    plt.setp(ax1.get_xticklabels(),visible=False)
    #ax1.set_yscale("log")
    ax1.set_ylim(-8, -1)
    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=1, fontsize=20, loc=3)
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)
    plt.tight_layout()

    ax2 = plt.subplot(gs[1],  sharex=ax1)

    ratio1  = (nG)/(dn_dG_new)
    ratio2  = (nG_scatt)/(dn_dG_new)

    ax2.plot(G_mid, ratio1, linestyle='-' , color='b', linewidth=2.5, label=r'$\rm \Sigma=0.00$')
    ax2.plot(G_mid, ratio2, linestyle='--', color='r', linewidth=3.0, label=r'$\rm \Sigma=%0.2f$'%sigma)

    ax2.axhline(y=1., linestyle='-.', linewidth=2, color='k') 
    ax2.set_xlabel(r'$\rm \log_{10} (M_{HI})$ [$\rm{M}_\odot$]', fontsize=20)
    ax2.set_ylabel(r'$\rm Data/Mock$', fontsize=18)
    ax1.set_xlim(9., 11)
    ax2.set_ylim(0.5, 1.5)
    ax2.minorticks_on()
    ax2.legend(frameon=False, ncol=2, fontsize=14, loc=2)
    ax2.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax2.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax2.spines['bottom'].set_linewidth(1.2)
    ax2.spines['left'].set_linewidth(1.2)
    ax2.spines['top'].set_linewidth(1.2)
    ax2.spines['right'].set_linewidth(1.2)
    plt.tight_layout()
    plt.savefig(fname+'.png', dpi=300, edgecolor='none', frameon='False')
    plt.close()



#-----------------------------------------------------------------------------


def make_AM_plots(G, dn_dG, H, dn_dH, frac, mstar, beta, Lbox, sigma, logMhalo_max, fname, Mock_HI_scatter, Mock_halo):

    guo_H = np.genfromtxt("../sailis-data/guo_points.dat", usecols=0)
    guo_HI_halo = np.genfromtxt("../sailis-data/guo_points.dat", usecols=1)
    
    NBINS = 1000
    
    HI_halo_func_orig, dn_dH_func_ini = AM_nonparam(G, dn_dG, H, dn_dH, 
                                                    sigma, 0, 0, 0, analytic=False, 
                                                    P_x=False, H_min=6, H_max=logMhalo_max, 
                                                    nH=NBINS, tol_err=0.01)
  
  
    foo, dn_dH_func_mod = AM_nonparam(G, dn_dG, H, dn_dH, sigma, 
                                      frac, mstar, beta, analytic=False,  P_x=False, 
                                      HI_selected_MF=True, H_min=6, H_max=logMhalo_max, 
                                      nH=NBINS, tol_err=0.01)


    HI_halo_func_scatt, HI_halo_func_HIselec = AM_nonparam(G, dn_dG, H, dn_dH, sigma, 
                                                           frac, mstar, beta, analytic=False,  P_x=True, 
                                                           HI_selected_MF=True, H_min=6, H_max=logMhalo_max, 
                                                           nH=NBINS, tol_err=0.01)

    
    ratio1 = dn_dH_func_ini(H)/dn_dH_func_mod(H)
    
    props = dict(boxstyle='round', facecolor='none', linewidth=1.8)
    
    
    try:
        in_file='sigma_0.00_AM_rel_alfa70_guo_fit_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta)
        fbar_sigma_0pt0 = np.genfromtxt(in_file+'.txt', usecols=1)
    except:
        foo, dn_dH_func_mod = AM_nonparam(G, dn_dG, H, dn_dH, 0.0, 
                                      frac, mstar, beta, analytic=False,  P_x=False, 
                                      HI_selected_MF=True, H_min=6, H_max=logMhalo_max, 
                                      nH=NBINS, tol_err=0.01)
        fbar_sigma_0pt0 = dn_dH_func_ini(H)/dn_dH_func_mod(H)
        fbar_sigma_0pt0 = HI_halo_func_HIselec(H)-np.log10(fbar_sigma_0pt0)

    

    table = [H, HI_halo_func_orig(H), HI_halo_func_HIselec(H), fbar_sigma_0pt0, HI_halo_func_scatt(H)-np.log10(ratio1) ]             
    names = ['log10(MhaloBin)', 'HI-Halo_orig', 'HI-Halo_HIselec', 'fbar_noscatter', 'fbar_sigma' ]        
    Formats = {
        'log10(MhaloBin)':'%2.14f', 
        'HI-Halo_orig':'%2.14f', 
        'HI-Halo_HIselec':'%2.14f',
        'fbar_noscatter': '%2.14f',
        'fbar_sigma':'%2.14f',
    }
    ascii.write(table, fname+"_Mvir.txt", names=names, delimiter='\t', formats=Formats, overwrite=True)



    plt.figure(1, figsize=(6, 6))

    gs = gridspec.GridSpec(1, 1)#, hspace=0.0, height_ratios=[3,1])
    ax1 = plt.subplot(gs[0, 0])
    test, =ax1.plot(H, HI_halo_func_orig(H), dashes=[3,3,10,3] , linewidth=2, color='r', label=r'$\rm \Phi(M_{h})$')
    test1, =ax1.plot(H, HI_halo_func_HIselec(H), linestyle='--', linewidth=2, color='k', label=r'$\rm \Phi^{M_{HI}}(M_{h})$')
    #test2, =ax1.plot(H, HI_halo_func_scatt(H)  , linestyle='-', linewidth=2., color='g' , label=r'$\rm \Phi^{M_{HI}}_{\sigma=%0.2f}(M_{h})$'%sigma)
    leg1  = ax1.legend(handles=[test, test1], frameon=False, ncol=1, fontsize=16, loc=2,numpoints=1, handlelength=2 )
    ax1.add_artist(leg1)
   
    ttmp,  = ax1.plot(H, fbar_sigma_0pt0, dashes=[2, 2, 2, 2, 10, 2], linewidth=2, color='b', label=r'$\rm \bar{f}^{M_{HI}}_{\sigma=0.00}(M_{h})$')
    ttmp1,  = ax1.plot(H, HI_halo_func_scatt(H)-np.log10(ratio1), ls='-', linewidth=2,color='g', 
                     label=r'$\rm \bar{f}^{M_{HI}}_{\sigma=%0.2f}(M_{h})$'%sigma)
    ttmp2, = ax1.plot(guo_H, guo_HI_halo, linestyle='', linewidth=2.0,marker="o", color='b', ms=8, mew=1.2, 
                     label=r'$\rm \bar{f}^{M_{HI}}_{Guo2020}(M_{h})$')
    leg2 = ax1.legend(handles=[ttmp, ttmp1, ttmp2], frameon=False, ncol=1, fontsize=16, loc=4,numpoints=2 , handlelength=2)

    #ax1.text(9.5, 9.8, r"$\rm f = {0:0.2f}$" "\n" r"$\rm M_* = {1:0.2f}$" "\n" r"$\beta = {2:0.2f}$" "\n" r"$\sigma = {3:0.2f}$" .format(frac, mstar, beta, sigma), fontsize=16)#, bbox=props)

    ax1.set_ylim(6, 11)
    ax1.set_xlim(9, 15.1)
    ax1.set_xlabel(r'$\rm \log_{10} (M_{h})$ $[\rm M_{\odot}]$', fontsize=20)
    ax1.set_ylabel(r'$\rm \log_{10} (M_{HI})$ $[\rm M_{\odot}]$', fontsize=20)
    ax1.minorticks_on()
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)
    
    plt.tight_layout()
    plt.savefig(fname+'_1.png', dpi=300, edgecolor='none', frameon='False')
    plt.close()
        

    #-------------------------------------------------------- 

    NBINS = 50
    xlim  = [10., 15.5]    # log10(MHI)
    delta_mf=(xlim[1]-xlim[0])/float(NBINS)
    H1  = np.arange(xlim[0],xlim[1]+ delta_mf,delta_mf)

    foo = _spline(HI_halo_func_scatt(H1), H1, k=1)


    ind = (Mock_HI_scatter < 11)*(Mock_HI_scatter >=9.)
    Mock_HI_scatt   = Mock_HI_scatter[ind]
    Mock_halo_scatt = Mock_halo[ind]

    deltax=(15.5-10.)/20.
    massbin=np.arange(10., 15.5+deltax,deltax)
    mean_mass = np.zeros(len(massbin)-1, dtype=np.float64)
    err_mass  = np.zeros(len(massbin)-1, dtype=np.float64)   
    
    for i in range(len(massbin)-1):
        ind = (Mock_halo_scatt>=massbin[i])*(Mock_halo_scatt<massbin[i+1])
        mean_mass[i] = np.mean(Mock_HI_scatt[ind])
        err_mass[i] = np.std(Mock_HI_scatt[ind])

    
    massbin = (massbin[1:]+massbin[:-1])*0.5

    deltax=(15.5-10.)/100.
    deltay=(11-9.)/100.

    xbins =np.arange(10.,15.5+deltax,deltax)
    ybins =np.arange(9.,11+deltay,deltay)

    xedges = (xbins[1:]+xbins[:-1])*0.5 
    yedges = (ybins[1:]+ybins[:-1])*0.5
    Hist, blah, blahh = np.histogram2d(Mock_halo_scatt, Mock_HI_scatt, bins=(xbins, ybins))

    #HISelecHMF, blah = np.histogram(Mock_halo_scatt, bins = xbins)
    fname1 ='../pipeline_result_data_alfa70_Mvir/HaloMockCat_Mvir_frac_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta)
    fname1 = fname1+"_sigma_{0:0.2f}.txt".format(sigma)
    #table = [xedges, HISelecHMF]             
    table = [10**Mock_halo_scatt]             
    #names = ['log10(Mbin)', 'HIHFM']        
    names = ['Mhalo']        
    Formats = {'Mhalo':'%2.14f'}
    ascii.write(table, fname1, names=names, delimiter='\t', formats=Formats, overwrite=True)



    #cmap = plt.cm.Blues
    cmap = plt.cm.binary
    cmap.set_bad('w', 1.)

    
    plt.figure(4, figsize=(6, 6))

    gs = gridspec.GridSpec(1, 1)#, hspace=0.0) #, height_ratios=[3,1])
    ax1 = plt.subplot(gs[0, 0])
    ax1.plot(H, HI_halo_func_HIselec(H), linestyle='--' , color='k', linewidth=3, label=r'$\rm \Phi^{M_{HI}}(M_{h})$')
    ax1.plot(H, HI_halo_func_scatt(H)  , linestyle='-' , color='r', linewidth=2.5, label=r'$\rm \Phi^{M_{HI}}_{\sigma=%0.2f}(M_{h})$'%sigma)
    ax1.errorbar(massbin, mean_mass    , yerr=err_mass  , fmt  ='o', color='r', ls='', mec='k',  mew=1.2, elinewidth=2, capsize=1, ms=8, label=r"")
    
#    ax1.hlines(y=10.2     , xmin=10. , xmax=foo(10.2) , linestyle ='--' , linewidth=2, color='k') 
#    ax1.vlines(x=foo(10.2), ymin=9.  , ymax=10.2  , linestyle ='--'     , linewidth=2, color='k') 
#    ax1.hlines(y=10.0     , xmin=10. , xmax=foo(10.0) , linestyle ='--' , linewidth=2, color='k') 
#    ax1.vlines(x=foo(10.0), ymin=9.  , ymax=10.   , linestyle ='--'     , linewidth=2, color='k') 
#    ax1.hlines(y=9.8      , xmin=10. , xmax=foo(9.8)   , linestyle ='--', linewidth=2, color='k') 
#    ax1.vlines(x=foo(9.8) , ymin=9.  , ymax=9.8    , linestyle ='--'    , linewidth=2, color='k') 
#    ax1.hlines(y=9.6      , xmin=10. , xmax=foo(9.6)   , linestyle ='--', linewidth=2, color='k') 
#    ax1.vlines(x=foo(9.6) , ymin=9.  , ymax=9.6    , linestyle ='--'    , linewidth=2, color='k') 
#    ax1.hlines(y=9.4      , xmin=10. , xmax=foo(9.4)   , linestyle ='--', linewidth=2, color='k') 
#    ax1.vlines(x=foo(9.4) , ymin=9.  , ymax=9.4    , linestyle ='--'    , linewidth=2, color='k') 
 

    Hist[Hist == 0] = 1  # prevent warnings in log10
    ax1.imshow(np.log10(Hist).T, origin='lower', extent=[xbins[0], xbins[-1], ybins[0], ybins[-1]],
               cmap=cmap, interpolation='none', aspect='auto')

    ax1.set_ylabel(r'$\rm log_{10} (M_{HI})[M_{\odot}]$ ', fontsize=20)
    ax1.set_xlabel(r'$\rm log_{10} (M_{h})[M_{\odot}]$', fontsize=20)
    ax1.set_xlim(10., 15.5)
    ax1.set_ylim(9., 11)
    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=1, fontsize=16, loc=4)
    ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.8)
    ax1.spines['left'].set_linewidth(1.8)
    ax1.spines['top'].set_linewidth(1.8)
    ax1.spines['right'].set_linewidth(1.8)
    plt.tight_layout()
    plt.savefig(fname+'median_relScatter.png', dpi=300, edgecolor='none', frameon='False')
    plt.close()
    
#-----------------------------------------------------------------------------

def plot_alpha40_scatt(G, dn_dG, H, dn_dH, frac, mstar, beta, sigma, logMhalo_max, NBINS):

    """
    """
    guo_H       = np.genfromtxt("../sailis-data/guo_points.dat", usecols=0)
    guo_HI_halo = np.genfromtxt("../sailis-data/guo_points.dat", usecols=1)


    HI_halo_func_orig, dn_dH_func_ini = AM_nonparam(G, dn_dG, H, dn_dH, 
                                        sigma, 0, 0, 0, analytic=False, 
                                        P_x=False, H_min=6, H_max=logMhalo_max, 
                                        nH=NBINS, tol_err=0.01)


    foo, dn_dH_func_mod = AM_nonparam(G, dn_dG, H, dn_dH, sigma, 
                                      frac, mstar, beta, analytic=False,  P_x=False, 
                                      HI_selected_MF=True, H_min=6, H_max=logMhalo_max, 
                                      nH=NBINS, tol_err=0.01)


    HI_halo_func_scatt, HI_halo_func_HIselec = AM_nonparam(G, dn_dG, H, dn_dH, sigma, 
                                             frac, mstar, beta, analytic=False,  P_x=True, 
                                             HI_selected_MF=True, H_min=6, H_max=logMhalo_max, 
                                             nH=NBINS, tol_err=0.01)


    ratio1 = dn_dH_func_ini(H)/dn_dH_func_mod(H)
    
    #-------------------------------------------------------
    # Scatter plot for alfa 40 case

    MHI_vol_lim = np.genfromtxt("../sailis-data/alfalfa_vol-limited.dat", usecols=11)
    MHalo_vol_lim = np.genfromtxt("../sailis-data/alfalfa_vol-limited.dat", usecols=17)

    keep = (MHalo_vol_lim>11.0)
    MHI_vol_lim = MHI_vol_lim[keep]
    MHalo_vol_lim = MHalo_vol_lim[keep]


    def lin_model(x, b0, b1):
        return b0+b1*x


    model = lmfit.Model(lin_model)
    p = model.make_params(b0=0.5, b1=7)
    result_lin = model.fit(data=MHI_vol_lim, params=p, x=MHalo_vol_lim, method='nelder', nan_policy='omit')
    lmfit.report_fit(result_lin)
    props = dict(boxstyle='round', facecolor='none', linewidth=1.5)
    

    plt.figure(1, figsize=(6, 6))
    gs = gridspec.GridSpec(1, 1)#, hspace=0.0, height_ratios=[3,1])
    ax1 = plt.subplot(gs[0, 0])

    ax1.plot(MHalo_vol_lim, MHI_vol_lim, linestyle='', marker='.', ms=8, lw=0, mfc='none', mec='b', mew=1.2, alpha=0.4)
    ax1.plot(H, HI_halo_func_orig(H), linestyle='-', linewidth=2.5, color='r', label=r'$\rm \Phi (M_{h})$')
    ax1.plot(H, HI_halo_func_HIselec(H), linestyle='--', linewidth=3, color='k', label=r'$\rm \Phi^{HI} (M_{h})$')

    #ax1.text(12.3, 8.3, r"$\rm \alpha.40 (Params)$" "\n" r"$\rm \phi_{*} = 0.00534$" "\n" r"$\rm M_* = 9.96$" "\n" r"$\alpha = -1.35$", fontsize=16)#, 
    ax1.set_ylim(7.8, 11)
    ax1.set_xlim(10.8, 13)
    ax1.set_xticks([11, 12, 13])
    ax1.set_yticks([8, 9, 10, 11])
    ax1.set_xlabel(r'$\rm log_{10}(M_{h})$ $[\rm{M}_\odot]$', fontsize=20)
    ax1.set_ylabel(r'$\rm log_{10}(M_{HI})$ $[\rm{M}_\odot]$', fontsize=20)
    ax1.minorticks_on()

    ax1.legend(frameon=False, ncol=1, fontsize=16, loc=4)
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)
    plt.tight_layout()
#    fname='alfa40_VolLim_MHI_Mhalo_11pt0_v1_blkWht'
#    plt.savefig(fname+'.png', dpi=600, bbox_layout='tight', edgecolor='none', frameon='False')
#    plt.close()


#--------------------------------------------------------------------------
    # Fitting Gaussian
    epsilon = MHI_vol_lim-HI_halo_func_HIselec(MHalo_vol_lim)
    temp = result_lin.params['b0'].value + result_lin.params['b1'].value*MHalo_vol_lim
    epsilon1 = MHI_vol_lim-temp
    eps_hist, bin_edges = np.histogram(epsilon, bins='auto')
    eps_hist1, bin_edges1 = np.histogram(epsilon1, bins='auto')
    norm = 1./np.sum(eps_hist*np.abs(np.diff(bin_edges))) 
    norm1 = 1./np.sum(eps_hist1*np.abs(np.diff(bin_edges1))) 


    def gaussian(x, amp, mean, sigma):
        return (amp / (np.sqrt(2*np.pi) * sigma)) * np.exp(-(x-mean)**2 / (2*sigma**2))


    model = lmfit.Model(gaussian)
    p = model.make_params(amp=1.0, mean=0.05, sigma=0.3)
    #----------------------------------------------------------------------------
    
    plt.figure(2, figsize=(12., 4.5))
    gs = gridspec.GridSpec(1, 2, wspace=0.25)#, hspace=0.000)
    
    result_gauss = model.fit(data=eps_hist*norm, params=p, x=bin_edges[:-1], method='nelder', nan_policy='omit')
    lmfit.report_fit(result_gauss)
    amp   = result_gauss.params['amp'].value
    mean  = result_gauss.params['mean'].value
    sigma = result_gauss.params['sigma'].value
    temp = (amp / (np.sqrt(2*np.pi) * sigma)) * np.exp(-(bin_edges[:-1]-mean)**2 / (2*sigma**2))


    ax1 = plt.subplot(gs[0, 0])
    ax1.plot(bin_edges[:-1],  eps_hist*norm, linestyle='', marker='o', ms=8, mfc='none', mec='b', mew=1.5, label=r'$\rm \Phi^{HI}(M_{h})$')
    ax1.plot(bin_edges[:-1], temp, linestyle='-', lw=2.5, color='r', label=r'$\rm \sigma^{\Phi^{HI}}_{bestfit}$=$%0.2f$'%sigma)
    ax1.axvline(x=mean, linestyle ='--', linewidth=2.5, color='c') 
    ax1.axvline(x=mean-sigma, linestyle ='--', linewidth=2.5, color='k') 
    ax1.axvline(x=mean+sigma, linestyle ='--', linewidth=2.5, color='k') 
    ax1.set_ylim(-0.01, )
    ax1.set_xlim(-2, 2)
    ax1.set_xticks([-2, -1, 0, 1, 2])
    ax1.set_ylabel(r'$\rm N$', fontsize=20)
    ax1.set_xlabel(r'$\rm \Delta$', fontsize=18)
    ax1.minorticks_on()

    ax1.legend(frameon=False, ncol=1, fontsize=14, loc=2, numpoints=1, handlelength=2)
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=14)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=14)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)
    
    #--------------------------------------------------------
    
    result_gauss = model.fit(data=eps_hist1*norm1, params=p, x=bin_edges1[:-1], method='nelder', nan_policy='omit')
    lmfit.report_fit(result_gauss)
    amp   = result_gauss.params['amp'].value
    mean  = result_gauss.params['mean'].value
    sigma = result_gauss.params['sigma'].value
    temp = (amp / (np.sqrt(2*np.pi) * sigma)) * np.exp(-(bin_edges1[:-1]-mean)**2 / (2*sigma**2))


    ax1 = plt.subplot(gs[0, 1])
    ax1.plot(bin_edges1[:-1],  eps_hist1*norm1, linestyle='', marker='o', ms=8, mfc='none', mec='b', mew=1.5, label=r'$\rm lin$-$\rm fit$')
    ax1.plot(bin_edges1[:-1], temp, linestyle='-', lw=2.5, color='r', label=r'$\rm \sigma^{linfit}_{bestfit}$=$%0.2f$'%sigma)
    ax1.axvline(x=mean, linestyle ='--', linewidth=2.5, color='c') 
    ax1.axvline(x=mean-sigma, linestyle ='--', linewidth=2.5, color='k') 
    ax1.axvline(x=mean+sigma, linestyle ='--', linewidth=2.5, color='k') 
    ax1.set_ylim(-0.01, )
    ax1.set_xlim(-2, 2)
    ax1.set_xticks([-2, -1, 0, 1, 2])
    ax1.set_ylabel(r'$\rm N$', fontsize=20)
    ax1.set_xlabel(r'$\rm \Delta$', fontsize=18)
    ax1.minorticks_on()

    ax1.legend(frameon=False, ncol=1, fontsize=14, loc=2, numpoints=1, handlelength=2)
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=14)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=14)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)

    #fname='scatte_epsilon_blkWht_1'
    #plt.savefig(fname+'.png', dpi=600, bbox_inches='tight', edgecolor='none', frameon='False')
    #plt.close()

    
    #----------------------------------------------------------------------------
    #Plotting HI mass function, Halo mass function and HI selected halo mass function
    
    Mbins = np.genfromtxt("/mnt/data2/saili/guovol_alfa70/total/total_final_2dswml.dat", usecols=0)
    MF = np.genfromtxt("/mnt/data2/saili/guovol_alfa70/total/total_final_2dswml.dat", usecols=1)
    err = np.genfromtxt("/mnt/data2/saili/guovol_alfa70/total/total_final_2dswml.dat", usecols=2)

    props = dict(boxstyle='round', facecolor='none', linewidth=1.5)

    fig = plt.figure(4, figsize=(6, 6))
    gs = gridspec.GridSpec(1, 1)#, hspace=0.0, height_ratios=[3,1])
    ax1 = plt.subplot(gs[0])

    ax1.errorbar(Mbins, MF, yerr=err, fmt='o', color='b', mec='b', mfc='none',
                elinewidth=1.5, capsize=5, ms=8.5, mew=1.8, label=r"$\rm \Phi_{\alpha.70}(M_{HI})$")
    ax1.plot(G, np.log10(dn_dG), ls='-', color='b', lw=2.5, label=r'')

    #ax1.plot(H, np.log10(dn_dH), ls='-', color='r', lw=2., label=r'$\rm \Phi_{SMDPL}(M_{h})$')
    #ax1.plot(H, np.log10(dn_dH_func_mod(H)), dashes=[3,3,10,3], color='k', lw=2, label=r'$\rm \Phi^{HI}_{SMDPL}(M_{h})$')

    ax1.text(6.5, -5, r"$\rm \phi_{*} = 4.47\times 10^{-3}$" "\n" r"$\rm M_* = 9.98$" "\n" r"$\alpha$  $= -1.29$", fontsize=16, bbox=props) 
    #ax1.set_xlim(6, 15.5)
    ax1.set_xlim(6, 11)
    #ax1.set_ylim(-6, 3)
    ax1.set_ylim(-6, 0.5)

    #ax1.set_xlabel(r'$\rm \log_{10}(M_{x})$ [$\rm{M}_\odot$]', fontsize=18)
    ax1.set_xlabel(r'$\rm \log_{10}(M_{HI})$ [$\rm{M}_\odot$]', fontsize=18)
    ax1.set_ylabel(r'$\rm \log_{10} \Phi$ $[\rm Mpc^{-3}$ $\rm dex^{-1}$]', fontsize=18)
    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=1, fontsize=18, loc=1)
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=14)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=14)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)
    plt.tight_layout()
    fout="alfa70_HI_mass_func_1.png"
    fig.savefig(fout, bbox_inches='tight', dpi=300, edgecolor='none', frameon='False')
   
    #---------------------------------------------------------------------------------
    
    props = dict(boxstyle='round', facecolor='none', linewidth=1.8)
    plt.figure(3, figsize=(6, 6))

    gs = gridspec.GridSpec(1, 1)#, hspace=0.0, height_ratios=[3,1])
    ax1 = plt.subplot(gs[0, 0])
    #ax1.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
    
    ax1.plot(H, HI_halo_func_orig(H), dashes=[3,3,10,3] , linewidth=2.0, color='r', label=r'$\rm \Phi_{M_{h}}$')
    ax1.plot(H, HI_halo_func_HIselec(H), linestyle='--', linewidth=2.0, color='k', label=r'$\rm \Phi^{HI}(M_{h})$')
    ax1.plot(H, HI_halo_func_HIselec(H)-np.log10(ratio1), dashes=[2, 2, 2, 2, 10, 2], color='b', linewidth=2.0, label=r'$\rm \bar{f}^{HI}(M_{h})}^{\sigma=0.00}$')
    
    fname='sigma_0.00_AM_rel_alfa70_guo_fit_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta)
    f = open(fname+".txt", "w")
    foo = HI_halo_func_HIselec(H)-np.log10(ratio1)
    for i in range(len(H)):
        f.write("{0:0.6f}\t{1:0.6f}\n".format(H[i], foo[i]))
    f.close()

    ax1.plot(guo_H, guo_HI_halo, linestyle='', linewidth=2.0,marker="o", ms=8, color='b', mew=1.2, label=r'$\rm \bar{f}_{Guo2020}^{HI}(M_{h})$')
    
    #ax1.text(9.5, 10., r"$\rm f={0:0.2f}$" "\n" r"$\rm M_*={1:0.2f}$" "\n" r"$\beta={2:0.2f}$" .format(frac, mstar, beta), fontsize=14, bbox=props)
    ax1.set_ylim(6, 11)
    ax1.set_xlim(9, 15.1)
    
    ax1.set_xlabel(r'$\rm log_{10} (M_{h})$ $[\rm{M}_\odot]$', fontsize=18)
    ax1.set_ylabel(r'$\rm log_{10} (M_{HI})$ $[\rm{M}_\odot]$', fontsize=18)
    ax1.minorticks_on()
    ax1.legend(frameon=False, ncol=1, fontsize=16, loc=4, numpoints=1, handlelength=2)
    #ax1.legend(frameon=False, ncol=1, fontsize=16, loc=4, numpoints=1)
    ax1.tick_params(axis='both', which='minor', length=5, width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8, width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)
    plt.tight_layout()
    fname='sigma_0.00_AM_rel_alfa70_guo_fit_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta)
    plt.savefig(fname+'.png', dpi=300, bbox_layout='tight', edgecolor='none', frameon='False')
    plt.close()

#-----------------------------------------------------------------------------


def make_mock_HI(logMvir, G, dn_dG, H, dn_dH, sigma, frac, mstar, beta, NBINS, 
                 logMhalo_min, logMhalo_max, Lbox,Halox, Haloy, Haloz, Halovz, 
                 grid_pram_search, PLOT=True):

    """
    """

    foo, dn_dH_func_mod = AM_nonparam(G, dn_dG, H, dn_dH, sigma, 
                          frac, mstar, beta, analytic=False, P_x=False, 
                          HI_selected_MF=True, H_min=6, H_max=logMhalo_max, 
                          nH=NBINS, tol_err=0.01)
                          

    HI_halo_func_scatt, HI_halo_func_ini = AM_nonparam(G, dn_dG, H, dn_dH, sigma, 
                                           frac, mstar, beta, analytic=False, P_x=True, 
                                           HI_selected_MF=True, H_min=6, H_max=logMhalo_max, 
                                           nH=NBINS, tol_err=0.01)


    NBINS = 50
    xlim  = [logMhalo_min, logMhalo_max]    # log10(MHI)
    delta_mf=(xlim[1]-xlim[0])/float(NBINS)
    H1  = np.arange(xlim[0],xlim[1]+delta_mf, delta_mf)
    
    Mock_HI         = np.zeros(1, dtype=np.float64)
    Mock_HI_scatter = np.zeros(1, dtype=np.float64)
    Mock_Halo       = np.zeros(1, dtype=np.float64)

    Mock_x = np.zeros(1, dtype=np.float64)
    Mock_y = np.zeros(1, dtype=np.float64)
    Mock_z = np.zeros(1, dtype=np.float64)
    Mock_vz = np.zeros(1, dtype=np.float64)

    in_seed = 68927 
    #np.random.seed(12340)
    np.random.seed(in_seed)

    for i in range(len(H1)-1):

    #    temp1 = (dn_dH_func_mod(H[i])+ dn_dH_func_mod(H[i+1]))*0.5*delta_mf*Lbox**3/(0.6777**3)
        y = lambda x: dn_dH_func_mod(x)
        temp2, err = integrate.quad(y, H1[i], H1[i+1])
        
        keep = (logMvir>=H1[i])*(logMvir<H1[i+1])
        halo_in = logMvir[keep]
         
        halo_xin  = Halox[keep]
        halo_yin  = Haloy[keep]
        halo_zin  = Haloz[keep]
        halo_vzin = Halovz[keep]

        rand_pick_halo = np.random.randint(0, len(halo_in), size=np.int64(temp2*Lbox**3))
        u1 = np.random.normal(0, sigma, size=np.int64(temp2*Lbox**3))
       
        temp = halo_in[rand_pick_halo]
        Mock_Halo = np.concatenate((Mock_Halo, temp))

        mock_HI = HI_halo_func_ini(halo_in[rand_pick_halo])
        Mock_HI = np.concatenate((Mock_HI, mock_HI))
        
        mock_HI_scatt = HI_halo_func_scatt(halo_in[rand_pick_halo])+u1
        Mock_HI_scatter = np.concatenate((Mock_HI_scatter, mock_HI_scatt))

        temp = halo_xin[rand_pick_halo]
        Mock_x = np.concatenate((Mock_x, temp))
        
        temp = halo_yin[rand_pick_halo]
        Mock_y = np.concatenate((Mock_y, temp))
     
        temp = halo_zin[rand_pick_halo]
        Mock_z = np.concatenate((Mock_z, temp))

        temp = halo_vzin[rand_pick_halo]
        Mock_vz = np.concatenate((Mock_vz, temp))
     
     
    print('\n')
    print('||--------- Computing wp(rp) for scatter and non-scatter case --------||')
    print('\n')

    
    if not grid_pram_search:
        #dir_name =('../pipeline_result_data_alfa70_%d'%in_seed)
        #if not os.path.exists(dir_name):
        #    os.mkdir(dir_name)
        #    os.mkdir(dir_name+'/clust_data')
        #    os.mkdir(dir_name+'/clust_data/REAL')
        #else:
        dir_name =('../pipeline_result_data_alfa70_Mvir/clust_data/REAL/')
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
    else:
        dir_name =('../pipeline_result_data_alfa70_scatter_%0.2f/bf_params_clust_data_Scatter_%0.2f/'%(sigma, sigma))

    fname = (dir_name+'HI_alfa70_wp_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta))
#    wp_corr(Mock_x[1:], Mock_y[1:], Mock_z[1:], Mock_HI_scatter[1:], Mock_vz[1:], sigma, fname)

    if not grid_pram_search:
        dir_name =('../pipeline_result_data_alfa70_Mvir/clust_data/REAL/')
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
    else:
        dir_name ='../pipeline_result_data_alfa70_scatter_%0.2f/bf_params_clust_data_NoScatter/'%sigma
        
    fname = (dir_name+'HI_alfa70_wp_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta))
#    wp_corr(Mock_x[1:], Mock_y[1:], Mock_z[1:], Mock_HI[1:], Mock_vz[1:], 0.00, fname)
     

    if PLOT:
        print('||--------- making some plots --------||')
        print('\n')
        # Make Plots
        if not grid_pram_search:
            dir_name =('../pipeline_result_data_alfa70_Mvir/bf_params_grid_AM_fit_plots/sigma_%0.2f_'%sigma)
        else:
            dir_name =('../pipeline_result_data_alfa70_scatter_%0.2f/bf_params_grid_AM_fit_plots/'%sigma)

        fname = (dir_name+'AM_rel_alfa70_guo_fit_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta))
        make_AM_plots(G, dn_dG, H, dn_dH, frac, mstar, beta, Lbox, sigma, logMhalo_max, fname, Mock_HI_scatter[1:], Mock_Halo[1:])
        
        if not grid_pram_search:
            dir_name =('../pipeline_result_data_alfa70_Mvir/bf_params_grid_HI_recons_plots/sigma_%0.2f_'%sigma)
        else:
            dir_name =('../pipeline_result_data_alfa70_scatter_%0.2f/bf_params_grid_HI_recons_plots/'%sigma)
        
        
        fname = (dir_name+'HI_MF_Mvir_recons_alfa70_guo_fit_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta))
        make_HI_MF_plots(Mock_HI, Mock_HI_scatter,frac, mstar, beta, Lbox, sigma, fname)


#-----------------------------------------------------------


def main_driver():
    
    # Read User given parameters

    parser = argparse.ArgumentParser()
    parser.add_argument("sigma", type=float, 
    help='Input scatter e.g 0.25')
    parser.add_argument("use_schechter", type=int, 
    help='if 1 use schecter function else use linear interpolation')

    
    args             = parser.parse_args()
    scatter_inp      = args.sigma # input scatter that is needed
    use_schechter    = args.use_schechter # do you want Schechter fit(1) or linear extrapolation
    grid_pram_search = False # Do you want to get clustering value around bestfit of Guo points


    # Read Rockstar Halo catalog for SMDPL

    #Halo_Cat_fname = ("/mnt/data1/sandeep/multidark2_data/halo_cat/"\
    #                 "halo_data/Rockstar_SMDPL_halo_properties.csv")
    
    Halo_Cat_fname = ("/mnt/data1/sandeep/multidark2_data/halo_cat/"\
                     "halo_data/Rockstar_SMDPL_Halo_Mpeak.csv")
    

    # Box Size for SMDPL
    #SMDPL cosmology uses h = 0.6777
    
    Lbox = 400/0.6777 #h^-1 Mpc taken out h^-1 Mpc

    print("Reading Rockstar SMDPL Halo Catalog (Mvir and positions)\n")
    #print("Reading Rockstar SMDPL Halo Catalog (Mpeak and positions)\n")
    
    MDPLpropdata = dt.fread(Halo_Cat_fname)
    Mvir   = np.ndarray.flatten(MDPLpropdata['Mvir'].to_numpy())/0.6777 # as Mvir is h^-1 Msun units taken out h^-1
    #Mvir   = np.ndarray.flatten(MDPLpropdata['Mpeak'].to_numpy())/0.6777 # as Mvir is h^-1 Msun units taken out h^-1
    Halox  = np.ndarray.flatten(MDPLpropdata['x'].to_numpy()) # as X is h^-1 Mpc units
    Haloy  = np.ndarray.flatten(MDPLpropdata['y'].to_numpy()) # as Y is h^-1 Mpc units
    Haloz  = np.ndarray.flatten(MDPLpropdata['z'].to_numpy()) # as Z is h^-1 Mpc units
    Halovz = np.ndarray.flatten(MDPLpropdata['vz'].to_numpy()) # as Z is h^-1 Mpc units
    
    print("Releasing unused memory\n")
    del MDPLpropdata
    
    print("||----- Fitting Halo mass function and getting bestfit Schechter function parameter -----||\n")
    

    logMhalo_min = 10.0
    logMhalo_max = 15.5
    NBINS = 20
    
    xlim      = [logMhalo_min, logMhalo_max]
    deltaM    = (xlim[1]-xlim[0])/np.float64(NBINS)
    logMbins  = np.arange(xlim[0],xlim[1]+deltaM, deltaM)

    p_ini = dict(phi1=0.001, x1=14, alpha1=-1.0) #initial guess for parameters to fit function
    
    result_SF, logMbins_avg, dn_dH_ini, err, dn_dH_func  = fit_abundance_halo(Mvir, weights=1.0/Lbox**3.0, bins=logMbins, xlog=True,
                                                           fit_type='schechter', p = p_ini, show_fit=False)

    #-------------------------------------------------------------------------------------------
    
    print("||----- Fitting Halo MF intial points for linear interpolation -----||\n")
    
    # non-linear generalized fitting library

    def Lin_Model(x, m, c):
        return m*x+c 


    Model = lmfit.Model(Lin_Model)
    p = dict(m=0.86298182, c = 1.) #initial guess for parameters to fit function
    
    # create Parameters, giving initial values
    params = Model.make_params(m=p['m'], c=p['c'] )
    # do fit, print result
    X = logMbins_avg[3:6]
    Y = np.log10(dn_dH_ini[3:6])
    Lin_result = Model.fit(Y, params, x=X)
    print(Lin_result.fit_report())
    print("--------------------------------------------------\n")


    print("||----- Reading Guo Points -----||\n")
    
    guo_H       = np.genfromtxt("../sailis-data/guo_points.dat", usecols=0)
    guo_HI_halo = np.genfromtxt("../sailis-data/guo_points.dat", usecols=1)

    NBINS = 1000
    #NBINS = 50
    #NBINS = 200
    print("||----- Getting alfa70% mass function -----||\n")
    
    xlim  = [6, 11]    # log10(MHI)
    delta_mf=(xlim[1]-xlim[0])/float(NBINS)
    G  = np.arange(xlim[0],xlim[1]+delta_mf,delta_mf)
    dn_dG   = 10**HI_Mass_func(G)         # Alfa-alfa HI mass function

    print("||----- Getting halo mass function -----||\n")

    xlim  = [6, logMhalo_max]    # log10(MHI)
    delta_mf=(xlim[1]-xlim[0])/float(NBINS)
    H  = np.arange(xlim[0],xlim[1]+delta_mf,delta_mf)

    # Here using best fit schechter func params to get halo mass func.
    
    if use_schechter==1:

        print("||----- Using Schechter Linear fit -----||\n")

        dn_dH = schechter_function(H, result_SF.params['phistar'].value,
                result_SF.params['alpha'].value, result_SF.params['mstar'].value)
    else:
        print("||----- Using HMF Linear fit -----||\n")

        dn_dH = dn_dH_func(H)
        ind  = (H>logMbins_avg[3])
        y = Lin_result.params['m'].value*H[~ind]+ Lin_result.params['c']
        y1 = dn_dH_func(H[ind])
        dn_dH = np.concatenate((10**y, y1))



    # Calling AM_nonparam routine for iterative deconvolution to get abundance matching
    # with without scatter, if P_x is false then 
    # return HI-Halo AM match without scatter, halo MF interpolation function
    
    HI_halo_func_ini1, dn_dH_func_ini = AM_nonparam(G, dn_dG, H, dn_dH, 
                                            scatter_inp, 0, 0, 0, analytic=False, 
                                            P_x=False, H_min=6, H_max=logMhalo_max, 
                                            nH=NBINS, tol_err=0.01)
    
#----------------------------------------------------------------------------

    print("\n")
    print("||----- Fitting for HI-Selected Mass function ----||\n")
    

    def my_model(x, frac, mstar, beta):
        HI_halo_func_ini2, dn_dH_func_mod = AM_nonparam(G, dn_dG, H, dn_dH, 
                                                scatter_inp, frac, mstar, 
                                                beta, analytic=False, HI_selected_MF=True, 
                                                P_x=False, H_min=6, H_max=logMhalo_max, 
                                                nH=NBINS, tol_err=0.01)
 
        ratio = dn_dH_func_ini(x)/dn_dH_func_mod(x)
        return HI_halo_func_ini2(x)-np.log10(ratio)  

#---------------------------------------------------------

    Model = lmfit.Model(my_model)

    p = dict(frac=0.3204,mstar=13.6609,beta=0.9957) #initial guess for parameters to fit function
    #p = dict(frac=0.3,mstar=13.,beta=0.5) #initial guess for parameters to fit function
    # create Parameters, giving initial values
    params = Model.make_params(frac=p['frac'], mstar=p['mstar'], beta=p['beta'])
    params['frac'].min = 0.1
    params['mstar'].min = 12.5
    params['beta'].min = 0.1

    # do fit, print result
    result = Model.fit(guo_HI_halo, params, x=guo_H)

    print(result.fit_report())
    print("\n")
    
    f = open("guo_SMDPL_best_fit_alfa70_Mvir.txt", "w")
    f.write("{0:0.6f}\t{1:0.6f}\t{2:0.6f}\n".format(result.params['frac'].value, result.params['mstar'].value, result.params['beta'].value))
    f.close()
    logMvir = np.log10(Mvir)


    sigma = scatter_inp
    frac = result.params['frac'].value
    mstar = result.params['mstar'].value
    beta = result.params['beta'].value
#    plot_alpha40_scatt(G, dn_dG, H, dn_dH, frac, mstar, beta, sigma, logMhalo_max, NBINS)
   
#    plt.show()

        
    if not grid_pram_search:

        make_mock_HI(logMvir, G, dn_dG, H, dn_dH, scatter_inp, result.params['frac'].value, result.params['mstar'].value, 
                     result.params['beta'].value, NBINS, logMhalo_min, logMhalo_max, Lbox, Halox, Haloy, Haloz,Halovz, 
                     grid_pram_search)

    else:

        frac_bins  = np.linspace(result.params['frac'].value  - 0.3, result.params['frac'].value + 0.3, 5, endpoint=True) 
        mstar_bins = np.linspace(result.params['mstar'].value - 1.6, result.params['mstar'].value + 1.6, 5, endpoint=True) 
        beta_bins  = np.linspace(result.params['beta'].value  - 0.2, result.params['beta'].value + 0.2, 5, endpoint=True) 
    
        inp_param = np.zeros((len(frac_bins)*len(mstar_bins)*len(beta_bins), 3), dtype=np.float64)
        count =0
        for i in range(len(frac_bins)):
            for j in range(len(mstar_bins)):
                for k in range(len(beta_bins)):
                    inp_param[count, 0]  = frac_bins[i]
                    inp_param[count, 1]  = mstar_bins[j]
                    inp_param[count, 2]  = beta_bins[k]
                    count+=1    


        max_core =25
        count=0
        print('||---------- Doing first batch of 25 params combination out of 125 ---------||')
        print('\n') 
        
        strn = [] 
        for i in range(0, max_core):
            s = 'Cell_Count%d' % (i+1)
            strn.append(s)
        

        for i in range(len(strn)):
            strn[i] = Process(target=make_mock_HI, args=(logMvir, G, dn_dG, H, dn_dH, scatter_inp, inp_param[count, 0], inp_param[count, 1], 
                              inp_param[count, 2], NBINS, logMhalo_min, logMhalo_max, Lbox, Halox, Haloy, Haloz,Halovz))
            strn[i].start()
            count+=1
            
        for i in range(len(strn)):
            strn[i].join()
        
        print('||---------- Doing Second batch of 25 params combination out of 125 ---------||')
        print('\n') 

        strn = [] 
        for i in range(0, max_core):
            s = 'Cell_Count%d' % (i+1)
            strn.append(s)
        

        for i in range(len(strn)):
            strn[i] = Process(target=make_mock_HI, args=(logMvir, G, dn_dG, H, dn_dH, scatter_inp, inp_param[count, 0], inp_param[count, 1], 
                              inp_param[count, 2], NBINS, logMhalo_min, logMhalo_max, Lbox, Halox, Haloy, Haloz,Halovz))
            strn[i].start()
            count+=1
            
        for i in range(len(strn)):
            strn[i].join()
        
        
        print('||---------- Doing Third batch of 25 params combination out of 125 ---------||')
        print('\n') 

        strn = [] 
        for i in range(0, max_core):
            s = 'Cell_Count%d' % (i+1)
            strn.append(s)
        

        for i in range(len(strn)):
            strn[i] = Process(target=make_mock_HI, args=(logMvir, G, dn_dG, H, dn_dH, scatter_inp, inp_param[count, 0], inp_param[count, 1], 
                              inp_param[count, 2], NBINS, logMhalo_min, logMhalo_max, Lbox, Halox, Haloy, Haloz,Halovz))
            strn[i].start()
            count+=1
            
        for i in range(len(strn)):
            strn[i].join()
        

        print('||---------- Doing 4th batch of 25 params combination out of 125 ---------||')
        print('\n') 

        strn = [] 
        for i in range(0, max_core):
            s = 'Cell_Count%d' % (i+1)
            strn.append(s)
        

        for i in range(len(strn)):
            strn[i] = Process(target=make_mock_HI, args=(logMvir, G, dn_dG, H, dn_dH, scatter_inp, inp_param[count, 0], inp_param[count, 1], 
                              inp_param[count, 2], NBINS, logMhalo_min, logMhalo_max, Lbox, Halox, Haloy, Haloz,Halovz))
            strn[i].start()
            count+=1
            
        for i in range(len(strn)):
            strn[i].join()
        
      
        print('||---------- Doing 5th batch of 25 params combination out of 125 ---------||')
        print('\n') 

        strn = [] 
        for i in range(0, max_core):
            s = 'Cell_Count%d' % (i+1)
            strn.append(s)
        

        for i in range(len(strn)):
            strn[i] = Process(target=make_mock_HI, args=(logMvir, G, dn_dG, H, dn_dH, scatter_inp, inp_param[count, 0], inp_param[count, 1], 
                              inp_param[count, 2], NBINS, logMhalo_min, logMhalo_max, Lbox, Halox, Haloy, Haloz,Halovz))
            strn[i].start()
            count+=1
            
        for i in range(len(strn)):
            strn[i].join()
        
    
    
        

if __name__ == "__main__":
    main_driver()



"""
plt.figure(5, figsize=(6, 6))

gs = gridspec.GridSpec(1, 1)#, hspace=0.0) #, height_ratios=[3,1])
ax1 = plt.subplot(gs[0, 0])

#ax1.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
#ax1.set_prop_cycle('color', palettable.colorbrewer.qualitative.Dark2_4.mpl_colors)
ax1.set_prop_cycle('color', palettable.colorbrewer.qualitative.Set1_7.mpl_colors)

ax1.plot(H, HI_halo_func_HIselec(H), linestyle='-', linewidth=2.5, label=r'$\rm \Phi_{M_{h}(M_{HI})}$')
ax1.plot(H, HI_halo_func_scatt(H), linestyle='-', linewidth=2.5, label=r'$\rm \Phi_{M_{h}(M_{HI})}^{\sigma=%0.2f}$'%sigma)

ax1.hlines(y=10.2     , xmin=10. , xmax=foo(10.2) , linestyle ='--' , linewidth=2, color='k') 
#ax1.vlines(x=foo(10.2), ymin=9.  , ymax=10.2  , linestyle ='--'     , linewidth=2, color='k') 
ax1.hlines(y=10.0     , xmin=10. , xmax=foo(10.0) , linestyle ='--' , linewidth=2, color='k') 
#ax1.vlines(x=foo(10.0), ymin=9.  , ymax=10.   , linestyle ='--'     , linewidth=2, color='k') 
ax1.hlines(y=9.8      , xmin=10. , xmax=foo(9.8)   , linestyle ='--', linewidth=2, color='k') 
#ax1.vlines(x=foo(9.8) , ymin=9.  , ymax=9.8    , linestyle ='--'    , linewidth=2, color='k') 
ax1.hlines(y=9.6      , xmin=10. , xmax=foo(9.6)   , linestyle ='--', linewidth=2, color='k') 
#ax1.vlines(x=foo(9.6) , ymin=9.  , ymax=9.6    , linestyle ='--'    , linewidth=2, color='k') 
ax1.hlines(y=9.4      , xmin=10. , xmax=foo(9.4)   , linestyle ='--', linewidth=2, color='k') 
#ax1.vlines(x=foo(9.4) , ymin=9.  , ymax=9.4    , linestyle ='--'    , linewidth=2, color='k') 


Hist[Hist == 0] = 1  # prevent warnings in log10
ax1.imshow(np.log10(Hist).T, origin='lower', extent=[xbins[0], xbins[-1], ybins[0], ybins[-1]],
           cmap=cmap, interpolation='none', aspect='auto')

ax1.set_ylabel(r'$\rm log_{10} (M_{HI})[M_{\odot}]$ ', fontsize=22)
ax1.set_xlabel(r'$\rm log_{10} (M_{h})[M_{\odot}]$', fontsize=22)
ax1.set_xlim(10., 15.5)
ax1.set_ylim(9., 11)
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=20, loc=4)
ax1.tick_params(axis='both', which='minor', length=5,
                  width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                   width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
plt.tight_layout()
plt.savefig(fname+'median_relScatter_1.png', dpi=300, edgecolor='none', frameon='False')
plt.close()


"""

