from __future__ import print_function, division
import numpy as np
import sys
import time
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
from colorama import Fore

from tqdm import tqdm, trange
import argparse
from astropy.cosmology import LambdaCDM
import datatable as dt


mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rc('font', weight='bold')
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Ubuntu'
mpl.rcParams['font.monospace'] = 'Ubuntu Mono'
mpl.rcParams['axes.labelweight'] = 'bold'

mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'

import palettable

#set some plotting parameters
plt.style.use("classic")
#plt.style.use("ggplot")
#plt.style.use("seaborn-paper")
#plt.style.use("fast")


#---------------- Global Values ---------------------------
guo_dir_name='../../HI-Halo_scatter_relation/HI_mock_cat/clust_data/'
cutoff_name = np.array(['9pt4', '9pt6', '9pt8', '10pt0', '10pt2'])

jackknife = "/mnt/data1/sandeep/workspace/Guo_2017_reproduce/new_test"\
            "/pipeline_result_data_alfa70_Mvir/clust_data/Jackknife_test/Subsample_2Cube_180Mpchinv/"

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("sigma", type=float, 
    help='Input scatter e.g 0.25')

    args = parser.parse_args()
    sigma= args.sigma

    dir_name1 = ('../pipeline_result_data_alfa70_Mpeak/clust_data/REAL/')
    dir_name2 = ('../pipeline_result_data_alfa70/clust_data/REAL/')

    frac1  = np.genfromtxt("guo_SMDPL_best_fit_alfa70_Mpeak.txt", usecols=0)
    mstar1 = np.genfromtxt("guo_SMDPL_best_fit_alfa70_Mpeak.txt", usecols=1)
    beta1  = np.genfromtxt("guo_SMDPL_best_fit_alfa70_Mpeak.txt", usecols=2)
    
    frac2  = np.genfromtxt("guo_SMDPL_best_fit_alfa70.txt", usecols=0)
    mstar2 = np.genfromtxt("guo_SMDPL_best_fit_alfa70.txt", usecols=1)
    beta2  = np.genfromtxt("guo_SMDPL_best_fit_alfa70.txt", usecols=2)
    


    dirc ='/mnt/data1/sandeep/my_all_xanadu_codes/my_mesh_npairs/GFastCorr/examples/'
    guo_dm_rp = np.genfromtxt(dirc+"wp_guo.txt", usecols=0) 
    guo_dm_wp = np.genfromtxt(dirc+"wp_guo.txt", usecols=1) 
    plot_dir = '../pipeline_result_data_alfa70_Mvir/'
    print(plot_dir)
    props = dict(boxstyle='round', facecolor='none', linewidth=1.5)
    
    fname1    = (dir_name1+'HI_alfa70_wp_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac1, mstar1, beta1))
    fname2    = (dir_name2+'HI_alfa70_wp_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac2, mstar2, beta2))

    chi_tot=0.0
    
    fig= plt.figure(1, figsize=(12., 10))

    gs = gridspec.GridSpec(2, 3, wspace=0.000, hspace=0.000)
    ax1 = plt.subplot(gs[0, 0])

    fname = fname2+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[0], sigma)
    rp_Mvir =np.genfromtxt(fname, usecols=0) 
    wp_model_Mvir =np.genfromtxt(fname, usecols=2) 
    
    wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[0], usecols=1) 
    err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[0], usecols=2) 
    
    fname = jackknife+"Jack_wp_real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[0], sigma)
    wp_mean_ =np.genfromtxt(fname, usecols=1) 
    jack_err_Mvir =np.genfromtxt(fname, usecols=4) 


    test, = ax1.plot(rp_Mvir, wp_model_Mvir, lw=2, ls='--', color='k', label=r"$\rm M_{HI}>10^{9.4}M_{\odot}^{M_{vir}}$")
    ax1.fill_between(rp_Mvir, wp_mean_-jack_err_Mvir, wp_mean_+jack_err_Mvir, color='#bdbdbd')  

    leg1  = ax1.legend(handles=[test], frameon=False, ncol=1, fontsize=20, loc=2,numpoints=1 )
    ax1.add_artist(leg1)
    
    ttmp,  = ax1.plot(guo_dm_rp, guo_dm_wp, lw=2.5, ls='-', color='r', label=r"$\rm Dark Matter$")
    ttmp1 =  ax1.errorbar(rp_Mvir, wp_data, yerr=err, mec='b', linestyle='',capsize=5, mfc='none', mew=1.5, fmt="o", 
                color='b', elinewidth=1.5, ms=8, label=r"$\rm Guo2017$")

    leg2 = ax1.legend(handles=[ttmp, ttmp1], frameon=False, ncol=1, fontsize=18, loc=3,numpoints=2 )

    ax1.set_ylabel(r'$\rm w_{p}(r_{p}) [h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax1.set_xscale("log")
    ax1.set_yscale("log")
    #ax1.set_xlim(0.1, 30)
    ax1.set_ylim(1, 900)
    ax1.set_yticks([1, 10, 100])
    ax1.set_yticklabels(['1', '10', '100'])
    ax1.set_xticks([0.1, 1, 10])
    ax1.set_xticklabels(['0.1', '1', '10'])

    ax1.minorticks_on()
    plt.setp(ax1.get_xticklabels(),visible=False)
    
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)

    #------------------------------------------------------------------
    
    ax2 = plt.subplot(gs[0, 1],  sharey=ax1)
    ax2.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)

    fname = fname2+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[1], sigma)
    rp_Mvir =np.genfromtxt(fname, usecols=0) 
    wp_model_Mvir =np.genfromtxt(fname, usecols=2) 
    
    wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[1], usecols=1) 
    err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[1], usecols=2) 
    
    fname = jackknife+"Jack_wp_real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[1], sigma)
    jack_err_Mvir =np.genfromtxt(fname, usecols=4) 
    wp_mean_ =np.genfromtxt(fname, usecols=1) 


    test, = ax2.plot(rp_Mvir, wp_model_Mvir, lw=2, ls='--', color='k', label=r"$\rm M_{HI}>10^{9.6}M_{\odot}^{M_{vir}}$")
    ax2.fill_between(rp_Mvir, wp_mean_-jack_err_Mvir, wp_mean_+jack_err_Mvir, color='#bdbdbd')  
    
    leg1  = ax2.legend(handles=[test], frameon=False, ncol=1, fontsize=20, loc=1,numpoints=1 )
    ax2.add_artist(leg1)
    
    ttmp,  = ax2.plot(guo_dm_rp, guo_dm_wp, lw=2.5, ls='-', color='r', label=r"$\rm Dark Matter$")
    ttmp1 =  ax2.errorbar(rp_Mvir, wp_data, yerr=err, mec='b', linestyle='', mfc='none',capsize=5, mew=1.5, fmt="o", 
                          color='b', elinewidth=1.5, ms=8, label=r"$\rm Guo2017$")
    leg2 = ax2.legend(handles=[ttmp, ttmp1], frameon=False, ncol=1, fontsize=18, loc=3,numpoints=2 )

    
    ax2.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax2.set_xscale("log")
    ax2.set_yscale("log")
    #ax2.set_xlim(0.1, 30)
    ax2.set_ylim(1, 900)
    ax2.set_yticks([1, 10, 100])
    ax2.set_yticklabels(['1', '10', '100'])
    ax2.set_xticks([0.1, 1, 10])
    ax2.set_xticklabels(['0.1', '1', '10'])


    ax2.minorticks_on()
    ax2.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax2.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax2.spines['bottom'].set_linewidth(1.2)
    ax2.spines['left'].set_linewidth(1.2)
    ax2.spines['top'].set_linewidth(1.2)
    ax2.spines['right'].set_linewidth(1.2)
    plt.setp(ax2.get_yticklabels(),visible=False)
    plt.setp(ax2.get_xticklabels(),visible=False)


    #------------------------------------------------------------------
    
    ax3 = plt.subplot(gs[1, 0])
    ax3.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
    
    fname = fname2+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[2], sigma)
    rp_Mvir =np.genfromtxt(fname, usecols=0) 
    wp_model_Mvir =np.genfromtxt(fname, usecols=2) 
    
    wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[2], usecols=1) 
    err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[2], usecols=2) 
    
    fname = jackknife+"Jack_wp_real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[2], sigma)
    jack_err_Mvir =np.genfromtxt(fname, usecols=4) 
    wp_mean_ =np.genfromtxt(fname, usecols=1) 

    test, = ax3.plot(rp_Mvir, wp_model_Mvir, lw=2, ls='--', color='k', label=r"$\rm M_{HI}>10^{9.8}M_{\odot}^{M_{vir}}$")
    ax3.fill_between(rp_Mvir, wp_mean_-jack_err_Mvir, wp_mean_+jack_err_Mvir, color='#bdbdbd')  
   
    leg1  = ax3.legend(handles=[test], frameon=False, ncol=1, fontsize=20, loc=1,numpoints=1 )
    ax3.add_artist(leg1)
    
    ttmp,  = ax3.plot(guo_dm_rp, guo_dm_wp, lw=2.5, ls='-', color='r', label=r"$\rm DarkMatter$")
    ttmp1 =  ax3.errorbar(rp_Mvir, wp_data, yerr=err, mec='b', linestyle='',capsize=5, mfc='none', mew=1.5, fmt="o", 
                color='b', elinewidth=1.5, ms=8, label=r"$\rm Guo2017$")

    leg2 = ax3.legend(handles=[ttmp, ttmp1], frameon=False, ncol=1, fontsize=20, loc=3,numpoints=2 )
    
    ax3.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax3.set_ylabel(r'$\rm w_{p}(r_{p}) [h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax3.set_xscale("log")
    ax3.set_yscale("log")
    ax3.set_xlim(0.1, 30)
    ax3.set_ylim(1, 900)
    ax3.set_yticks([1, 10, 100])
    ax3.set_yticklabels(['1', '10', '100'])
    ax3.set_xticks([0.1, 1, 10])
    ax3.set_xticklabels(['0.1', '1', '10'])
    
    ax3.minorticks_on()
    ax3.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax3.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax3.spines['bottom'].set_linewidth(1.2)
    ax3.spines['left'].set_linewidth(1.2)
    ax3.spines['top'].set_linewidth(1.2)
    ax3.spines['right'].set_linewidth(1.2)
    
    #------------------------------------------------------------------

    ax4 = plt.subplot(gs[1, 1], sharey=ax3)
    ax4.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
    
    fname = fname2+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[3], sigma)
    rp_Mvir =np.genfromtxt(fname, usecols=0) 
    wp_model_Mvir =np.genfromtxt(fname, usecols=2) 
    
    wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[3], usecols=1) 
    err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[3], usecols=2) 
    
    fname = jackknife+"Jack_wp_real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[3], sigma)
    jack_err_Mvir =np.genfromtxt(fname, usecols=4) 
    wp_mean_ =np.genfromtxt(fname, usecols=1) 

    test, = ax4.plot(rp_Mvir, wp_model_Mvir, lw=2,ls='--', color='k', label=r"$\rm M_{HI}>10^{10.0}M_{\odot}^{M_{vir}}$")
    ax4.fill_between(rp_Mvir, wp_mean_-jack_err_Mvir, wp_mean_+jack_err_Mvir, color='#bdbdbd')  
    
    leg1 = ax4.legend(handles=[test], frameon=False, ncol=1, fontsize=20, loc=1,numpoints=1 )
    ax4.add_artist(leg1)
    
    ttmp,  = ax4.plot(guo_dm_rp, guo_dm_wp, lw=2.5, ls='-',color='r', label=r"$\rm DarkMatter$")
    ttmp1 =  ax4.errorbar(rp_Mvir, wp_data, yerr=err,mec='b',  linestyle='',capsize=5, mfc='none', mew=1.5, fmt="o", 
               color='b',  elinewidth=1.5, ms=8, label=r"$\rm Guo2017$")

    leg2 = ax4.legend(handles=[ttmp, ttmp1], frameon=False, ncol=1, fontsize=20, loc=3,numpoints=2 )

    
    ax4.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax4.set_xscale("log")
    ax4.set_yscale("log")
    ax4.set_xlim(0.1, 30)
    #ax4.set_ylim(1, 1000)
    #ax4.set_yticks([1, 10, 100])
    ax4.set_ylim(1, 900)
    ax4.set_yticks([1, 10, 100])
    ax4.set_yticklabels(['1', '10', '100'])
    ax4.set_xticks([0.1, 1, 10])
    ax4.set_xticklabels(['0.1', '1', '10'])
    ax4.minorticks_on()
    plt.setp(ax4.get_yticklabels(),visible=False)
    ax4.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax4.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax4.spines['bottom'].set_linewidth(1.2)
    ax4.spines['left'].set_linewidth(1.2)
    ax4.spines['top'].set_linewidth(1.2)
    ax4.spines['right'].set_linewidth(1.2)
    
   
    #------------------------------------------------------------------
    
    ax5 = plt.subplot(gs[1, 2],  sharey=ax3)
    ax5.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
    
    fname = fname2+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[4], sigma)
    rp_Mvir =np.genfromtxt(fname, usecols=0) 
    wp_model_Mvir =np.genfromtxt(fname, usecols=2) 
    wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[4], usecols=1) 
    err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[4], usecols=2) 
    
    fname = jackknife+"Jack_wp_real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[4], sigma)
    jack_err_Mvir = np.genfromtxt(fname, usecols=4) 
    wp_mean_      = np.genfromtxt(fname, usecols=1) 


    test, = ax5.plot(rp_Mvir, wp_model_Mvir, lw=2, ls='--', color='k', label=r"$\rm M_{HI}>10^{10.2}M_{\odot}^{M_{vir}}$")
    ax5.fill_between(rp_Mvir, wp_mean_-jack_err_Mvir, wp_mean_+jack_err_Mvir, color='#bdbdbd')  

    leg1 = ax5.legend(handles=[test], frameon=False, ncol=1, fontsize=20, loc=1,numpoints=1 )
    ax5.add_artist(leg1)
    
    ttmp, = ax5.plot(guo_dm_rp, guo_dm_wp, lw=2.5, ls='-', color='r', label=r"$\rm Dark Matter$")
    ttmp1 = ax5.errorbar(rp_Mvir, wp_data, yerr=err, mec='b', linestyle='',capsize=5, 
                         mfc='none', mew=1.5, fmt="o", color='b', elinewidth=1.5, ms=8, label=r"$\rm Guo2017$")

    ax5.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax5.set_xscale("log")
    ax5.set_yscale("log")
    ax5.set_xlim(0.1, 30)
    ax5.set_ylim(1, 900)
    ax5.set_yticks([1, 10, 100])
    ax5.set_yticklabels(['1', '10', '100'])
    ax5.set_xticks([0.1, 1, 10])
    ax5.set_xticklabels(['0.1', '1', '10'])
    ax5.minorticks_on()
    
    leg2 = ax5.legend(handles=[ttmp, ttmp1], frameon=False, ncol=1, fontsize=20, loc=3,numpoints=2 )

    ax5.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax5.tick_params(axis='both', which='major', length=10,
           width=2, labelsize=16)
    ax5.spines['bottom'].set_linewidth(1.2)
    ax5.spines['left'].set_linewidth(1.2)
    ax5.spines['top'].set_linewidth(1.2)
    ax5.spines['right'].set_linewidth(1.2)
    plt.setp(ax5.get_yticklabels(),visible=False)
   
    plt.tight_layout()
    #fout = (plot_dir+'Compare_real_wp_sigma_{0:0.2f}_200Mpchinv_region.png'.format(sigma))
    fout = (plot_dir+'Jack2Cube_real_wp_sigma_{0:0.2f}_180Mpchinv_region.png'.format(sigma))
    fig.savefig(fout, bbox_inches='tight', dpi=600, edgecolor='none', frameon='False')
 
if __name__ == "__main__":
    main()



