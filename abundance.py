"""
functions used to calculate abundance functions, e.g. halo mass function, given a sample.
"""

import numpy as np
from scipy import optimize
from scipy import integrate
import sys
import matplotlib as mpl
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
# Fast tabular data reading module
import datatable as dt

#---------------- Global Values ---------------------------
from hmf import MassFunction
from hmf import cosmo
from astropy.cosmology import LambdaCDM
from tqdm import tqdm



__author__ = "Sandeep Rana"

__credits__ = [""]

__license__ = "GPL"

__version__ = "2.0.0"

__maintainer__ = "Sandeep Rana"

__email__ = "sandeepranaiiser@gmail.com"




# Multidark 2 cosmology hmf model
OmegaMatter = 0.307115
OmegaLambda = 0.692885
H0 = 0.6777*100.
OmegaBaryon = 0.048206

new_model   = LambdaCDM(H0 = 0.6777*100, Om0 = OmegaMatter, 
            Tcmb0=2.73, Ob0= OmegaBaryon, Ode0=OmegaLambda)

#


mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'
mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'
#set some plotting parameters
#plt.style.use("classic")
#plt.style.use("ggplot")
#plt.style.use("seaborn-bright")
plt.style.use("fast")


#----------------------------------------------------------
#ALFA parameter 2DSWL Saili's
def h_c(c):
    return 100./c
    
alfalfa_param_dict = {
    'alfa70':{
        'alpha_ALFA': -1.28935,       # \pm 0.02 
        'log_M_star_ALFA': 9.98044,    #-2*np.log10(h_c(70)) # \pm 0.2 h_70^-2
        'phi_star_ALFA':0.004468  #*h_c(70)**3 #\pm 0.8 \pm 0.6 h_70^3 Mpc^-3 dex^-1
    },
    'alfa40':{
    'alpha_ALFA': -1.35,       # \pm 0.02 
    'log_M_star_ALFA': 9.96,   #-2*np.log10(h_c(70)) # \pm 0.2 h_70^-2
    'phi_star_ALFA': 0.00534  #*h_c(70)**3 #\pm 0.8 \pm 0.6 h_70^3 Mpc^-3 dex^-1
    }
}
   
#----------------------------------------------------------

def HI_Mass_func(MHI, mode='alfa70', ALFA=True):

    if not ALFA:
        MHI_hat = MHI - log_M_star_Hipass
        MHI_hat_ratio = 10**MHI/10**log_M_star_Hipass

        return ( np.log10(phi_star_Hipass * np.log(10.)) + 
                 (1+alpha_Hipass)*MHI_hat - MHI_hat_ratio * np.log10(np.exp(1)))
    else:
        mf_p_dict = alfalfa_param_dict[mode]
        MHI_hat = MHI - mf_p_dict['log_M_star_ALFA']
        MHI_hat_ratio = 10**MHI/10**mf_p_dict['log_M_star_ALFA']
        return ( np.log10(mf_p_dict['phi_star_ALFA'] * np.log(10.)) + 
                 (1+mf_p_dict['alpha_ALFA'])*MHI_hat - MHI_hat_ratio * np.log10(np.exp(1)))


#------------------------------------------------------------------------


def _is_monotonic(x,y):
    """
    Is the tabulated function y(x) monotonically increasing or decreasing?
    """
    sorted_inds = np.argsort(x)
    x = x[sorted_inds]
    y = y[sorted_inds]
    
    N_greater = 0
    N_less = 0
    for i in range(1,len(x)):
       if y[i]>y[i-1]: N_greater = N_greater+1
       else: N_less = N_less+1
    
    if (N_greater==len(x)-1) | (N_less==len(x)-1):
        return True
    else: return False


#----------------------------------------------------------


def lognormal_dist(logMHI, Mhalo, func, scatter):
       
    """
    Conditional luminostiy function in the 
    form of log normal distribution.
    """
    #scatter = 0.3
    temp = logMHI - func(Mhalo)
    return (1./np.sqrt(2*np.pi*scatter**2)) * np.exp(-temp**2/2./scatter**2)
     
#------------------------------------------------------------------------


def _is_reversed(x, y):
    """
    Does the tabulated function y(x) decrease for increasing x?
    """
    sorted_inds = np.argsort(x)
    x = x[sorted_inds]
    y = y[sorted_inds]
    
    return  np.all(np.diff(y)<0)

#------------------------------------------------------------------------

def get_cumulative(f, bins, reverse=True):
    
    """
    calculate the cumulative abundance function by 
    integrating the differential abundance 
    function.

    Parameters
    ----------
    Returns
    ----------
    """
    
    #determine the normalization.
    # Initial point
    if reverse==True:
        N0 = np.float64(f(bins[-1])) * (bins[-2]-bins[-1])
    else:
        N0 = float(f(bins[0]))*(bins[1]-bins[0])

    #if the function is monotonically decreasing, integrate in the opposite direction
    if reverse==True:
        N_cum = integrate.cumtrapz(f(bins[::-1]), bins[::-1], initial=N0)
        N_cum = N_cum[::-1]*(-1.0)
    else:
        N_cum = integrate.cumtrapz(f(bins), bins, initial=N0)

    #interpolate result to get a functional form
    N_cum_func = _spline(bins, N_cum, k=1)
    
    return N_cum_func


#------------------------------------------------------------------------


def AM_nonparam(G, dn_dG, H, dn_dH, spread, frac, mstar, beta, analytic=True, P_x=False, 
                     HI_selected_MF=False, H_min=None, H_max=None, nH=100, 
                     tol_err=0.001):

    """
    Abundance match galaxy property 'G' to halo property 'H'. 
    
    Iteratively deconvolve <G(H)> from dn/dH.
    
    Tabulated abundances of galaxies and haloes are required.
    
    Determines the mean of P(G_gal | H_halo), given the centered distribution, 
    i.e. all other moments.
    
    In detail P should be of the form: P_x(y, mu_xy(y)), where y 
    is a property of the halo and mu_xy(y) is the mean of the distribution 
    as a function of y. This function simply solves for the form of mu_xy.
    
    Parameters
    ==========
    dn_dx: array_like
        galaxy abundance as a function of property 'x'
    
    x: array_like
    
    dn_dy: array_like
        halo abundance as a function of property 'y'
    
    y: array_like
    
    P_x: function
        The centered probability distribution of P(x_gal | y_halo).  
	If this is None, traditional abundance matching is 
	done with no scatter.
    
    y_min: float
        minimum value to determine the form of P(x|y) for
    
    y_max: float
        maximum value to determine the form of P(x|y) for
    
    ny: int
        number of points used to sample the first moment in the range [y_min,y_max]]
    
    tol_err: float, optional
         stop the calculation when <x(y)> changes by less than tol_err
    
    Returns
    =======
    mu : function 
         first moment of P(x| y)
    """

    #process input parameters
    
    G     = np.array(G, dtype=np.float64) # In log10
    dn_dG = np.array(dn_dG, dtype=np.float64)

    if H_min==None:
	    H_min = np.amin(H)
    if H_max==None:
	    H_min = np.amax(H)
    if H_min > H_max:
	    raise ValueError("H_min must be less than H_max!")


    if not analytic:
        H     = np.array(H, dtype=np.float64) # log mass
        dn_dH = np.array(dn_dH, dtype=np.float64) # dndlog10m
    #check halo abundance range inputs
    else:
        H      = np.array(H, dtype=np.float64)
        deltax = (H_max- H_min)/float(len(H))
        h_sampl = MassFunction(z=0.0,Mmin=H_min, Mmax=H_max, dlog10m=deltax, 
	    	                   hmf_model="Bhattacharya", cosmo_model=new_model, 
	    	                   #hmf_model="SMT", cosmo_model=new_model, 
                               sigma_8=0.8228, n=0.96)
        dn_dH  = (h_sampl.dndlog10m * 0.6777**3)
        
    if HI_selected_MF:    
        tempp = 1. +  (10**H/10**mstar)**beta
        dn_dH  = dn_dH*(frac/tempp)


    #my_cosmo = cosmo.Cosmology(new_model)
    #define y samples used when integrating dn_dH*P(g|H)dH. 
    #This needs to be finely spaced enough to get accurate 
    #integrals of P(G|H)dH. This can get very narrow for small
    #scatter and/or for steep G(H) relations.

    #define y samples of x(y). This will be the number of parameters to minimize when 
    #solving for x(y).  More parameters will slow the code down!
    deltax = (H_max- H_min)/float(nH)
    H_sampling = np.arange(H_min, H_max+deltax, deltax)


    #step 0: preliminary work
    #Put in order so the independent variable is monotonically increasing

    #Galaxies
    inds = np.argsort(G)
    G = G[inds]
    dn_dG = dn_dG[inds]
    #Halos
    inds = np.argsort(H)
    H = H[inds]
    dn_dH = dn_dH[inds]

#-------------------------------------------------------------------

    #check direction of increasing number density.  Usually this is reversed.
    #This affects the sign of integrations, and how interpolations are implemented.
    
    reverse_G = _is_reversed(G, dn_dG)
    reverse_H = _is_reversed(H, dn_dH)


    #print("Galaxy property decreases as dn_dG increases: ", reverse_G)
    #print("Halo mass decreases as dn_dH increases: ", reverse_H)

    #check numerical values of dn_dx and dn_dy
    #trim down ranges if they return numbers they are too small


    keep = (dn_dG > 10.0**(-20.0))

    if not np.all(keep):
        #print("Triming G-range to keep number densities above 1e-20")
        G = G[keep]
        dn_dG = dn_dG[keep]

    keep = (dn_dH>10.0**(-20.0))
    if not np.all(keep):
        #print("Triming H-range to keep number densities above 1e-20")
        H = H[keep]
        dn_dH = dn_dH[keep]


    #convert tabulated abundance functions into function objects using interpolation
    #interpolation.

    #galaxy abundance function
    ln_dn_dG_func = _spline(G, np.log10(dn_dG), k=1)
    dn_dG_func    = _spline(G, dn_dG, k=1)

    #halo abundance function
    ln_dn_dH_func = _spline(H, np.log10(dn_dH), k=1)
    dn_dH_func    = _spline(H, dn_dH, k=1)

    #calculate the cumulative abundance functions
    N_cum_halo_func = get_cumulative(dn_dH_func, H, reverse = reverse_H)
    N_cum_gal_func  = get_cumulative(dn_dG_func, G, reverse = reverse_G)


    #galaxy cumulative abundance function number density range must span that of the halo 
    #abundance function for abundance matching to be possible.
    N_min = min(N_cum_halo_func(H_min), N_cum_halo_func(H_max)) # This is to taken into account for 
    N_max = max(N_cum_halo_func(H_min), N_cum_halo_func(H_max)) # Galaxy luminosity 

#    print("maximum halo cumulative abundnace: {0}".format(N_max))
#    print("mimimum halo cumulative abundnace: {0}".format(N_min))
#    print("maximum galaxy cumulative abundnace: {0}".format(np.amax(N_cum_gal_func(G))))
 #   print("mimimum galaxy cumulative abundnace: {0}".format(np.amin(N_cum_gal_func(G))))

#--------------------------------------------------------------------------------------------

    # Now to do abundance matching we need to calculate the inverse cumulative abundances

    if reverse_G==True:
        N_cum_gal_inv  = _spline(N_cum_gal_func(G)[::-1], G[::-1], k=1)
    else:
        N_cum_gal_inv  = _spline(N_cum_gal_func(G), G, k=1)


    if reverse_H==True:    
        N_cum_halo_inv = _spline(N_cum_halo_func(H)[::-1], H[::-1], k=1)
    else:
        N_cum_halo_inv = _spline(N_cum_halo_func(H), H, k=1)


    #calculate pivot point below this point, the galaxy abundance function will be 
    #incomplete given the halo value range under consideration.

    G_pivot = N_cum_gal_inv(N_cum_halo_func(H_min))
    #print("minimum G value in range: {0}".format(G_pivot))

    #discard the galaxy abundance function below this point.
    keep = (G>G_pivot)
    G = G[keep]


    ##########################################################
    #step 1: solve for first moment in the absence of scatter.  This is abundance matching
    #with no scatter.
    G_H_ini  = N_cum_gal_inv(N_cum_halo_func(H_sampling))
    G_H_ini = _spline(H_sampling, G_H_ini, k=1)
    
    """    
    plt.plot(H, G_H_ini(H), 'r-', linewidth=1.8)
    plt.xlim(9, 16)
    plt.ylim(6, 12)
    plt.show()
    """
    if P_x==False:
        return G_H_ini, dn_dH_func
        
        
    ##########################################################
    #step 2: get second estimate of the first moment by integrating to get the galaxy
    #abundance function of haloes and solve for N_halo(x) = N_halo(y) to get a second
    #estimate of x(y)
    
    #do integral
    dn_halo_dx = np.zeros(len(G), dtype=np.float64)
    for i in range(len(G)):
        lnPx      = lognormal_dist(G[i], H, G_H_ini, spread)
        integrand = lnPx*dn_dH_func(H)
        dn_halo_dx[i] = integrate.simps(integrand, H)
    
    #get galaxy abundance function function (number of haloes as a function of x)
    dn_halo_dx = _spline(G, dn_halo_dx, k=1) 
    
    #get new cumulative function
    N_cum_halo_G = get_cumulative(dn_halo_dx, G, reverse = reverse_G)
    """ 
    plt.plot(G, np.log10(N_cum_halo_G(G)), 'r-', linewidth=1.8)
    plt.plot(G, np.log10(N_cum_gal_func(G)), 'b-', linewidth=1.8)
    plt.xlim(6, 12)
    plt.show()
    """


    #get inverse cumulative mass function in our case solving convolution 
    #integral gives us new HI mass function 

    if reverse_G==True:
        N_cum_halo_G_inv = _spline(N_cum_halo_G(G)[::-1], G[::-1], k=1)
    else:
        N_cum_halo_G_inv = _spline(N_cum_halo_G(G), G, k=1)


    # Now after abundance match get the transformation between G and G_trans
    G_trans = N_cum_gal_inv(N_cum_halo_G(G))
    G_trans = _spline(G, G_trans, k=1)
    
    #get x2(y).  This is our second estimate.
    G_H_upd =  G_trans(G_H_ini(H_sampling))
    G_H_upd =  _spline(H_sampling, G_H_upd, k=1)
       
    G_H_upd_i = G_H_upd #previous iteration of the x(y)

    stop = False
    iteration = 0
    max_iterations=1000
    
    pbar = tqdm(total=700)
    
    while stop==False: 
	    #do integral
        dn_halo_dx = np.zeros(len(G), dtype=np.float64)
        for i in range(len(G)):
	        lnPx      = lognormal_dist(G[i], H, G_H_upd_i, spread)
	        integrand = lnPx*dn_dH_func(H)
	        dn_halo_dx[i] = integrate.simps(integrand, H)
	
        #get galaxy abundance function function (number of haloes as a function of x)
        dn_halo_dx = _spline(G, dn_halo_dx, k=1) 
	
        #get new cumulative function
        N_cum_halo_G = get_cumulative(dn_halo_dx, G, reverse = reverse_G)
	
        if reverse_G==True:
            N_cum_halo_G_inv = _spline(N_cum_halo_G(G)[::-1], G[::-1], k=1)
        else:
            N_cum_halo_G_inv = _spline(N_cum_halo_G(G), G, k=1)

        # Now after abundance match get the transformation between G and G_trans
        G_trans = N_cum_gal_inv(N_cum_halo_G(G))
        G_trans = _spline(G, G_trans, k=1)
    
        #This is our second estimate.
        G_H_upd_ii = G_H_upd_i
        G_H_upd_i =  G_trans(G_H_upd_i(H_sampling))
        G_H_upd_i =  _spline(H_sampling, G_H_upd_i, k=1)

        err = np.amax(np.fabs(G_H_upd_i(H_sampling) - G_H_upd_ii(H_sampling))/G_H_upd_i(H_sampling))
        
        if err<=tol_err:
            break
        iteration = iteration+1
        if iteration==max_iterations:
            print("specified tolerance not reached!")
            break
            
        pbar.update(1)

    pbar.close()
    return G_H_upd_i, G_H_ini


#------------------------------------------------------------------------
def schechter_function(x, phistar, alpha, mstar):
    """
    return a model fitting function, and a function class
    """

    norm = np.log(10.0)*phistar
    val = norm*(10.0**((x-mstar)*(1.0+alpha)))*np.exp(-10.0**(x-mstar))
    return val


def fit_abundance_halo(x, weights, bins, xlog=True, fit_type='schechter', p=None, show_fit=True):
    """
    given objects with property 'x' and weights, fit a functional form to the raw 
    abundances.
    
    Parameters
    ==========
    x: array_like
    
    weights: array_like
    
    bins: array_like
    
    xlog: boolean, optional
        If True, use log values of x. 
    
    fit_type: string, optional
        type of function to use for fit
    
    p: dict, optional
        initial guesses of parameters in fit
    
    show_fit: boolean, optional
        plot fit
    
    Returns
    =======
    dndx: function
    
    """
    from astropy.modeling import fitting
    import warnings
    warnings.filterwarnings("ignore")
    import lmfit
    

    #get empirical measurements of the abundance function
    dn, xedges, err = raw_abundance(x, weights, bins, xlog=xlog, show=False)
    
    schechter_model = lmfit.Model(schechter_function)

    # create Parameters, giving initial values
    params = schechter_model.make_params(phistar=p['phi1'], alpha=p['alpha1'], mstar=p['x1'])
    params['phistar'].min = 0
    params['alpha'].min = -2.5
    params['mstar'].min = 12.5


    # do fit, print result
    result = schechter_model.fit(dn, params, x=xedges, weights=1./dn)
    print(result.fit_report())
   
    if show_fit:

        SAM_H = np.genfromtxt("../sailis-data/new_hmf.dat", usecols=0)
        SHI_halo_AM = np.genfromtxt("../sailis-data/new_hmf.dat", usecols=1)


        NBINS = 100
        xlim              = [9, 15.5]
        deltaM            = (xlim[1]-xlim[0])/np.float64(NBINS)
        logMbins = np.arange(xlim[0]+0.5*deltaM, xlim[1], deltaM)
        test_func = schechter_function(logMbins, result.params['phistar'].value,
        result.params['alpha'].value, result.params['mstar'].value)
        
        plt.figure(1, figsize=(8,6))

        gs = gridspec.GridSpec(1, 1)#, hspace=0.0, height_ratios=[3,1])
        ax1 = plt.subplot(gs[0])
            
        #ax1.plot(xedges, result.best_fit, linestyle='--', linewidth=2, label=r'$\rm Best-fit$')
        ax1.plot(logMbins, test_func, linestyle='--', linewidth=2, label=r'$\rm Best-fit1$')

        #ax1.plot(SAM_H, 10**SHI_halo_AM, linestyle=':', linewidth=2, label=r'$\rm Saili$')
        

        ax1.errorbar(xedges, dn, yerr=err, alpha=0.7, linestyle='',capsize=2,
                      mew=1.2, fmt="o",color='r', ecolor='r', elinewidth=1.5, ms=4,label=r"$\rm M_{vir}^{MDPL2}$")

        ax1.axvline(x=11, linestyle='--', linewidth=2, color='peru', 
                    label=r"")
                
        #ax1.set_ylabel(r'$\rm{dn/d}\log_{10} \rm{M}$ ($\rm{h}^{3}$ $\rm{Mpc}^{-3}$)', fontsize=20)
        ax1.set_ylabel(r'$\rm{dn/d}\log_{10} \rm{M}$ ($\rm{Mpc}^{-3}$)', fontsize=20)
        #ax1.set_xlabel(r'$\rm{M}$ [$\rm{M}_\odot$ $\rm{h}^{-1}$]', fontsize=20)
        ax1.set_xlabel(r'$\rm{M}$ [$\rm{M}_\odot$]', fontsize=20)
        #ax1.set_xscale("log")
        ax1.set_yscale("log")
        ax1.set_xlim(9, 16)
        ax1.set_ylim(10**(-6), 10**1)
        ax1.minorticks_on()
        ax1.legend(frameon=False, ncol=2, fontsize=16, loc=1)
        ax1.tick_params(axis='both', which='minor', length=5,
              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        plt.tight_layout()
        plt.show()


    
    dn_dh_func = _spline(xedges, dn, k=1)

    return result, xedges, dn, err, dn_dh_func


def raw_abundance(x, weights, bins, xlog=True, monotonic_correction=False, show=False):
    """
    given objects with property 'x' and weights, return tabulated abundances.
    
    Parameters
    ==========
    x: array_like
    
    weights: array_like
    
    bins: array_like
    
    xlog: boolean, optional
        If True, use log values of x. 
    
    monotonic_correction: boolean, optional
        If True, attempt to correct for minor non-monotonicity
    
    show: boolean, optional
        plot abundance function 
    
    Returns
    =======
    dndx: dn, x, err
    """
    
    if xlog==True:
        x = np.log10(x)
    
    if np.shape(weights)==():
        weights = np.array([weights]*len(x))
    
    n = np.histogram(x, bins, weights=weights)[0]
    bin_centers = (bins[:-1]+bins[1:])/2.0
    dx = bins[1:]-bins[:-1]

    dn = n/dx
    
    raw_counts = np.histogram(x, bins=bins)[0]
    err = (1.0/np.sqrt(raw_counts))*dn
    
    #remove zeros
    keep = (raw_counts>0)
    dn = dn[keep]
    bin_centers = bin_centers[keep]
    err = err[keep]
    
    if show==True:
        fig = plt.figure(figsize=(3.3,3.3))
        fig.subplots_adjust(left=0.2, right=0.85, bottom=0.2, top=0.9)
        plt.errorbar(bin_centers, dn, yerr=err,fmt='.')
        plt.yscale('log')
        plt.show()
    
    
    return dn, bin_centers, err


####Utility Functions#####################################################################
def _get_fitting_function(name, phi1, alpha1, x1, x):
    """
    return a model fitting function, and a function class
    """

#    from astropy.modeling.models import custom_model
#    import custom_utilities as cu
#    @custom_model
    if name=='schechter':
    #@schechter_model  
    #def schechter_model(x, phi1=1, x1=1, alpha1=-1):
        norm = np.log(10.0)*phi1
        val = norm*(10.0**((x-x1)*(1.0+alpha1)))*np.exp(-10.0**(x-x1))
        return val

    """
    @custom_model
    def double_schechter_model(x, phi1=1, phi2=1, x1=1, x2=1, alpha1=-1, alpha2=-1):
        norm = np.log(10.0)
        val = norm *\
              (np.exp(-10.0**(x - x1)) * 10.0**(x - x1) *\
                   phi1 * (10.0**((x - x1) * alpha1)) +\
               np.exp(-10.0**(x - x2)) * 10.0**(x - x2) *\
                   phi2 * (10.0**((x - x2) * alpha2)))
        return val
    if name=='schechter':
        return schechter_model
    #elif name=='double_schechter':
    #    return double_schechter_model
    else:
        raise ValueError("fitting function not avalable.")

    """


