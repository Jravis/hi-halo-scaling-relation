from __future__ import print_function, division
import numpy as np
import sys
import time
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
from colorama import Fore

from tqdm import tqdm, trange
import argparse
from astropy.cosmology import LambdaCDM
import datatable as dt


mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rc('font', weight='bold')
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Ubuntu'
mpl.rcParams['font.monospace'] = 'Ubuntu Mono'
mpl.rcParams['axes.labelweight'] = 'bold'

mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'

import palettable

#set some plotting parameters
plt.style.use("classic")
#plt.style.use("ggplot")
#plt.style.use("seaborn-paper")
#plt.style.use("fast")


#---------------- Global Values ---------------------------
guo_dir_name='../../HI-Halo_scatter_relation/HI_mock_cat/clust_data/'
cutoff_name = np.array(['9pt4', '9pt6', '9pt8', '10pt0', '10pt2'])

def gen_chi_sqr(data, model, err):
    
    """  
    Returns the chi-square error statistic as the sum of squared errors between  
    Ydata(i) and Ymodel(i). If individual standard deviations (array sd) are supplied,   
    then the chi-square error statistic is computed as the sum of squared errors  
    divided by the standard deviations. Inspired on the IDL procedure linfit.pro.  
    See http://en.wikipedia.org/wiki/Goodness_of_fit for reference.  
    """  
    # Chi-square statistic (Bevington, eq. 6.9)  
    chisq = np.sum( ((data-model)/err)**2 )  
    dof=4
    nu = data.size - dof
    return chisq, chisq/nu


def get_tot_chi_red(dir_name, sigma, frac, mstar, beta):

    fname1    = (dir_name+'HI_alfa70_wp_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta))
    summ=0.0
    for j in range(5):
        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[0], sigma)
        wp_model =np.genfromtxt(fname, usecols=2) 
        wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[0], usecols=1) 
        err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[0], usecols=2) 
        chi, red_chi= gen_chi_sqr(wp_data, wp_model, err)
        summ+=red_chi
    
    return summ

#------------------------------------------------------------------
def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("sigma", type=float, 
    help='Input scatter e.g 0.25')

    args = parser.parse_args()
    sigma= args.sigma

    #dir_name = ('../pipeline_result_data_alfa70_seed_68927/clust_data/REAL/')
    dir_name = ('../pipeline_result_data_alfa70_Mpeak/clust_data/REAL/')

    frac  = np.genfromtxt("guo_SMDPL_best_fit_alfa70_Mpeak.txt", usecols=0)
    mstar = np.genfromtxt("guo_SMDPL_best_fit_alfa70_Mpeak.txt", usecols=1)
    beta  = np.genfromtxt("guo_SMDPL_best_fit_alfa70_Mpeak.txt", usecols=2)
    
    #frac = 0.45
    #mstar=13.25
    #beta = 0.66

    dirc ='/mnt/data1/sandeep/my_all_xanadu_codes/my_mesh_npairs/GFastCorr/examples/'
    guo_dm_rp = np.genfromtxt(dirc+"wp_guo.txt", usecols=0) 
    guo_dm_wp = np.genfromtxt(dirc+"wp_guo.txt", usecols=1) 


    tot_red_chi = get_tot_chi_red(dir_name, sigma, frac, mstar, beta)

    #------------------------------------------------------------------
    
    
    #plot_dir = '../pipeline_result_data_alfa70_seed_68927/clust_plots/'
    plot_dir = '../pipeline_result_data_alfa70_Mpeak/clust_plots/'
    print(plot_dir)
    props = dict(boxstyle='round', facecolor='none', linewidth=1.5)
    
    
    fname1    = (dir_name+'HI_alfa70_wp_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta))

    chi_tot=0.0
    fig= plt.figure(1, figsize=(12, 10))

    gs = gridspec.GridSpec(2, 3, wspace=0.000, hspace=0.000)
    ax1 = plt.subplot(gs[0, 0])
#    ax1.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)

    fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[0], sigma)
    
    rp =np.genfromtxt(fname, usecols=0) 
    wp_model =np.genfromtxt(fname, usecols=2) 
    wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[0], usecols=1) 
    err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[0], usecols=2) 
    chi, red_chi= gen_chi_sqr(wp_data, wp_model, err)
    chi_tot+=red_chi
    
    
    test, = ax1.plot(rp, wp_model, lw=3,ls='--', color='r', label=r"$\rm M_{HI}>10^{9.4}M_{\odot}$")
    leg1  = ax1.legend(handles=[test], frameon=False, ncol=1, fontsize=20, loc=2,numpoints=1 )
    ax1.add_artist(leg1)
    
    ttmp,  = ax1.plot(guo_dm_rp, guo_dm_wp, lw=2.5, ls='-', color='k', label=r"$\rm Dark Matter$")
    ttmp1 =  ax1.errorbar(rp, wp_data, yerr=err, mec='b', linestyle='',capsize=5, mfc='none', mew=1.5, fmt="o", 
                color='b', elinewidth=1.5, ms=8, label=r"$\rm Guo2017$")

    leg2 = ax1.legend(handles=[ttmp, ttmp1], frameon=False, ncol=1, fontsize=18, loc=3,numpoints=2 )

    
   # ax1.text(5, 40, r"$\rm f=%0.2f$" "\n" r"$\rm M_*=%0.2f$" "\n" r"$\beta=%0.2f$" "\n" r"$\sigma=%0.2f$"%(frac, mstar, beta, sigma), fontsize=18, bbox=props) 
    #ax1.text(2, 200,r"$\rm \chi^2_{red}=\frac{%0.2f}{%0.2f}$"%(chi, 8), fontsize=18)#, bbox=props) 

    ax1.set_ylabel(r'$\rm w_{p}(r_{p}) [h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax1.set_xscale("log")
    ax1.set_yscale("log")
    #ax1.set_xlim(0.1, 30)
    ax1.set_ylim(1, 1000)
    ax1.set_yticks([1, 10, 100])
    ax1.minorticks_on()
    plt.setp(ax1.get_xticklabels(),visible=False)
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)
    

    #------------------------------------------------------------------
    
    ax2 = plt.subplot(gs[0, 1],  sharey=ax1)
    ax2.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)

    fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[1], sigma)
    
    rp =np.genfromtxt(fname, usecols=0) 
    wp_model =np.genfromtxt(fname, usecols=2) 
    wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[1], usecols=1) 
    err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[1], usecols=2) 
    chi, red_chi= gen_chi_sqr(wp_data, wp_model, err)
    chi_tot+=red_chi
    
    test, = ax2.plot(rp, wp_model, lw=3,ls='--', color='r', label=r"$\rm M_{HI}>10^{9.6}M_{\odot}$")
    leg1  = ax2.legend(handles=[test], frameon=False, ncol=1, fontsize=20, loc=1,numpoints=1 )
    ax2.add_artist(leg1)
    
    ttmp,  = ax2.plot(guo_dm_rp, guo_dm_wp, lw=2.5, ls='-', color='k', label=r"$\rm Dark Matter$")
    ttmp1 =  ax2.errorbar(rp, wp_data, yerr=err, mec='b', linestyle='', mfc='none',capsize=5, mew=1.5, fmt="o", 
                color='b', elinewidth=1.5, ms=8, label=r"$\rm Guo2017$")

    leg2 = ax2.legend(handles=[ttmp, ttmp1], frameon=False, ncol=1, fontsize=18, loc=3,numpoints=2 )

    #ax2.text(2, 200,r"$\rm \chi^2_{red}=\frac{%0.2f}{%0.2f}$"%(chi, 8), fontsize=18)#, bbox=props) 
    
    ax2.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax2.set_xscale("log")
    ax2.set_yscale("log")
    #ax2.set_title(r"$\rm \chi^2_{red-tot}=%0.2f$"%(tot_red_chi), fontsize=18,  pad=10)
    #ax2.set_xlim(0.1, 30)
    ax2.minorticks_on()
    ax2.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax2.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax2.spines['bottom'].set_linewidth(1.2)
    ax2.spines['left'].set_linewidth(1.2)
    ax2.spines['top'].set_linewidth(1.2)
    ax2.spines['right'].set_linewidth(1.2)
    plt.setp(ax2.get_yticklabels(),visible=False)
    plt.setp(ax2.get_xticklabels(),visible=False)


    #------------------------------------------------------------------
    
    ax3 = plt.subplot(gs[1, 0],  sharey=ax1)
    ax3.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
#    ax2.set_prop_cycle('color', palettable.cartocolors.qualitative.Bold_4.mpl_colors)
#    ax2.set_prop_cycle('color', palettable.tableau.ColorBlind_10.mpl_colors)
    
    fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[2], sigma)
    
    rp =np.genfromtxt(fname, usecols=0) 
    wp_model =np.genfromtxt(fname, usecols=2) 
    wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[2], usecols=1) 
    err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[2], usecols=2) 
    chi, red_chi= gen_chi_sqr(wp_data, wp_model, err)
    chi_tot+=red_chi
    
    
    test, = ax3.plot(rp, wp_model, lw=3,ls='--', color='r', label=r"$\rm M_{HI}>10^{9.8} M_{\odot}$")
    leg1  = ax3.legend(handles=[test], frameon=False, ncol=1, fontsize=20, loc=1,numpoints=1 )
    ax3.add_artist(leg1)
    
    ttmp,  = ax3.plot(guo_dm_rp, guo_dm_wp, lw=2.5, ls='-', color='k', label=r"$\rm DarkMatter$")
    ttmp1 =  ax3.errorbar(rp, wp_data, yerr=err, mec='b', linestyle='',capsize=5, mfc='none', mew=1.5, fmt="o", 
                color='b', elinewidth=1.5, ms=8, label=r"$\rm Guo2017$")

    leg2 = ax3.legend(handles=[ttmp, ttmp1], frameon=False, ncol=1, fontsize=20, loc=3,numpoints=2 )

 
    #ax3.text(2, 200,r"$\rm \chi^2_{red}=\frac{%0.2f}{%0.2f}$"%(chi, 8), fontsize=18)#, bbox=props) 
    
    ax3.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax3.set_ylabel(r'$\rm w_{p}(r_{p}) [h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax3.set_xscale("log")
    ax3.set_yscale("log")
    ax3.set_xlim(0.1, 30)
    ax3.minorticks_on()
    ax3.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax3.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax3.spines['bottom'].set_linewidth(1.2)
    ax3.spines['left'].set_linewidth(1.2)
    ax3.spines['top'].set_linewidth(1.2)
    ax3.spines['right'].set_linewidth(1.2)
    
    #------------------------------------------------------------------

    ax4 = plt.subplot(gs[1, 1], sharey=ax3)
    ax4.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
    
    fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[3], sigma)
    
    rp =np.genfromtxt(fname, usecols=0) 
    wp_model =np.genfromtxt(fname, usecols=2) 
    wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[3], usecols=1) 
    err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[3], usecols=2) 
    chi, red_chi= gen_chi_sqr(wp_data, wp_model, err)
    chi_tot+=red_chi
    
    
    test, = ax4.plot(rp, wp_model, lw=3,ls='--', color='r', label=r"$\rm M_{HI}>10^{10.0} M_{\odot}$")
    leg1 = ax4.legend(handles=[test], frameon=False, ncol=1, fontsize=20, loc=1,numpoints=1 )
    ax4.add_artist(leg1)
    
    ttmp,  = ax4.plot(guo_dm_rp, guo_dm_wp, lw=2.5, ls='-',color='k', label=r"$\rm DarkMatter$")
    ttmp1 =  ax4.errorbar(rp, wp_data, yerr=err,mec='b',  linestyle='',capsize=5, mfc='none', mew=1.5, fmt="o", 
               color='b',  elinewidth=1.5, ms=8, label=r"$\rm Guo2017$")

    leg2 = ax4.legend(handles=[ttmp, ttmp1], frameon=False, ncol=1, fontsize=20, loc=3,numpoints=2 )

    #ax4.text(2, 200,r"$\rm \chi^2_{red}=\frac{%0.2f}{%0.2f}$"%(chi, 8), fontsize=18)#, bbox=props) 
    
    ax4.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax4.set_xscale("log")
    ax4.set_yscale("log")
    ax4.set_xlim(0.1, 30)
    ax4.set_ylim(1, 1000)
    ax4.set_yticks([1, 10, 100])
    ax4.minorticks_on()
    
    plt.setp(ax4.get_yticklabels(),visible=False)
    ax4.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax4.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax4.spines['bottom'].set_linewidth(1.2)
    ax4.spines['left'].set_linewidth(1.2)
    ax4.spines['top'].set_linewidth(1.2)
    ax4.spines['right'].set_linewidth(1.2)
    
   
    #------------------------------------------------------------------
    
    ax5 = plt.subplot(gs[1, 2],  sharey=ax3)
    ax5.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
    fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[4], sigma)
    
    rp =np.genfromtxt(fname, usecols=0) 
    wp_model =np.genfromtxt(fname, usecols=2) 
    
    wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[4], usecols=1) 
    err =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[4], usecols=2) 
    chi, red_chi= gen_chi_sqr(wp_data, wp_model, err)
    chi_tot+=red_chi
    

    test, = ax5.plot(rp, wp_model, lw=3,ls='--', color='r', label=r"$\rm M_{HI}>10^{10.2} M_{\odot}$")
    leg1 = ax5.legend(handles=[test], frameon=False, ncol=1, fontsize=20, loc=1,numpoints=1 )
    ax5.add_artist(leg1)
    
    ttmp,  =ax5.plot(guo_dm_rp, guo_dm_wp, lw=2.5, ls='-', color='k', label=r"$\rm Dark Matter$")
    ttmp1 = ax5.errorbar(rp, wp_data, yerr=err, mec='b', linestyle='',capsize=5, mfc='none', mew=1.5, fmt="o",
                 color='b', elinewidth=1.5, ms=8, label=r"$\rm Guo2017$")

    #ax5.text(2, 200,r"$\rm \chi^2_{red}=\frac{%0.2f}{%0.2f}$"%(chi, 8), fontsize=18)#, bbox=props) 
    
    ax5.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    ax5.set_xscale("log")
    ax5.set_yscale("log")
    ax5.set_xlim(0.1, 30)
    ax5.minorticks_on()
    
    leg2 = ax5.legend(handles=[ttmp, ttmp1], frameon=False, ncol=1, fontsize=20, loc=3,numpoints=2 )

    ax5.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax5.tick_params(axis='both', which='major', length=10,
           width=2, labelsize=16)
    ax5.spines['bottom'].set_linewidth(1.2)
    ax5.spines['left'].set_linewidth(1.2)
    ax5.spines['top'].set_linewidth(1.2)
    ax5.spines['right'].set_linewidth(1.2)
    plt.setp(ax5.get_yticklabels(),visible=False)
   
    plt.tight_layout()
    fout = (plot_dir+'_real_wp_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}_sigma_{3:0.2f}.png'.format(frac, mstar, beta, sigma))
    fig.savefig(fout, bbox_inches='tight', dpi=600, edgecolor='none', frameon='False')
    
#    plt.show()


#----------------------------------------------------------------------------------------

    sigma_cut = np.array([0.00, 0.15, 0.20, 0.25 ])
    
   
    for i in range(5):
        
        fig= plt.figure(2, figsize=(6, 6))
        gs = gridspec.GridSpec(1, 1)#, wspace=0.000, hspace=0.000)
        ax1 = plt.subplot(gs[0, 0])
 
        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], sigma_cut[0])
        rp =np.genfromtxt(fname, usecols=0) 
        
        wp_model =np.genfromtxt(fname, usecols=2) 
        wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[i], usecols=1) 
        
        delta1 = np.abs((wp_model-wp_data)/wp_data)*100
        
        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], sigma_cut[1])
        
        wp_model =np.genfromtxt(fname, usecols=2) 
        wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[i], usecols=1) 
        
        delta2 = np.abs((wp_model-wp_data)/wp_data)*100

        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], sigma_cut[2])
        
        wp_model =np.genfromtxt(fname, usecols=2) 
        wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[i], usecols=1) 
        
        delta3 = np.abs((wp_model-wp_data)/wp_data)*100

        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], sigma_cut[3])
        wp_model =np.genfromtxt(fname, usecols=2) 
        wp_data =np.genfromtxt(guo_dir_name+'guo_wp_gt_%s.dat'%cutoff_name[i], usecols=1) 
        
        delta4 = np.abs((wp_model-wp_data)/wp_data)*100


        ax1.plot(rp, delta1, lw=3,ls='', mew=1.5, ms=8, marker='o', mfc='none', mec='r', label=r"$\rm \sigma=0.0$")
        ax1.plot(rp, delta2, lw=3,ls='', mew=1.5, ms=8, marker='s', mfc='none', mec='g', label=r"$\rm \sigma=0.15$")
        ax1.plot(rp, delta3, lw=3,ls='', mew=1.5, ms=8, marker='D', mfc='none', mec='k', label=r"$\rm \sigma=0.20$")
        ax1.plot(rp, delta4, lw=3,ls='', mew=1.5, ms=8, marker='d', mfc='none', mec='b', label=r"$\rm \sigma=0.25$")
        
        ax1.legend(frameon=False, ncol=1, fontsize=16, loc=1,numpoints=1 )
        ax1.axhline(y=0.0, color='orangered', ls='--', lw=3.0) 
        
        ax1.set_ylabel(r'$\rm Error_{\%}$', fontsize=18, fontweight='bold')
        ax1.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=18, fontweight='bold')
        ax1.set_xscale("log")
        ax1.set_ylim(-10, 80)
        ax1.set_yticks([0, 20, 40, 60, 80])
        #ax3.set_yscale("log")
        ax1.set_xlim(0.1, 30)
        ax1.minorticks_on()
        ax1.tick_params(axis='both', which='minor', length=5,
              width=2, labelsize=14)
        ax1.tick_params(axis='both', which='major', length=8,
               width=2, labelsize=14)
        ax1.spines['bottom'].set_linewidth(1.2)
        ax1.spines['left'].set_linewidth(1.2)
        ax1.spines['top'].set_linewidth(1.2)
        ax1.spines['right'].set_linewidth(1.2)
        
           
        plt.tight_layout()
        fout = (plot_dir+'_real_wp_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}_gt_{3:s}_perc_error.png'.format(frac, mstar, beta, cutoff_name[i]))
        fig.savefig(fout, bbox_inches='tight', dpi=600, edgecolor='none', frameon='False')
        plt.close()
        


#----------------------------------------------------------------------------------------        
    """ 
    sigma_cut = np.array([0.00, 0.15, 0.20, 0.25 ])
    amp_9pt4 = np.zeros(4, dtype=np.float64)
    amp_9pt6 = np.zeros(4, dtype=np.float64)
    amp_9pt8 = np.zeros(4, dtype=np.float64)
    amp_10pt0 = np.zeros(4, dtype=np.float64)
    amp_10pt2 = np.zeros(4, dtype=np.float64)

    fig= plt.figure(2, figsize=(10, 5))
    gs = gridspec.GridSpec(1, 2, wspace=0.000)
    ax1 = plt.subplot(gs[0, 0])

    ax1.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
    for i in range(4):

        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%('9pt4', sigma_cut[i])
        rp =np.genfromtxt(fname, usecols=0) 
        wp_model =np.genfromtxt(fname, usecols=2)
        wp_func = _spline(rp, wp_model, k=1)
        amp_9pt4[i] = wp_func(0.3)
        
        
        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%('9pt6', sigma_cut[i])
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_9pt6[i] = wp_func(0.3)
     

        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%('9pt8', sigma_cut[i])
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_9pt8[i] = wp_func(0.3)
     
        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%('10pt0', sigma_cut[i])
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_10pt0[i] = wp_func(0.3)
     
        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%('10pt2', sigma_cut[i])
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_10pt2[i] = wp_func(0.3)
 

    ax1.plot(sigma_cut, amp_9pt4/amp_9pt4[0]  , ls='-', marker='o', lw=2.,
             ms=8, mew=1.5, label=r"$\rm M_{HI}>10^{9.4} [M_{\odot}]$")
    
    ax1.plot(sigma_cut, amp_9pt6/amp_9pt6[0]  , ls='-', marker='s', lw=2.,
             ms=8, mew=1.5, label=r"$\rm M_{HI}>10^{9.4} [M_{\odot}]$")
    
    ax1.plot(sigma_cut, amp_9pt8/amp_9pt8[0]  , ls='-', marker='D', lw=2.,
             ms=8, mew=1.5, label=r"$\rm M_{HI}>10^{9.8} [M_{\odot}]$")
    
    ax1.plot(sigma_cut, amp_10pt0/amp_10pt0[0], ls='-', marker='^', lw=2.,
             ms=8, mew=1.5)#, label=r"$\rm M_{HI}>10^{10.0} [M_{\odot}]$")
    
    ax1.plot(sigma_cut, amp_10pt2/amp_10pt2[0], ls='-', marker='>', lw=2.,
             ms=8, mew=1.5)#, label=r"$\rm M_{HI}>10^{10.2} [M_{\odot}]$")

    ax1.text(0, 0.85,r'$\rm r_{s}=0.30$ $\rm [h^{-1} Mpc]$' , fontsize=18)#, bbox=props) 
    ax1.set_ylabel(r'$\rm w_{p}^{\sigma}(r_{s})/w_{p}^{\sigma=0}(r_{s})$', fontsize=20, fontweight='bold')
    ax1.set_xlabel(r"$\rm \sigma$ $\rm(Input$ $\rm Scatter)$", fontsize=18, fontweight='bold')
    ax1.legend(frameon=False, ncol=1, fontsize=16, loc=3,numpoints=1)

    #ax1.set_yscale("log")
    #ax1.set_yscale("log")
    ax1.set_xlim(-0.05, 0.30)
    ax1.set_xticks([0.00, 0.05, 0.10, 0.15, 0.20, 0.25])
    ax1.set_ylim(0.8, 1.2)

    #ax1.set_ylim(0.9, 1.1)

    ax1.minorticks_on()
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)

#------------------------------------------------------------------

    ax2 = plt.subplot(gs[0, 1],  sharey=ax1)
    ax2.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
    for i in range(4):

        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%('9pt4', sigma_cut[i])
        rp =np.genfromtxt(fname, usecols=0) 
        wp_model =np.genfromtxt(fname, usecols=2)
        wp_func = _spline(rp, wp_model, k=1)
        amp_9pt4[i] = wp_func(15)
        
        
        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%('9pt6', sigma_cut[i])
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_9pt6[i] = wp_func(15)
     

        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%('9pt8', sigma_cut[i])
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_9pt8[i] = wp_func(15)
     
        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%('10pt0', sigma_cut[i])
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_10pt0[i] = wp_func(15)
     
        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%('10pt2', sigma_cut[i])
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_10pt2[i] = wp_func(15)


    ax2.plot(sigma_cut, amp_9pt4/amp_9pt4[0]  ,ls='-', marker='o', lw=2.,
             ms=8, mew=1.5)#, label=r"$\rm M_{HI}>10^{9.4} [M_{\odot}]$") 
                                                                    
    ax2.plot(sigma_cut, amp_9pt6/amp_9pt6[0]  ,ls='-', marker='s', lw=2.,
             ms=8, mew=1.5)#, label=r"$\rm M_{HI}>10^{9.4} [M_{\odot}]$") 
                                                                    
    ax2.plot(sigma_cut, amp_9pt8/amp_9pt8[0]  ,ls='-', marker='D', lw=2.,
             ms=8, mew=1.5)#, label=r"$\rm M_{HI}>10^{9.8} [M_{\odot}]$") 
                                                                    
    ax2.plot(sigma_cut, amp_10pt0/amp_10pt0[0],ls='-', marker='^', lw=2.,
             ms=8, mew=1.5, label=r"$\rm M_{HI}>10^{10.0} [M_{\odot}]$")

    ax2.plot(sigma_cut, amp_10pt2/amp_10pt2[0],ls='-', marker='>', lw=2.,
             ms=8, mew=1.5, label=r"$\rm M_{HI}>10^{10.2} [M_{\odot}]$")

    ax2.text(0, 0.85, r'$\rm r_{s}=15$ $\rm[h^{-1} Mpc] $' , fontsize=18)#, bbox=props) 
    #ax2.set_ylabel(r'$\rm w_{p}^{r_{1}}$', fontsize=18, fontweight='bold')
    ax2.set_xlabel(r"$\rm \sigma$ $\rm(Input$ $\rm Scatter)$", fontsize=18, fontweight='bold')
    ax2.legend(frameon=False, ncol=1, fontsize=16, loc=3,numpoints=1)

    #ax1.set_yscale("log")
    #ax1.set_yscale("log")
    ax2.set_xlim(-0.05, 0.30)
    ax2.set_xticks([0.00, 0.05, 0.10, 0.15, 0.20, 0.25])
    #ax2.set_ylim(0.95, 1.2)

    ax2.minorticks_on()
    #ax2.set_yscale("log")
    ax2.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax2.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax2.spines['bottom'].set_linewidth(1.2)
    ax2.spines['left'].set_linewidth(1.2)
    ax2.spines['top'].set_linewidth(1.2)
    ax2.spines['right'].set_linewidth(1.2)
    plt.setp(ax2.get_yticklabels(),visible=False)

    plt.tight_layout()
    #fout = (plot_dir+'_real_wp_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}_sigma_{3:0.2f}.png'.format(frac, mstar, beta, sigma))
    fig.savefig('test1.png', bbox_inches='tight', dpi=300, edgecolor='none', frameon='False')
    
    #plt.show()
    



#-------------------------------------------------------------

    Mass_cut = np.array([9.4, 9.6, 9.8, 10.0, 10.2])
 
    amp_noScat      = np.zeros(5, dtype=np.float64)
    amp_Scat_0pt15 = np.zeros(5, dtype=np.float64)
    amp_Scat_0pt20 = np.zeros(5, dtype=np.float64)
    amp_Scat_0pt25 = np.zeros(5, dtype=np.float64)

   

    fig= plt.figure(3, figsize=(10, 5))
    gs = gridspec.GridSpec(1, 2, wspace=0.000)
    ax1 = plt.subplot(gs[0, 0])

    ax1.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
    
    for i in range(5):

        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], 0.00)
        rp =np.genfromtxt(fname, usecols=0) 
        wp_model =np.genfromtxt(fname, usecols=2)
        wp_func = _spline(rp, wp_model, k=1)
        amp_noScat[i] = wp_func(0.3)
        
        
        fname    = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], 0.15)
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_Scat_0pt15[i] = wp_func(0.3)
     

        fname    = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], 0.20)
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_Scat_0pt20[i] = wp_func(0.3)
     
        fname    = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], 0.25)
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_Scat_0pt25[i] = wp_func(0.3)
     
        fname    = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], 0.25)
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_Scat_0pt25[i] = wp_func(0.3)
 


    ax1.plot(Mass_cut, amp_noScat/amp_noScat, ls='-', marker='o', lw=2., 
             ms=8, mew=1.5, label=r"$\sigma=0.00$")

    ax1.plot(Mass_cut, amp_Scat_0pt15/amp_noScat ,ls='-', marker='s', lw=2,
             ms=8, mew=1.5, label=r"$\sigma=0.15$")

    ax1.plot(Mass_cut, amp_Scat_0pt20/amp_noScat ,ls='-', marker='D', lw=2,
             ms=8, mew=1.5)#, label=r"$\sigma=0.20$")

    ax1.plot(Mass_cut, amp_Scat_0pt25/amp_noScat ,ls='-', marker='^', lw=2,
             ms=8, mew=1.5)#,  label=r"$\sigma=0.25$")

    ax1.text(9.4, 0.85,r'$\rm r_{s}=0.3$ $\rm [h^{-1} Mpc]$' , fontsize=18)#, bbox=props) 
    ax1.set_ylabel(r'$\rm w_{p}^{\sigma}(r_{s})/w_{p}^{\sigma=0}(r_{s})$', fontsize=20, fontweight='bold')
    ax1.set_xlabel(r"$\rm \log_{10} (M_{HI}>) [M_{\odot}]$", fontsize=18, fontweight='bold')
    ax1.legend(frameon=False, ncol=1, fontsize=16, loc=3,numpoints=1)

    ax1.set_xlim(9.2, 10.4)
    ax1.set_ylim(0.8, 1.2)
    ax1.set_xticks([9.2, 9.4, 9.6, 9.8, 10.0, 10.20])
    ax1.set_xticklabels([9.2, 9.4, 9.6, 9.8, 10.0, 10.20])

    ax1.minorticks_on()
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)

    plt.tight_layout()
#------------------------------------------------------------------

    ax2 = plt.subplot(gs[0, 1],  sharey=ax1)
    ax2.set_prop_cycle('color', palettable.tableau.GreenOrange_6.mpl_colors)
    for i in range(4):

        fname = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], 0.00)
        rp =np.genfromtxt(fname, usecols=0) 
        wp_model =np.genfromtxt(fname, usecols=2)
        wp_func = _spline(rp, wp_model, k=1)
        amp_noScat[i] = wp_func(15)
        
        
        fname    = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], 0.15)
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_Scat_0pt15[i] = wp_func(15)
     

        fname    = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], 0.20)
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_Scat_0pt20[i] = wp_func(15)
     
        fname    = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], 0.25)
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_Scat_0pt25[i] = wp_func(15)
     
        fname    = fname1+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], 0.25)
        rp       = np.genfromtxt(fname, usecols=0) 
        wp_model = np.genfromtxt(fname, usecols=2)
        wp_func  = _spline(rp, wp_model, k=1)
        amp_Scat_0pt25[i] = wp_func(15)
 


    ax2.plot(Mass_cut, amp_noScat/amp_noScat     ,ls='-', marker='o', lw=2.5, 
             ms=8, mew=1.5)#, label=r"$\sigma=0.00$")

    ax2.plot(Mass_cut, amp_Scat_0pt15/amp_noScat ,ls='-', marker='s', lw=2.5,
             ms=8, mew=1.5)#, label=r"$\sigma=0.15$")

    ax2.plot(Mass_cut, amp_Scat_0pt20/amp_noScat ,ls='-', marker='D', lw=2.5,
             ms=8, mew=1.5, label=r"$\sigma=0.20$")

    ax2.plot(Mass_cut, amp_Scat_0pt25/amp_noScat ,ls='-', marker='^', lw=2.5,
             ms=8, mew=1.5,  label=r"$\sigma=0.25$")


    ax2.text(9.4, 0.85, r'$\rm r_{s}=15$ $\rm[h^{-1} Mpc] $' , fontsize=18)#, bbox=props) 
    ax2.set_xlabel(r"$\rm \log_{10} (M_{HI}>) [M_{\odot}]$", fontsize=18, fontweight='bold')
    ax2.legend(frameon=False, ncol=1, fontsize=16, loc=3,numpoints=1)

    ax2.set_xlim(9.2, 10.4)
    #ax2.set_xticks([9.4, 9.6, 9.8, 10.0, 10.20])

    ax2.minorticks_on()
    ax2.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax2.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax2.spines['bottom'].set_linewidth(1.2)
    ax2.spines['left'].set_linewidth(1.2)
    ax2.spines['top'].set_linewidth(1.2)
    ax2.spines['right'].set_linewidth(1.2)
    plt.setp(ax2.get_yticklabels(),visible=False)

    plt.tight_layout()
    #fout = (plot_dir+'_real_wp_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}_sigma_{3:0.2f}.png'.format(frac, mstar, beta, sigma))
    fig.savefig('test2.png', bbox_inches='tight', dpi=300, edgecolor='none', frameon='False')
    """    


if __name__ == "__main__":
    main()


