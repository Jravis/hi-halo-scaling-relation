from __future__ import print_function, division
import numpy as np
import matplotlib.pyplot as plt
import sys
import time
from scipy import integrate
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
from abundance import fit_abundance_halo, raw_abundance, HI_Mass_func, AM_nonparam, schechter_function

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

from astropy.cosmology import LambdaCDM
import datatable as dt

import Corrfunc
from Corrfunc.theory.wp import wp



mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rc('font', weight='bold')
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Ubuntu'
mpl.rcParams['font.monospace'] = 'Ubuntu Mono'
mpl.rcParams['axes.labelweight'] = 'bold'

mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'

import palettable

#set some plotting parameters
plt.style.use("classic")
#plt.style.use("ggplot")
#plt.style.use("seaborn-paper")
#plt.style.use("fast")


#---------------- Global Values ---------------------------
from hmf import MassFunction
from hmf import cosmo

# Multidark 2 cosmology hmf model
OmegaMatter = 0.307115
OmegaLambda = 0.692885
H0 = 0.6777*100.
OmegaBaryon = 0.048206
new_model   = LambdaCDM(H0 = 0.6777*100, Om0 = OmegaMatter, 
			Tcmb0=2.73, Ob0= OmegaBaryon, Ode0=OmegaLambda)

logMhalo_min=9.0
logMhalo_max=15.5
NBINS = 20
xlim      = [logMhalo_min, logMhalo_max]
deltaM    = (xlim[1]-xlim[0])/np.float64(NBINS)
logMbins  = np.arange(xlim[0],xlim[1]+deltaM, deltaM)

scatter_inp=0.25

#Halo_Cat_fname = ("/mnt/data1/sandeep/multidark2_data/halo_cat/"\
#                     "halo_data/Rockstar_SMDPL_Halo_Mpeak.csv")
 
name = "/mnt/data1/sandeep/multidark2_data/halo_cat/halo_data/Rockstar_SMDPL_Halo_Mpeak.csv"
MDPLpropdata = dt.fread(name)
Lbox = 400/0.6777 #h^-1 Mpc
Mvir = np.ndarray.flatten(MDPLpropdata['Mvir'].to_numpy())/0.6777 # as Mvir is h^-1 Msun units
Mpeak = np.ndarray.flatten(MDPLpropdata['Mpeak'].to_numpy())/0.6777 # as Mvir is h^-1 Msun units
del MDPLpropdata


p = dict(phi1=0.001,x1=14,alpha1=-1.0) #initial guess for parameters to fit function

result_SF, logMbins_avg, dn_dH_ini, err, dn_dH_func  = fit_abundance_halo(
                                                            Mvir, weights=1.0/Lbox**3.0, 
                                                            bins=logMbins, xlog=True,
                                                            fit_type='schechter', p = p, 
                                                            show_fit=False
                                                        )

result_SF_mpeak, logMbins_avg_mpeak, dn_dH_ini_mpeak, err_mpeak, dn_dH_func_mpeak  = fit_abundance_halo(
                                                                                        Mpeak, weights=1.0/Lbox**3.0, 
                                                                                        bins=logMbins, xlog=True,
                                                                                        fit_type='schechter', p = p, 
                                                                                        show_fit=False
                                                                                    )
logMhalo_min=10.0
logMhalo_max=15.5
NBINS = 20
xlim      = [logMhalo_min, logMhalo_max]
deltaM    = (xlim[1]-xlim[0])/np.float64(NBINS)
logMbins  = np.arange(xlim[0],xlim[1]+deltaM, deltaM)


fname ="../pipeline_result_data_alfa70_Mvir/HaloMockCat_Mvir_frac_0.45_Mstar_13.25_beta_0.66_sigma_0.15.txt"
Mhalo_sigma =  np.genfromtxt(fname, usecols=0)

foo, logMbins_0pt15, dn_dH_ini_0pt15, err_0pt15, bar  = fit_abundance_halo(
                                                                    Mhalo_sigma ,
                                                                    weights=1.0/Lbox**3.0, 
                                                                    bins=logMbins, xlog=True,
                                                                    fit_type='schechter', p = p, 
                                                                    show_fit=False
                                                                )

fname ="../pipeline_result_data_alfa70_Mvir/HaloMockCat_Mvir_frac_0.45_Mstar_13.25_beta_0.66_sigma_0.20.txt"
Mhalo_sigma =  np.genfromtxt(fname, usecols=0)
foo, logMbins_0pt2, dn_dH_ini_0pt2, err_0pt2, bar  = fit_abundance_halo(
                                                                    Mhalo_sigma,
                                                                    weights=1.0/Lbox**3.0, 
                                                                    bins=logMbins, xlog=True,
                                                                    fit_type='schechter', p = p, 
                                                                    show_fit=False
                                                                )


fname ="../pipeline_result_data_alfa70_Mvir/HaloMockCat_Mvir_frac_0.45_Mstar_13.25_beta_0.66_sigma_0.25.txt"
Mhalo_sigma =  np.genfromtxt(fname, usecols=0)
foo, logMbins_0pt25, dn_dH_ini_0pt25, err_0pt25, bar  = fit_abundance_halo(
                                                                    Mhalo_sigma, 
                                                                    weights=1.0/Lbox**3.0, 
                                                                    bins=logMbins, xlog=True,
                                                                    fit_type='schechter', p = p, 
                                                                    show_fit=False
                                                                )


#===========================================================================

"""

logMhalo_min=10.0
logMhalo_max=15.5
NBINS = 20
xlim      = [logMhalo_min, logMhalo_max]
deltaM    = (xlim[1]-xlim[0])/np.float64(NBINS)
logMbins  = np.arange(xlim[0],xlim[1]+deltaM, deltaM)


name = "/mnt/data1/sandeep/multidark2_data/halo_cat/halo_data/Rockstar_MDPL2_halo_properties.csv"
MDPLpropdata = dt.fread(name)
Lbox_MDPL2 = 1000/0.6777 #h^-1 Mpc
Mvir_MDPL2= np.ndarray.flatten(MDPLpropdata['Mvir'].to_numpy())/0.6777 # as Mvir is h^-1 Msun units
del MDPLpropdata

p = dict(phi1=0.001,x1=14,alpha1=-1.0) #initial guess for parameters to fit function

result_SF_1, logMbins_avg1, dn_dH_ini_1, err_1, dn_dH_func_1  = fit_abundance_halo(
                                                                    Mvir_MDPL2, 
                                                                    weights=1.0/Lbox_MDPL2**3.0, 
                                                                    bins=logMbins, xlog=True,
                                                                    fit_type='schechter', p = p, 
                                                                    show_fit=False
                                                                )
#===========================================================================



import lmfit

def my_model(x, m, c):
    return m*x+c 

#---------------------------------------------------------

props = dict(boxstyle='round', facecolor='none', linewidth=1.5)
Model = lmfit.Model(my_model)

p = dict(m=0.86298182, c = 1.) #initial guess for parameters to fit function
#p = dict(frac=0.3,mstar=13.,beta=0.5) #initial guess for parameters to fit function
# create Parameters, giving initial values
params = Model.make_params(m=p['m'], c=p['c'] )

# do fit, print result
X = logMbins_avg[3:6]
Y = np.log10(dn_dH_ini[3:6])
result = Model.fit(Y, params, x=X)
print(result.fit_report())

#NBINS = 50
NBINS = 20
xlim  = [6, logMhalo_max]    # log10(MHI)
delta_mf=(xlim[1]-xlim[0])/float(NBINS)
H  = np.arange(xlim[0], xlim[1]+delta_mf, delta_mf)
print(np.max(H))

dn_dH = dn_dH_func(H)
record = 10**logMbins_avg[3]
ind  = (H>logMbins_avg[3])
y = result.params['m'].value*H[~ind]+ result.params['c']
y1 = dn_dH_func(H[ind])
dn_dH = np.concatenate((10**y, y1))

err = np.log10(dn_dH_ini+err) - np.log10(dn_dH_ini)
err_1 = np.log10(dn_dH_ini_1+err_1) - np.log10(dn_dH_ini_1)

fig = plt.figure(3, figsize=(6, 6))
gs = gridspec.GridSpec(1, 1)#, hspace=0.0, height_ratios=[3,1])
ax1 = plt.subplot(gs[0])

ax1.errorbar(logMbins_avg, np.log10(dn_dH_ini), yerr=err, fmt='o', ecolor='r', elinewidth=1.8, mfc='none',
             capsize=3, ms=8, mec='r', mew=1.5, label=r"$\rm \Phi_{SMDPL-Rockstar}(M_h)$")
             
#ax1.errorbar(logMbins_avg1, np.log10(dn_dH_ini_1), yerr=err_1, fmt='s', ecolor='r', elinewidth=1.8, mfc='none',
#             capsize=3, ms=8, mec='r', mew=1.5, label=r"$\rm \Phi_{MDPL2-Rockstar}(M_h)$")

ax1.plot(H, np.log10(dn_dH), ls='-', lw=2., color='r', label=r'')

ax1.axvline(x = np.log10(4.13941*10**10), linestyle='--', linewidth=2.5, color='b')
ax1.text(12, -1, r"$\rm Lbox = 400 Mpc/h$" "\n" r"$\rm N_p = 3840^3$" "\n" r"$\rm M_p = 9.63\times10^7 M_{\odot}/h$", fontsize=14, bbox=props) 


ax1.set_xlim(8, 16)
ax1.set_ylim(-6, 1.1)
ax1.set_xlabel(r'$\rm \log_{10} (M_{h})$ [$\rm{M}_\odot$]', fontsize=18)
ax1.set_ylabel(r'$\rm \log_{10} (dn/d \log_{10} M_{h}) $ [$\rm Mpc^{-3}$ ]', fontsize=18)
#ax1.set_ylabel(r'$\rm log_{10} \Phi(M_{h}) $ [$\rm Mpc^{-3}$ ]', fontsize=20)
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=14, loc=1, numpoints=1)
ax1.tick_params(axis='both', which='minor', length=5,
      width=2, labelsize=13)
ax1.tick_params(axis='both', which='major', length=8,
       width=2, labelsize=13)
ax1.spines['bottom'].set_linewidth(1.2)
ax1.spines['left'].set_linewidth(1.2)
ax1.spines['top'].set_linewidth(1.2)
ax1.spines['right'].set_linewidth(1.2)
fout="mass_func_v1.png"
#fig.savefig(fout, bbox_inches='tight', dpi=300, edgecolor='none', frameon='False')
"""

#------------------------------------------------------------

fig = plt.figure(1, figsize=(6, 6))
gs = gridspec.GridSpec(1, 1)#, hspace=0.0, height_ratios=[3,1])
ax1 = plt.subplot(gs[0])

ax1.errorbar(logMbins_avg, np.log10(dn_dH_ini), yerr=err, fmt='o', ecolor='r', elinewidth=1.8, mfc='none',
             capsize=3, ms=8, mec='r', mew=1.5, label=r"$\rm \Phi_{SMDPL-RS}(M_h)$")
             
#ax1.errorbar(logMbins_avg_mpeak, np.log10(dn_dH_ini_mpeak), yerr=err_mpeak, fmt='o', ecolor='g', elinewidth=1.8, mfc='none',
#             capsize=3, ms=8, mec='g', mew=1.5, label=r"$\rm \Phi_{SMDPL-RS}(M_h)^{M_{peak}}$")

ax1.errorbar(logMbins_0pt15, np.log10(dn_dH_ini_0pt15), yerr=err_0pt15, fmt='s', ecolor='peru', elinewidth=1.8, mfc='none',
             capsize=3, ms=8, mec='peru', mew=1.5, label=r"$\rm \Phi_{SMDPL-RS}^{\sigma = 0.15} (M_h)$")
             

ax1.errorbar(logMbins_0pt2, np.log10(dn_dH_ini_0pt2), yerr=err_0pt2, fmt='o', ecolor='k', elinewidth=1.8, mfc='none',
             capsize=3, ms=8, mec='k', mew=1.5, label=r"$\rm \Phi_{SMDPL-RS}^{\sigma = 0.20} (M_h)$")
             
ax1.errorbar(logMbins_0pt25, np.log10(dn_dH_ini_0pt25), yerr=err_0pt25, fmt='d', ecolor='g', elinewidth=1.8,mfc='none',
             capsize=3, ms=8, mec='g', mew=1.5, label=r"$\rm \Phi_{SMDPL-RS}^{\sigma = 0.25} (M_h)$")
 

ax1.axvline(x = np.log10(4.13941*10**10), linestyle='--', linewidth=2.5, color='b')
#ax1.text(12.5, -1.5, r"$\rm Lbox = 400 Mpc/h$" "\n" r"$\rm N_p = 3840^3$" "\n" r"$\rm M_p = 9.63\times10^7 M_{\odot}/h$", fontsize=14, bbox=props) 


ax1.set_xlim(8, 16)
ax1.set_ylim(-6, 1.1)
ax1.set_xlabel(r'$\rm \log_{10} (M_{h})$ [$\rm{M}_\odot$]', fontsize=18)
ax1.set_ylabel(r'$\rm \log_{10} (dn/d \log_{10} M_{h}) $ [$\rm Mpc^{-3}$ ]', fontsize=18)
#ax1.set_ylabel(r'$\rm log_{10} \Phi(M_{h}) $ [$\rm Mpc^{-3}$ ]', fontsize=20)
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=14, loc=1, numpoints=1)
ax1.tick_params(axis='both', which='minor', length=5,
      width=2, labelsize=13)
ax1.tick_params(axis='both', which='major', length=8,
       width=2, labelsize=13)
ax1.spines['bottom'].set_linewidth(1.2)
ax1.spines['left'].set_linewidth(1.2)
ax1.spines['top'].set_linewidth(1.2)
ax1.spines['right'].set_linewidth(1.2)
#fout="mass_func_mpeak.png"
fout="HIHMF_withScatter_.png"
fig.savefig(fout, bbox_inches='tight', dpi=300, edgecolor='none', frameon='False')




"""
fig = plt.figure(2, figsize=(6, 6))
gs = gridspec.GridSpec(1, 1)#, hspace=0.0, height_ratios=[3,1])
ax1 = plt.subplot(gs[0])

ax1.plot(logMbins_avg, np.log10(dn_dH_ini), dashes=[2, 2, 2, 2, 10, 2], linewidth=2, color='b',
         label=r"$\rm \Phi_{SMDPL-RS}(M_h)^{M_{vir}}$")
ax1.plot(logMbins_avg_mpeak, np.log10(dn_dH_ini_mpeak), linestyle='-', linewidth=2, color='r',
         label=r"$\rm \Phi_{SMDPL-RS}(M_h)^{M_{peak}}$")
 
ax1.axvline(x = np.log10(4.13941*10**10), linestyle='--', linewidth=2.5, color='b')
ax1.text(13, -2, r"$\rm Lbox = 400 Mpc/h$" "\n" r"$\rm N_p = 3840^3$" "\n" r"$\rm M_p = 9.63\times10^7 M_{\odot}/h$", fontsize=14, bbox=props) 

ax1.set_xlim(8, 16)
ax1.set_ylim(-6, 1.1)
ax1.set_xlabel(r'$\rm \log_{10} (M_{h})$ [$\rm{M}_\odot$]', fontsize=18)
ax1.set_ylabel(r'$\rm \log_{10} (dn/d \log_{10} M_{h}) $ [$\rm Mpc^{-3}$ ]', fontsize=18)
#ax1.set_ylabel(r'$\rm log_{10} \Phi(M_{h}) $ [$\rm Mpc^{-3}$ ]', fontsize=20)
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=14, loc=1, numpoints=1)
ax1.tick_params(axis='both', which='minor', length=5,
      width=2, labelsize=13)
ax1.tick_params(axis='both', which='major', length=8,
       width=2, labelsize=13)
ax1.spines['bottom'].set_linewidth(1.2)
ax1.spines['left'].set_linewidth(1.2)
ax1.spines['top'].set_linewidth(1.2)
ax1.spines['right'].set_linewidth(1.2)
fout="mass_func_mpeak_lineplot.png"
fig.savefig(fout, bbox_inches='tight', dpi=300, edgecolor='none', frameon='False')



fig = plt.figure(4, figsize=(5, 5))
gs = gridspec.GridSpec(1, 1)#, hspace=0.0, height_ratios=[3,1])
ax1 = plt.subplot(gs[0])
ax1.plot(logMbins_avg, (dn_dH_ini/dn_dH_ini_mpeak), ls='', marker='o', ms=8, mec='b', mfc='none', mew=1.5, color='b')
ax1.set_xlim(8, 16)
ax1.set_ylim(0.5, 1.5)
ax1.set_xlabel(r'$\rm \log_{10} (M_{h})$ [$\rm{M}_\odot$]', fontsize=18)
ax1.set_ylabel(r"$\rm \Phi_{SMDPL-RS}(M_h)^{M_{vir}} / \Phi_{SMDPL-RS}(M_h)^{M_{peak}}$", fontsize=14)
ax1.axhline(y =1, dashes=[2, 2, 2, 2, 10, 2], linewidth=1.5, color='r')
ax1.minorticks_on()
ax1.tick_params(axis='both', which='minor', length=5,
      width=2, labelsize=13)
ax1.tick_params(axis='both', which='major', length=8,
       width=2, labelsize=13)
ax1.spines['bottom'].set_linewidth(1.2)
ax1.spines['left'].set_linewidth(1.2)
ax1.spines['top'].set_linewidth(1.2)
ax1.spines['right'].set_linewidth(1.2)
fout="mass_func_mpeak_ratio.png"
fig.savefig(fout, bbox_inches='tight', dpi=300, edgecolor='none', frameon='False')


#plt.show()

"""
