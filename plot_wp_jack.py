import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rc('font', weight='bold')
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Ubuntu'
mpl.rcParams['font.monospace'] = 'Ubuntu Mono'
mpl.rcParams['axes.labelweight'] = 'bold'

mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'

import palettable

#set some plotting parameters
plt.style.use("classic")
#plt.style.use("ggplot")
#plt.style.use("seaborn-paper")
#plt.style.use("fast")





        fig= plt.figure(1, figsize=(6, 6))
        gs = gridspec.GridSpec(1, 1)#, wspace=0.000, hspace=0.000)
        ax1 = plt.subplot(gs[0, 0])

        rp=rbin_mid

        test, = ax1.plot(rp, wp_, lw=2,ls='', marker='s', ms=8, color='r', mfc='none', mec='r', 
                        mew=1.5, label=r"$\rm M_{HI}>10^{%0.1f}M_{\odot}$"%(cutoffList[i]))

        test_ =  ax1.errorbar(rp, mean_wp, yerr=err, mec='b', linestyle='',capsize=5, mfc='none', mew=1.5, fmt="o", 
                        color='b', elinewidth=1.5, ms=8, label=r"$\rm Jack$")


        leg2 = ax1.legend(handles=[test, test_], frameon=False, ncol=1, fontsize=18, loc=1,numpoints=2 )

        ax1.set_ylabel(r'$\rm w_{p}(r_{p}) [h^{-1} Mpc]$', fontsize=22, fontweight='bold')
        ax1.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=22, fontweight='bold')
        ax1.set_xscale("log")
        ax1.set_yscale("log")
        ax1.set_yticks([1, 10, 100])
        ax1.minorticks_on()
        ax1.tick_params(axis='both', which='minor', length=5,
              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.2)
        ax1.spines['left'].set_linewidth(1.2)
        ax1.spines['top'].set_linewidth(1.2)
        ax1.spines['right'].set_linewidth(1.2)

        plt.tight_layout()
        fout ="jack_wp_test_gt_%s_sigma_%0.2f_SMDPL.png"%(cutoff_name[i], sigmaList[j])
        fig.savefig(fout, bbox_inches='tight', dpi=100, edgecolor='none', frameon='False')
         
        fig= plt.figure(2, figsize=(6, 6))
        gs = gridspec.GridSpec(1, 1)#, wspace=0.000, hspace=0.000)
        ax1 = plt.subplot(gs[0, 0])

        test, = ax1.plot(rp, wp_/mean_wp, lw=2,ls='', marker='o', ms=5, color='b', mec='b', mfc='none', mew=1.5)
        ax1.axhline(y=1, lw=2, ls='--', color='r')
        ax1.set_ylabel(r'$\rm w_{p}^{orig}/w_{p}^{jack}(r_{p}) [h^{-1} Mpc]$', fontsize=22, fontweight='bold')
        ax1.set_xlabel(r'$\rm r_{p}[h^{-1} Mpc]$', fontsize=22, fontweight='bold')
        ax1.set_xscale("log")
        ax1.minorticks_on()
        ax1.tick_params(axis='both', which='minor', length=5,
              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.2)
        ax1.spines['left'].set_linewidth(1.2)
        ax1.spines['top'].set_linewidth(1.2)
        ax1.spines['right'].set_linewidth(1.2)
        plt.tight_layout()
        fout ="jack_wp_test_gt_%s_sigma_%0.2f_SMDPL_ratio.png"%(cutoff_name[i], sigmaList[j])
        fig.savefig(fout, bbox_inches='tight', dpi=100, edgecolor='none', frameon='False')
         
 
#check_sum += len(X_sample)

#print(check_sum)
#print(3.*len(random_data))
#assert check_sum == 3.0*len(random_data)



