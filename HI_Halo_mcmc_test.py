
#####################################################
#Copyright (c) 2019, Sandeep Rana                   #
# This routine needs Python 3                       #
# This routine is to compute the effect of          #
# scatter into HI-halo abundace matching relation   #
# Following Behroozi 2010 and Ren 2019.             #
#This routine is for creating mock and computing    #
#clustering. Here we explore paramter space         #
#and it's effect on HI clustering with and without  #
#the scatter.                                       #
                                                    #                                                   
#####################################################

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""




from __future__ import print_function, division
import numpy as np
import sys
import os
import time
from scipy import integrate
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
from abundance import fit_abundance_halo, raw_abundance, HI_Mass_func, AM_nonparam, schechter_function
from astropy.cosmology import LambdaCDM
import datatable as dt
import lmfit
import argparse
from Corrfunc.theory.wp import wp
from multiprocessing import Process
from astropy.io import ascii
import emcee
import corner
import palettable
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from collections import OrderedDict
from matplotlib.transforms import blended_transform_factory
import pandas as pd


mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rc('font', weight='bold')
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Ubuntu'
mpl.rcParams['font.monospace'] = 'Ubuntu Mono'
mpl.rcParams['axes.labelweight'] = 'bold'

mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'
mpl.rcParams['legend.numpoints'] = 1

#set some plotting parameters
plt.style.use("classic")
lnstyles = OrderedDict(
    [('solid',               (0, ())),
     ('loosely dotted',      (0, (1, 10))),
     ('dotted',              (0, (1, 5))),
     ('densely dotted',      (0, (1, 1))),

     ('loosely dashed',      (0, (5, 10))),
     ('dashed',              (0, (5, 5))),
     ('densely dashed',      (0, (5, 1))),

     ('loosely dashdotted',  (0, (3, 10, 1, 10))),
     ('dashdotted',          (0, (3, 5, 1, 5))),
     ('densely dashdotted',  (0, (3, 1, 1, 1))),

     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))])



#---------------- Global Values ---------------------------
from hmf import MassFunction
from hmf import cosmo

# Multidark 2 cosmology hmf model
OmegaMatter = 0.307115
OmegaLambda = 0.692885
H0 = 0.6777*100.
OmegaBaryon = 0.048206
new_model   = LambdaCDM(H0 = 0.6777*100, Om0 = OmegaMatter, 
			Tcmb0=2.73, Ob0= OmegaBaryon, Ode0=OmegaLambda)

def plot_mcmc_chains():
    res_flatchain_70 = (pd.read_pickle('./result_mcmc/MCMC_chains_alfa_70pct'))[['frac', 'mstar', 'beta']]
    truth_value_70 = list((pd.read_pickle('./result_mcmc/alfa_70pct_params_values'))['Values'])
    truth_value_70 = truth_value_70[:-1]

    res_flatchain_40 = (pd.read_pickle('./result_mcmc/MCMC_chains_alfa_40pct'))[['frac', 'mstar', 'beta']]
    truth_value_40 = list((pd.read_pickle('./result_mcmc/alfa_40pct_params_values'))['Values'])
    truth_value_40 = truth_value_40[:-1]

    #names = [r'$\rm \hat{f}$', r'$\rm M_{*}$', r'$ \hat{\beta}$', r'$\rm ln \sigma$']
    names = [r'$\rm \hat{f}$', r'$\rm M_{*}$', r'$ \hat{\beta}$']
    labels_kwrg = {"fontsize":28}
    title_kwrg = {"fontsize":20}

    #list(res.params.valuesdict().values())
    fig1= corner.corner(
        res_flatchain_40, bins=100, smooth=5.0, smooth1d=10.0, hist_bin_factor=10,
        labels=names, truths=truth_value_40, show_titles=False,fillbetween=True,
        title_fmt=".2f", quantiles=[0.16, 0.84],label_kwargs=labels_kwrg,
        levels= [1-np.exp(-0.5), 2*(1-np.exp(-0.5))], color='darkblue',
        truth_color='darkblue',title_kwargs=title_kwrg,
        fill_contours=False, hist_kwargs={'color':'darkblue', 'linewidth':2, 'linestyle':'-', 'label':r'$\alpha 40$'},
        hist2D_kwargs={'color':'darkblue', 'edgecolor':'darkblue', 
        'linewidth':5, 'alpha':1, 'fontsize':28, 'ls':'--', 'label':r'$\alpha 40$', 'hatch':'/'}
    )


    corner.corner(
        res_flatchain_70, bins=100, smooth=5.0, smooth1d=10.0, hist_bin_factor=10,
        labels=names, truths=truth_value_70, show_titles=False,fig=fig1,
        title_fmt=".2f", quantiles=[0.16, 0.84],label_kwargs=labels_kwrg,
        levels= [1-np.exp(-0.5), 2*(1-np.exp(-0.5))],
        truth_color='k',title_kwargs=title_kwrg,
        fill_contours=False, hist_kwargs={'color':'k', 'linewidth':2,'label':r'$\alpha 70$'},
        hist2D_kwargs={'color':'k', 'linewidth':5, 'alpha':1, 'fontsize':28,'label':r'$\alpha 70$'}
    )
    

    scatter_inp = 0
    fig1.set_size_inches(15, 15)
    fname = "MHI_Mhalo_bestFitParam_sigam{}_test_v4.png".format(scatter_inp)
    #ndim=4
    ndim=3
    #c', 'mstar', 'beta', '__lnsigma']
    axes  = np.array(fig1.axes).reshape((ndim, ndim))

    for i in range(ndim):
        ax = axes[i, i]
        ax.minorticks_on()
        ax.tick_params(axis='both', which='minor', length=8,
              width=2.5, labelsize=20)
        ax.tick_params(axis='both', which='major', length=12,
               width=2.5, labelsize=20)
        ax.legend(frameon=False, ncol=1, fontsize=20, loc=1, 
                  numpoints=2 )
        if i ==1:
            ax.legend(frameon=False, ncol=1, fontsize=20, loc=2, 
                  numpoints=2 )


        ax.spines['bottom'].set_linewidth(2)
        ax.spines['left'].set_linewidth(2)
        ax.spines['top'].set_linewidth(2)
        ax.spines['right'].set_linewidth(2)

    for yi in range(ndim):
        for xi in range(yi):
            ax = axes[yi, xi]
            ax.minorticks_on()
            ax.tick_params(axis='both', which='minor', length=8,
                  width=2.5, labelsize=20)
            ax.tick_params(axis='both', which='major', length=12,
                   width=2.5, labelsize=20)
            ax.spines['bottom'].set_linewidth(2.)
            ax.spines['left'].set_linewidth(2.)
            ax.spines['top'].set_linewidth(2.)
            ax.spines['right'].set_linewidth(2.)
            #ax.legend(frameon=False, ncol=1, fontsize=20, loc=1, 
            #      numpoints=2)



    fig1.savefig(fname, dpi=300,bbox='tight_layout', edgecolor='none', frameon='False')



def main_driver():
    
    # Read User given parameters

    parser = argparse.ArgumentParser()
    parser.add_argument("sigma", type=float, 
    help='Input scatter e.g 0.25')
    parser.add_argument("use_schechter", type=int, 
    help='if 1 use schecter function else use linear interpolation')

    args             = parser.parse_args()
    scatter_inp      = args.sigma # input scatter that is needed
    use_schechter    = args.use_schechter # do you want Schechter fit(1) or linear extrapolation
    grid_pram_search = False # Do you want to get clustering value around bestfit of Guo points


    # Read Rockstar Halo catalog for SMDPL

    Halo_Cat_fname = ("/mnt/data1/sandeep/multidark2_data/halo_cat/"\
                     "halo_data/Rockstar_SMDPL_Halo_Mpeak.csv")

    # Box Size for SMDPL
    #SMDPL cosmology uses h = 0.6777
    
    Lbox = 400/0.6777 #h^-1 Mpc taken out h^-1 Mpc

    print("Reading Rockstar SMDPL Halo Catalog (Mvir and positions)\n")
    #print("Reading Rockstar SMDPL Halo Catalog (Mpeak and positions)\n")
    
    MDPLpropdata = dt.fread(Halo_Cat_fname)
    Mvir   = np.ndarray.flatten(MDPLpropdata['Mvir'].to_numpy())/0.6777 # as Mvir is h^-1 Msun units taken out h^-1
    #Mvir   = np.ndarray.flatten(MDPLpropdata['Mpeak'].to_numpy())/0.6777 # as Mvir is h^-1 Msun units taken out h^-1
    Halox  = np.ndarray.flatten(MDPLpropdata['x'].to_numpy()) # as X is h^-1 Mpc units
    Haloy  = np.ndarray.flatten(MDPLpropdata['y'].to_numpy()) # as Y is h^-1 Mpc units
    Haloz  = np.ndarray.flatten(MDPLpropdata['z'].to_numpy()) # as Z is h^-1 Mpc units
    Halovz = np.ndarray.flatten(MDPLpropdata['vz'].to_numpy()) # as Z is h^-1 Mpc units
    
    print("Releasing unused memory\n")
    del MDPLpropdata
    
    print("||----- Fitting Halo mass function and getting bestfit Schechter function parameter -----||\n")
    

    logMhalo_min = 10.0
    logMhalo_max = 15.5
    NBINS = 20
    
    xlim      = [logMhalo_min, logMhalo_max]
    deltaM    = (xlim[1]-xlim[0])/np.float64(NBINS)
    logMbins  = np.arange(xlim[0],xlim[1]+deltaM, deltaM)

    p_ini = dict(phi1=0.001, x1=14, alpha1=-1.0) #initial guess for parameters to fit function
    
    result_SF, logMbins_avg, dn_dH_ini, err, dn_dH_func  = fit_abundance_halo(Mvir, weights=1.0/Lbox**3.0, bins=logMbins, xlog=True,
                                                           fit_type='schechter', p = p_ini, show_fit=False)

    #-------------------------------------------------------------------------------------------
    
    print("||----- Fitting Halo MF intial points for linear interpolation -----||\n")
    
    # non-linear generalized fitting library

    def Lin_Model(x, m, c):
        return m*x+c 


    Model = lmfit.Model(Lin_Model)
    p = dict(m=0.86298182, c = 1.) #initial guess for parameters to fit function
    
    # create Parameters, giving initial values
    params = Model.make_params(m=p['m'], c=p['c'] )
    # do fit, print result
    X = logMbins_avg[3:6]
    Y = np.log10(dn_dH_ini[3:6])
    Lin_result = Model.fit(Y, params, x=X)
    print(Lin_result.fit_report())
    print("--------------------------------------------------\n")


    print("||----- Reading Guo Points -----||\n")
    
    guo_H       = np.genfromtxt("../sailis-data/guo_points.dat", usecols=0)
    guo_HI_halo = np.genfromtxt("../sailis-data/guo_points.dat", usecols=1)

    NBINS = 1000
    #NBINS = 50
    #NBINS = 200
    print("||----- Getting alfa70% mass function -----||\n")
    
    xlim  = [6, 11]    # log10(MHI)
    delta_mf=(xlim[1]-xlim[0])/float(NBINS)
    G  = np.arange(xlim[0],xlim[1]+delta_mf,delta_mf)
    #dn_dG   = 10**HI_Mass_func(G)         # Alfa-alfa HI mass function
    dn_dG   = 10**HI_Mass_func(G, mode='alfa40')         # Alfa-alfa HI mass function

    print("||----- Getting halo mass function -----||\n")

    xlim  = [6, logMhalo_max]    # log10(MHI)
    delta_mf=(xlim[1]-xlim[0])/float(NBINS)
    H  = np.arange(xlim[0],xlim[1]+delta_mf,delta_mf)

    # Here using best fit schechter func params to get halo mass func.
    
    if use_schechter==1:

        print("||----- Using Schechter Linear fit -----||\n")

        dn_dH = schechter_function(H, result_SF.params['phistar'].value,
                result_SF.params['alpha'].value, result_SF.params['mstar'].value)
    else:
        print("||----- Using HMF Linear fit -----||\n")

        dn_dH = dn_dH_func(H)
        ind  = (H>logMbins_avg[3])
        y = Lin_result.params['m'].value*H[~ind]+ Lin_result.params['c']
        y1 = dn_dH_func(H[ind])
        dn_dH = np.concatenate((10**y, y1))



    # Calling AM_nonparam routine for iterative deconvolution to get abundance matching
    # with without scatter, if P_x is false then 
    # return HI-Halo AM match without scatter, halo MF interpolation function
    
    HI_halo_func_ini1, dn_dH_func_ini = AM_nonparam(G, dn_dG, H, dn_dH, 
                                            scatter_inp, 0, 0, 0, analytic=False, 
                                            P_x=False, H_min=6, H_max=logMhalo_max, 
                                            nH=NBINS, tol_err=0.01)
    

    print("\n")
    print("||----- Fitting for HI-Selected Mass function ----||\n")
    

    def my_model(x, frac, mstar, beta):
        """Module returns the objective function that is 
        HI-Halo scaling relation (in log) subtracted log(Halo MF/HI selcted Halo MF)
        """
        HI_halo_func_ini2, dn_dH_func_mod = AM_nonparam(G, dn_dG, H, dn_dH, 
                                                scatter_inp, frac, mstar, 
                                                beta, analytic=False, HI_selected_MF=True, 
                                                P_x=False, H_min=6, H_max=logMhalo_max, 
                                                nH=NBINS, tol_err=0.01)
 
        ratio = dn_dH_func_ini(x)/dn_dH_func_mod(x)
        return HI_halo_func_ini2(x)-np.log10(ratio)  


    print('------------------------------------------')
    Model = lmfit.Model(my_model)

    p = dict(frac=0.3204,mstar=13.6609,beta=0.9957) #initial guess for parameters to fit function
    #p = dict(frac=0.3,mstar=13.,beta=0.5) #initial guess for parameters to fit function
    # create Parameters, giving initial values
    params = Model.make_params(frac=p['frac'], mstar=p['mstar'], beta=p['beta'])
    params['frac'].min = 0.1
    params['mstar'].min = 11.0
    params['beta'].min = 0.000
    
    params['frac'].max = 0.8
    params['mstar'].max = 14.5
    params['beta'].max = 2.0


    # do fit, print result
    result = Model.fit(guo_HI_halo, params, x=guo_H, method='Nelder')

    print(result.fit_report())
    print('||-------------------------Emcee--------------------||')
    print("\n")
    
    emcee_kws = dict(steps=10000, burn=500, thin=20, is_weighted=False, nwalkers=8,
                     progress=True, workers=20)
    emcee_params = result.params.copy()
    emcee_params.add('__lnsigma', value=np.log(0.1), min=np.log(0.001), max=np.log(2.0))
    res = Model.fit(guo_HI_halo, x=guo_H, params=emcee_params, method='emcee',
                    nan_policy='omit', fit_kws=emcee_kws)
    
    print(list(res.params.valuesdict().values()))
    print(list(res.params.valuesdict().keys()))
    
    fh = pd.DataFrame(index=list(res.params.valuesdict().keys()))
    fh['Values'] =list(res.params.valuesdict().values()) 
    fh.to_pickle('./result_mcmc/alfa_40pct_params_values')
    
    fh = open('./result_mcmc/alfa_40pct_emcee_fit.txt', 'w')
    fh.write(res.fit_report())

    res.flatchain.to_pickle('./result_mcmc/MCMC_chains_alfa_40pct')
    
    highest_prob = np.argmax(res.lnprob)
    hp_loc = np.unravel_index(highest_prob, res.lnprob.shape)
    mle_soln = res.chain[hp_loc]
    print("\nMaximum Likelihood Estimation (MLE) alfa 40%:")
    print('----------------------------------')
    for ix, param in enumerate(emcee_params):
        print(f"{param}: {mle_soln[ix]:.3f}")
        fh.write(f"{param}: {mle_soln[ix]:.3f}")

    quantiles = np.percentile(res.flatchain['mstar'], [2.28, 15.9, 50, 84.2, 97.7])
    print(f"\n\n1 sigma spread mstar = {0.5 * (quantiles[3] - quantiles[1]):.3f}")
    print(f"2 sigma spread mstar = {0.5 * (quantiles[4] - quantiles[0]):.3f}")
    fh.write(f"\n\n1 sigma spread mstar = {0.5 * (quantiles[3] - quantiles[1]):.3f}")
    fh.write(f"\n\n2 sigma spread mstar = {0.5 * (quantiles[4] - quantiles[0]):.3f}")
    fh.close() 
    
if __name__ == "__main__":
#    main_driver()
    plot_mcmc_chains()



