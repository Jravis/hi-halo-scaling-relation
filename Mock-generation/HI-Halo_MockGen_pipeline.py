
#####################################################
#Copyright (c) 2019, Sandeep Rana                   #
# This routine needs Python 3                       #
# This routine is to compute the effect of          #
# scatter into HI-halo abundance matching relation  #
# Following Behroozi 2010 and Ren 2019.             #
#This routine is for creating mock and computing    #
#clustering. Here we explore paramter space         #
#and it's effect on HI clustering with and without  #
#the scatter.                                       #
# For Cross Correlation I can write LZ estimator as #
# Xi_cross_corr = (D1D2 -(D1R + D2R) + RR)/RR       #
#####################################################

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import os
from scipy import integrate
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
from abundance import fit_abundance_halo, raw_abundance, HI_Mass_func, AM_nonparam, schechter_function
from astropy.cosmology import LambdaCDM
import datatable as dt
import lmfit
import argparse
from Corrfunc.theory.xi import xi as xi_corr
import Corrfunc
from Corrfunc.theory.wp import wp
from multiprocessing import Process
from astropy.io import ascii
from collections import OrderedDict
from typing import Optional, List, Dict
from halotools.empirical_models import Behroozi10SmHm


# ---------------- Global Values ---------------------------
from hmf import MassFunction
from hmf import cosmo

# Multidark 2 cosmology hmf model
OmegaMatter = 0.307115
OmegaLambda = 0.692885
H0 = 0.6777*100.
OmegaBaryon = 0.048206
new_model = LambdaCDM(H0=0.6777*100, Om0=OmegaMatter,
                      Tcmb0=2.73, Ob0=OmegaBaryon, Ode0=OmegaLambda)
# behroozi_model = PrebuiltSubhaloModelFactory('behroozi10', redshift=0)
behroozi_model = Behroozi10SmHm()
behroozi_model.param_dict['scatter_model_param1'] = 0.3
# -----------------------------------------------------------


def wp_corr(x, y, z, sx, sy, sz, mHI_mock, vx, vy, vz, sigma, fname, dir_name):
    """"""
    cutoff = np.array([9.4, 9.6, 9.8, 10.0, 10.2])
    cutoff_name = np.array(['9pt4', '9pt6', '9pt8', '10pt0', '10pt2'])
    nthreads = 32
    rmin = 0.1
    rmax = 25.11886431509582
    log_delta_r = 0.2
    rbins = 10**np.arange(np.log10(rmin), np.log10(rmax), log_delta_r)
    rbin_mid = [(rbins[i]+rbins[i+1])/2. for i in range(len(rbins)-1)]
    pimax = 20.0
    boxsize = 400  # h^-1 Mpc
    if not os.path.exists(dir_name + '/clust_data'):
        os.mkdir(dir_name+'/clust_data')

    for i in range(len(cutoff)):
        keep = (mHI_mock > cutoff[i])
        x1 = x[keep]
        y1 = y[keep]
        z1 = z[keep]
        sx1 = sx[keep]
        sy1 = sy[keep]
        sz1 = sz[keep]

        vx1 = vx[keep]
        vy1 = vy[keep]
        vz1 = vz[keep]

        log10_mHI = mHI_mock[keep]
        weights = np.ones_like(x1)
        fname1 = dir_name + \
            "/clust_data/real_HIMockCat_gt_%s_sigma_%0.2f_SMDPL.txt" % (
                cutoff_name[i], sigma)

        table = [log10_mHI, x1, y1, z1, vx1, vy1, vz1, sx1, sy1, sz1]
        names = ['log10_mHI', 'x', 'y', 'z',
                 'vx', 'vy', 'vz', 'sx', 'sy', 'sz']
        Formats = {
            'log10(m_HI)': '%2.14f', 'x': '%2.14f',
            'y': '%2.14f', 'z': '%2.14f', 'vx': '%2.14f',
            'vy': '%2.14f', 'vz': '%2.14f', 'sx': '%2.14f',
            'sy': '%2.14f', 'sz': '%2.14f'
        }
        ascii.write(table, fname1, names=names, delimiter='\t',
                    formats=Formats, overwrite=True)

        HISelect_bestFit_Params = wp(boxsize, pimax, nthreads, rbins, x1, y1, z1, weights=weights,
                                     weight_type='pair_product', verbose=True, output_rpavg=True)

        f = open(fname + "_gt_%s_sigma_%0.2f_SMDPL.txt" %
                 (cutoff_name[i], sigma), "w")
        f.write("#rbin\trpavg\twp\tnpairs\tweightavg\n")
        count = 0
        for r in HISelect_bestFit_Params:
            f.write("{0:10.6f}\t{1:10.6f}\t{2:10.6f}\t{3:10d}\t{4:10.6f}\n".
                    format(rbin_mid[count], r['rpavg'], r['wp'], r['npairs'], r['weightavg']))
            count += 1
        f.close()


def make_mock_HI(logMvir: np.ndarray,
                 G: np.ndarray, dn_dG: np.ndarray,
                 H: np.ndarray, dn_dH: np.ndarray,
                 halo_param_dict: Dict,
                 inp_params_dict: Dict) -> None:
    """_summary_

    """

    NBINS = inp_params_dict['NBINS']
    logMhalo_max = inp_params_dict['logMhalo_max']
    logMhalo_min = inp_params_dict['logMhalo_min']
    Lbox = inp_params_dict['Lbox']
    sigma = inp_params_dict['scatter']
    frac = inp_params_dict['frac']
    mstar = inp_params_dict['mstar']
    beta = inp_params_dict['beta']

    Halox = halo_param_dict['Halox']
    Haloy = halo_param_dict['Haloy']
    Haloz = halo_param_dict['Haloz']

    Halovx = halo_param_dict['Halovx']
    Halovy = halo_param_dict['Halovy']
    Halovz = halo_param_dict['Halovz']
    Halo_Id = halo_param_dict['HaloId']
    
    # Compute mean stellar mass with using Bheroozi 2010
    
    print('Computing Mean stellar_mass and scatter stellar mass using Behroozi 2010 relation')
    mean_sm = behroozi_model.mean_stellar_mass(prim_haloprop=10**logMvir, redshift=0)
    scatter = behroozi_model.scatter_realization(prim_haloprop=10**logMvir, redshift=0, seed=12345)
    logMean_sm = np.log10(mean_sm)
    logMean_sm_scatter = logMean_sm + scatter


    foo, dn_dH_func_mod = AM_nonparam(G, dn_dG, H, dn_dH, sigma,
                                      frac, mstar, beta, analytic=False,
                                      P_x=False, HI_selected_MF=True,
                                      H_min=6, H_max=logMhalo_max,
                                      nH=NBINS, tol_err=0.01)

    HI_halo_func_scatt, HI_halo_func_ini = AM_nonparam(G, dn_dG, H, dn_dH, sigma,
                                                       frac, mstar, beta, analytic=False,
                                                       P_x=True, HI_selected_MF=True,
                                                       H_min=6, H_max=logMhalo_max,
                                                       nH=NBINS, tol_err=0.01)

    NBINS = 50
    xlim = [logMhalo_min, logMhalo_max]    # log10(MHI)
    delta_mf = (xlim[1]-xlim[0])/float(NBINS)
    H1 = np.arange(xlim[0], xlim[1]+delta_mf, delta_mf)

    Mock_HI = np.zeros(1, dtype=np.float64)
    Mock_id = np.zeros(1, dtype=np.float64)
    Mock_id_cf = np.zeros(1, dtype=np.float64)
    Mock_Halo = np.zeros(1, dtype=np.float64)
    Mock_Halo_cf = np.zeros(1, dtype=np.float64)
    Mock_HI_scatter = np.zeros(1, dtype=np.float64)
    

    Mock_sm = np.zeros(1, dtype=np.float64)
    Mock_sm_cf = np.zeros(1, dtype=np.float64)
    Mock_sm_scatter = np.zeros(1, dtype=np.float64)
    Mock_sm_scatter_cf = np.zeros(1, dtype=np.float64)



    Mock_x = np.zeros(1, dtype=np.float64)
    Mock_y = np.zeros(1, dtype=np.float64)
    Mock_z = np.zeros(1, dtype=np.float64)
    Mock_vx = np.zeros(1, dtype=np.float64)
    Mock_vy = np.zeros(1, dtype=np.float64)
    Mock_vz = np.zeros(1, dtype=np.float64)

    Mock_x_cf = np.zeros(1, dtype=np.float64)
    Mock_y_cf = np.zeros(1, dtype=np.float64)
    Mock_z_cf = np.zeros(1, dtype=np.float64)
    Mock_vx_cf = np.zeros(1, dtype=np.float64)
    Mock_vy_cf = np.zeros(1, dtype=np.float64)
    Mock_vz_cf = np.zeros(1, dtype=np.float64)

    Mock_sx = np.zeros(1, dtype=np.float64)
    Mock_sy = np.zeros(1, dtype=np.float64)
    Mock_sz = np.zeros(1, dtype=np.float64)
    Mock_sx_cf = np.zeros(1, dtype=np.float64)
    Mock_sy_cf = np.zeros(1, dtype=np.float64)
    Mock_sz_cf = np.zeros(1, dtype=np.float64)

    in_seed = 68927
    np.random.seed(in_seed)

    boxsize = 400  # h^-1 Mpc
    redshift = 0.0
    H_z = new_model.H(redshift).value
    HubbleParam = 0.6777
    # SMDPL is in units of Mpc h^-1
    boost_factor = np.sqrt(1. + redshift)*1./H_z

    for i in range(len(H1)-1):

        def y(x): return dn_dH_func_mod(x)
        temp2, err = integrate.quad(y, H1[i], H1[i+1])

        keep = (logMvir >= H1[i])*(logMvir < H1[i+1])
        halo_in = logMvir[keep]

        halo_id_in = Halo_Id[keep]
        halo_xin = Halox[keep]
        halo_yin = Haloy[keep]
        halo_zin = Haloz[keep]

        halo_vxin = Halovx[keep]
        halo_vyin = Halovy[keep]
        halo_vzin = Halovz[keep]
        
        halo_sm = logMean_sm[keep]
        halo_sm_scatter = logMean_sm_scatter[keep]
        
        # Here we want to pick randomly a single halo out of set
        # halo (count permitted by HI selected halo mass function)
        rand_pick_halo = np.random.randint(
            0, len(halo_in), size=np.int64(temp2*Lbox**3), dtype=np.int64)
        rand_pick_halo = np.unique(rand_pick_halo)
        
        u1 = np.random.normal(0, sigma, size=len(rand_pick_halo))
        
        rand_pick_halo_all = np.arange(len(halo_in), dtype=np.int64)
        rand_pick_halo_cf = rand_pick_halo_all[~np.isin(rand_pick_halo_all, rand_pick_halo)]
        

        # associating HI to a Halo randomly from set of halo.
        temp = halo_in[rand_pick_halo]
        Mock_Halo = np.concatenate((Mock_Halo, temp))

        temp = halo_id_in[rand_pick_halo]
        Mock_id = np.concatenate((Mock_id, temp))

        mock_HI = HI_halo_func_ini(halo_in[rand_pick_halo])
        Mock_HI = np.concatenate((Mock_HI, mock_HI))

        mock_HI_scatt = HI_halo_func_scatt(halo_in[rand_pick_halo])+u1
        Mock_HI_scatter = np.concatenate((Mock_HI_scatter, mock_HI_scatt))
        
        mock_sm = halo_sm[rand_pick_halo]
        Mock_sm = np.concatenate((Mock_sm, mock_sm))
        
        mock_sm_scatter = halo_sm_scatter[rand_pick_halo]
        Mock_sm_scatter = np.concatenate((Mock_sm_scatter, mock_sm_scatter))

        temp_pos = halo_xin[rand_pick_halo]
        temp_vel = halo_vxin[rand_pick_halo]
        temp_pos_rsd = temp_pos + temp_vel * boost_factor * HubbleParam
        temp_pos_rsd = temp_pos_rsd % boxsize
        
        Mock_vx = np.concatenate((Mock_vx, temp_vel))
        Mock_x = np.concatenate((Mock_x, temp_pos))
        Mock_sx = np.concatenate((Mock_sx, temp_pos_rsd))

        temp_pos = halo_yin[rand_pick_halo]
        temp_vel = halo_vyin[rand_pick_halo]
        temp_pos_rsd = temp_pos + temp_vel * boost_factor * HubbleParam
        temp_pos_rsd = temp_pos_rsd % boxsize

        Mock_vy = np.concatenate((Mock_vy, temp_vel))
        Mock_y = np.concatenate((Mock_y, temp_pos))
        Mock_sy = np.concatenate((Mock_sy, temp_pos_rsd))

        temp_pos = halo_zin[rand_pick_halo]
        temp_vel = halo_vzin[rand_pick_halo]
        temp_pos_rsd = temp_pos + temp_vel * boost_factor * HubbleParam
        temp_pos_rsd = temp_pos_rsd % boxsize

        Mock_vz = np.concatenate((Mock_vz, temp_vel))
        Mock_z = np.concatenate((Mock_z, temp_pos))
        Mock_sz = np.concatenate((Mock_sz, temp_pos_rsd))

        # associate HI mass as -9999.0 to rest of the unselected halos.
        temp = halo_in[rand_pick_halo_cf]
        Mock_Halo_cf = np.concatenate((Mock_Halo_cf, temp))

        temp = halo_id_in[rand_pick_halo_cf]
        Mock_id_cf = np.concatenate((Mock_id_cf, temp))
        
        mock_sm = halo_sm[rand_pick_halo_cf]
        Mock_sm_cf = np.concatenate((Mock_sm_cf, mock_sm))
        
        mock_sm_scatter = halo_sm_scatter[rand_pick_halo_cf]
        Mock_sm_scatter_cf = np.concatenate((Mock_sm_scatter_cf, mock_sm_scatter))


        temp_pos = halo_xin[rand_pick_halo_cf]
        temp_vel = halo_vxin[rand_pick_halo_cf]
        temp_pos_rsd = temp_pos + temp_vel * boost_factor * HubbleParam
        temp_pos_rsd = temp_pos_rsd % boxsize
        
        Mock_vx_cf = np.concatenate((Mock_vx_cf, temp_vel))
        Mock_x_cf = np.concatenate((Mock_x_cf, temp_pos))
        Mock_sx_cf = np.concatenate((Mock_sx_cf, temp_pos_rsd))

        temp_pos = halo_yin[rand_pick_halo_cf]
        temp_vel = halo_vyin[rand_pick_halo_cf]
        temp_pos_rsd = temp_pos + temp_vel * boost_factor * HubbleParam
        temp_pos_rsd = temp_pos_rsd % boxsize
        
        Mock_vy_cf = np.concatenate((Mock_vy_cf, temp_vel))
        Mock_y_cf = np.concatenate((Mock_y_cf, temp_pos))
        Mock_sy_cf = np.concatenate((Mock_sy_cf, temp_pos_rsd))

        temp_pos = halo_zin[rand_pick_halo_cf]
        temp_vel = halo_vzin[rand_pick_halo_cf]
        temp_pos_rsd = temp_pos + temp_vel * boost_factor * HubbleParam
        temp_pos_rsd = temp_pos_rsd % boxsize
        
        Mock_vz_cf = np.concatenate((Mock_vz_cf, temp_vel))
        Mock_z_cf = np.concatenate((Mock_z_cf, temp_pos))
        Mock_sz_cf = np.concatenate((Mock_sz_cf, temp_pos_rsd))

    

    # Some Hard checkes
    index = (Mock_sz<0) 
    assert any(list(index))==False
    index = (Mock_sz> boxsize)
    assert any(list(index))==False
    

    index = (Mock_sx<0) 
    assert any(list(index))==False
    index = (Mock_sx> boxsize)
    assert any(list(index))==False

    index = (Mock_sy<0) 
    assert any(list(index))==False
    index = (Mock_sy> boxsize)
    assert any(list(index))==False


    index = (Mock_sz_cf<0) 
    assert any(list(index))==False
    index = (Mock_sz_cf> boxsize)
    assert any(list(index))==False
    
    index = (Mock_sx_cf<0) 
    assert any(list(index))==False
    index = (Mock_sx_cf> boxsize)
    assert any(list(index))==False

    index = (Mock_sy_cf<0) 
    assert any(list(index))==False
    index = (Mock_sy_cf> boxsize)
    assert any(list(index))==False


    print('\n')
    print('||--------- Computing wp(rp) for scatter and non-scatter case --------||')
    print('\n')
    dir_name = '/mnt/data1/sandeep/multidark2_data/'\
        'pipeline_MasterCat_alfa70_Mvir/HI_analysis_sigma_{}'.format(
            sigma)

    if not os.path.exists(dir_name):
        os.mkdir(dir_name)
    
    # save HI Mock Catalog
    fname1 = dir_name + \
        '/HIMockCat_alfa70_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}_scatter_{3:0.2f}.txt'.format(
            frac, mstar, beta, sigma)
    table = [
        Mock_id[1:], Mock_Halo[1:], Mock_HI[1:], Mock_HI_scatter[1:], Mock_sm[1:], Mock_sm_scatter[1:], Mock_x[1:], Mock_y[1:],
        Mock_z[1:], Mock_vx[1:], Mock_vy[1:], Mock_vz[1:], Mock_sx[1:], Mock_sy[1:], Mock_sz[1:]
    ]
    names = [
        'rockStarId', 'log10(MHalo)', 'log10(MHI)', 'log10(MHI_scatter)', 'log10(mean_sm)',
       'log10(mean_sm_scatter)', 'x', 'y', 'z', 'vx', 'vy', 'vz', 'sx', 'sy', 'sz'
    ]
    Formats = {
        'rockStarId': '%ld', 'log10(MHalo)': '%2.14f', 'log10(MHI)': '%2.14f',
        'log10(MHI_scatter)': '%2.14f', 'log10(mean_sm)': '%2.14f','log10(mean_sm_scatter)': '%2.14f',
        'x': '%2.14f', 'y': '%2.14f', 'z': '%2.14f', 'vx': '%2.14f',
        'vy': '%2.14f', 'vz': '%2.14f', 'sx': '%2.14f',
        'sy': '%2.14f', 'sz': '%2.14f'
    }
    ascii.write(table, fname1, names=names, delimiter='\t',
                formats=Formats, overwrite=True)

    # Compute mean stellar mass with using Bheroozi 2010
    tmp_HI_mass = -9999.0 * np.ones(len(Mock_Halo_cf[1:]), dtype=np.float64)

    fname1 = dir_name + \
        '/HaloMockCat_SMDPL_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}_scatter_{3:0.2f}.txt'.format(
            frac, mstar, beta, sigma)
    table = [
        Mock_id_cf[1:], Mock_Halo_cf[1:], tmp_HI_mass, tmp_HI_mass,  Mock_sm_cf[1:], Mock_sm_scatter_cf[1:], Mock_x_cf[1:], Mock_y_cf[1:],
        Mock_z_cf[1:], Mock_vx_cf[1:], Mock_vy_cf[1:], Mock_vz_cf[1:], Mock_sx_cf[1:], Mock_sy_cf[1:], Mock_sz_cf[1:]
    ]
    names = [
        'rockStarId', 'log10(MHalo)', 'log10(MHI)', 'log10(MHI_scatter)', 'log10(mean_sm)',
        'log10(mean_sm_scatter)', 'x', 'y', 'z', 'vx', 'vy', 'vz', 'sx', 'sy', 'sz'
    ]
    Formats = {
        'rockStarId': '%ld', 'log10(MHalo)': '%2.14f', 'log10(MHI)': '%2.14f',
        'log10(MHI_scatter)': '%2.14f', 'log10(mean_sm)': '%2.14f','log10(mean_sm_scatter)': '%2.14f',
        'x': '%2.14f', 'y': '%2.14f', 'z': '%2.14f', 'vx': '%2.14f',
        'vy': '%2.14f', 'vz': '%2.14f', 'sx': '%2.14f',
        'sy': '%2.14f', 'sz': '%2.14f'
    }
    ascii.write(table, fname1, names=names, delimiter='\t',
                formats=Formats, overwrite=True)

    fname = (
        dir_name+'/HI_alfa70_wp_corr_f_{0:0.2f}_Mstar_{1:0.2f}_beta_{2:0.2f}'.format(frac, mstar, beta))
    wp_corr(Mock_x[1:], Mock_y[1:], Mock_z[1:], Mock_sx[1:], Mock_sy[1:], Mock_sz[1:],
            Mock_HI_scatter[1:], Mock_vx[1:], Mock_vy[1:], Mock_vz[1:], sigma, fname, dir_name)

    wp_corr(Mock_x[1:], Mock_y[1:], Mock_z[1:], Mock_sx[1:], Mock_sy[1:], Mock_sz[1:],
            Mock_HI[1:], Mock_vx[1:], Mock_vy[1:], Mock_vz[1:], 0.0, fname, dir_name)


def main_driver() -> None:

    # Read User given parameters
    parser = argparse.ArgumentParser()
    parser.add_argument("sigma", type=float,
                        help='Input scatter e.g 0.25')
    parser.add_argument("use_schechter", type=int,
                        help='if 1 use schecter function else use linear interpolation')

    args = parser.parse_args()
    scatter_inp = args.sigma  # input scatter that is needed
    # do you want Schechter fit(1) or linear extrapolation
    use_schechter = args.use_schechter

    # Read Rockstar Halo catalog for SMDPL
    # Halo_Cat_fname = ("/mnt/data1/sandeep/multidark2_data/halo_cat/"\
    #                 "halo_data/Rockstar_SMDPL_halo_properties.csv")

    Halo_Cat_fname = ("/mnt/data1/sandeep/multidark2_data/halo_cat/"
                      "halo_data/Rockstar_SMDPL_Halo_Mpeak.csv")

    # Box Size for SMDPL
    # SMDPL cosmology uses h = 0.6777
    Lbox = 400/0.6777  # h^-1 Mpc taken out h^-1 Mpc

    print("Reading Rockstar SMDPL Halo Catalog (Mvir and positions)\n")
    # print("Reading Rockstar SMDPL Halo Catalog (Mpeak and positions)\n")

    MDPLpropdata = dt.fread(Halo_Cat_fname)

    HaloId = np.ndarray.flatten(MDPLpropdata['rockstarId'].to_numpy())
    # as Mvir is h^-1 Msun units taken out h^-1
    Mvir = np.ndarray.flatten(MDPLpropdata['Mvir'].to_numpy())/0.6777
    # Mvir   = np.ndarray.flatten(MDPLpropdata['Mpeak'].to_numpy())/0.6777 # as Mvir is h^-1 Msun units taken out h^-1

    # Here positions are comoving positions
    # as X is h^-1 Mpc units
    Halox = np.ndarray.flatten(MDPLpropdata['x'].to_numpy())
    # as Y is h^-1 Mpc units
    Haloy = np.ndarray.flatten(MDPLpropdata['y'].to_numpy())
    # as Z is h^-1 Mpc units
    Haloz = np.ndarray.flatten(MDPLpropdata['z'].to_numpy())
    
    Halovx = np.ndarray.flatten(
        MDPLpropdata['vx'].to_numpy())  # as Z is h^-1 Mpc units
    Halovy = np.ndarray.flatten(
        MDPLpropdata['vy'].to_numpy())  # as Z is h^-1 Mpc units
    Halovz = np.ndarray.flatten(
        MDPLpropdata['vz'].to_numpy())  # as Z is h^-1 Mpc units
    print("Releasing unused memory\n")
    del MDPLpropdata

    print("||----- Fitting Halo mass function and getting bestfit Schechter function parameter -----||\n")

    logMhalo_min = 10.0
    logMhalo_max = 15.5
    NBINS = 20

    xlim = [logMhalo_min, logMhalo_max]
    deltaM = (xlim[1]-xlim[0])/np.float64(NBINS)
    logMbins = np.arange(xlim[0], xlim[1]+deltaM, deltaM)

    # initial guess for parameters to fit function
    p_ini = dict(phi1=0.001, x1=14, alpha1=-1.0)
    result_SF, logMbins_avg, dn_dH_ini, err, dn_dH_func = fit_abundance_halo(Mvir, weights=1.0/Lbox**3.0,
                                                                             bins=logMbins, xlog=True,
                                                                             fit_type='schechter',
                                                                             p=p_ini, show_fit=False)

    print("||----- Fitting Halo MF intial points for linear interpolation -----||\n")
    # non-linear generalized fitting library

    def Lin_Model(x, m, c):
        return m*x+c

    Model = lmfit.Model(Lin_Model)
    # initial guess for parameters to fit function
    p = dict(m=0.86298182, c=1.)

    # create Parameters, giving initial values
    params = Model.make_params(m=p['m'], c=p['c'])
    # do fit, print result
    X = logMbins_avg[3:6]
    Y = np.log10(dn_dH_ini[3:6])
    Lin_result = Model.fit(Y, params, x=X)
    print(Lin_result.fit_report())
    print("--------------------------------------------------\n")

    print("||----- Reading Guo Points -----||\n")

    guo_H = np.genfromtxt("../../../sailis-data/guo_points.dat", usecols=0)
    guo_HI_halo = np.genfromtxt("../../../sailis-data/guo_points.dat", usecols=1)

    print("||----- Getting alfa70% mass function -----||\n")
    NBINS = 1000
    xlim = [6, 11]    # log10(MHI)
    delta_mf = (xlim[1]-xlim[0])/float(NBINS)
    G = np.arange(xlim[0], xlim[1]+delta_mf, delta_mf)
    dn_dG = 10**HI_Mass_func(G)         # Alfa-alfa HI mass function

    print("||----- Getting halo mass function -----||\n")
    xlim = [6, logMhalo_max]    # log10(MHI)
    delta_mf = (xlim[1]-xlim[0])/float(NBINS)
    H = np.arange(xlim[0], xlim[1]+delta_mf, delta_mf)

    # Here using best fit schechter func params to get halo mass func.
    if use_schechter == 1:
        print("||----- Using Schechter Linear fit -----||\n")
        dn_dH = schechter_function(H, result_SF.params['phistar'].value,
                                   result_SF.params['alpha'].value,
                                   result_SF.params['mstar'].value)
    else:
        print("||----- Using HMF Linear fit -----||\n")
        dn_dH = dn_dH_func(H)
        ind = (H > logMbins_avg[3])
        y = Lin_result.params['m'].value*H[~ind] + Lin_result.params['c']
        y1 = dn_dH_func(H[ind])
        dn_dH = np.concatenate((10**y, y1))

    # Calling AM_nonparam routine for iterative deconvolution to get abundance matching
    # with without scatter, if P_x is false then
    # return HI-Halo AM match without scatter, halo MF interpolation function
    HI_halo_func_ini1, dn_dH_func_ini = AM_nonparam(G, dn_dG, H, dn_dH,
                                                    scatter_inp, 0, 0, 0,
                                                    analytic=False, P_x=False,
                                                    H_min=6, H_max=logMhalo_max,
                                                    nH=NBINS, tol_err=0.01)

    print("||----- Fitting for HI-Selected Mass function ----||\n")

    def my_model(x, frac, mstar, beta):
        HI_halo_func_ini2, dn_dH_func_mod = AM_nonparam(G, dn_dG, H, dn_dH,
                                                        scatter_inp, frac, mstar,
                                                        beta, analytic=False,
                                                        HI_selected_MF=True,
                                                        P_x=False, H_min=6,
                                                        H_max=logMhalo_max,
                                                        nH=NBINS, tol_err=0.01)
        ratio = dn_dH_func_ini(x)/dn_dH_func_mod(x)
        return HI_halo_func_ini2(x)-np.log10(ratio)

    Model = lmfit.Model(my_model)

    # initial guess for parameters to fit function
    p = dict(frac=0.3204, mstar=13.6609, beta=0.9957)
    # create Parameters, giving initial values
    params = Model.make_params(
        frac=p['frac'], mstar=p['mstar'], beta=p['beta'])
    params['frac'].min = 0.1
    params['mstar'].min = 11.0
    params['beta'].min = 0.000

    params['frac'].max = 0.8
    params['mstar'].max = 14.5
    params['beta'].max = 2.0

    # do fit, print result
    HISelect_bestFit_Params = Model.fit(guo_HI_halo, params, x=guo_H)

    print(HISelect_bestFit_Params.fit_report())
    print("\n")

    f = open("guo_SMDPL_best_fit_alfa70_Mvir.txt", "w")
    f.write("{0:0.6f}\t{1:0.6f}\t{2:0.6f}\n".format(HISelect_bestFit_Params.params['frac'].value,
                                                    HISelect_bestFit_Params.params['mstar'].value,
                                                    HISelect_bestFit_Params.params['beta'].value))
    f.close()
    logMvir = np.log10(Mvir)

    inp_params_dict = {
        'NBINS': NBINS,
        'logMhalo_max': logMhalo_max,
        'logMhalo_min': logMhalo_min,
        'Lbox': Lbox,
        'scatter': scatter_inp,
        'frac': HISelect_bestFit_Params.params['frac'].value,
        'mstar': HISelect_bestFit_Params.params['mstar'].value,
        'beta': HISelect_bestFit_Params.params['beta'].value
    }

    halo_param_dict = {
        'Halox': Halox,
        'Haloy': Haloy,
        'Haloz': Haloz,
        'Halovx': Halovx,
        'Halovy': Halovy,
        'Halovz': Halovz,
        'HaloId': HaloId
    }
    make_mock_HI(logMvir, G, dn_dG, H, dn_dH, halo_param_dict, inp_params_dict)


if __name__ == "__main__":
    main_driver()
