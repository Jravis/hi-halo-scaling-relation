################################################################
# Copyright (c) 2019, Sandeep Rana
# This routine python 3 and computes the
# cross-correlation between halo and HI associated
# with halos. Also we will compute cross correlation
# between stellar-mass and HI associated
################################################################

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

import numpy as np
import os
from astropy.cosmology import LambdaCDM
from datatable import dt, f
from Corrfunc.theory.xi import xi as xi_corr
import argparse
from Corrfunc.theory.wp import wp
from Corrfunc.theory.DD import DD
from Corrfunc.theory.DDrppi import DDrppi
from Corrfunc.utils import convert_3d_counts_to_cf
from Corrfunc.utils import convert_rp_pi_counts_to_wp
# from astropy.io import ascii
# from collections import OrderedDict
import pandas as pd
from typing import Optional, List, Dict


# ---------------- Global Values ---------------------------
from hmf import MassFunction
from hmf import cosmo

# Multidark 2 cosmology hmf model
OmegaMatter = 0.307115
OmegaLambda = 0.692885
H0 = 0.6777*100.
OmegaBaryon = 0.048206
new_model = LambdaCDM(H0=0.6777*100, Om0=OmegaMatter,
                      Tcmb0=2.73, Ob0=OmegaBaryon, Ode0=OmegaLambda)


def get_xi_autoCorr(samp_df, rmin=None, rmax=None, 
                    log_delta_r=None, nthreads=None, redshift=False):

    nthreads = 31
    if not rmin:
        rmin = 0.1
    if not rmax:
        rmax = 30
    if not log_delta_r:
        log_delta_r = 0.1

    # get line of sight binning.
    rbins = 10**np.arange(np.log10(rmin), np.log10(rmax), log_delta_r)
    rbin_mid = [(rbins[i]+rbins[i+1])/2. for i in range(len(rbins)-1)]
    boxsize = 400  # h^-1 Mpc
    x1 = samp_df['x'].to_numpy()
    y1 = samp_df['y'].to_numpy()
    if redshift:
        z1 = samp_df['sz'].to_numpy()
    else:
        z1 = samp_df['z'].to_numpy()

    weights = np.ones_like(x1)
    xi_ = xi_corr(boxsize, nthreads, rbins, x1, y1, z1,
                      weights=weights, weight_type='pair_product', output_ravg=True)

    xi_autocorr = [r['xi'] for r in xi_]
    return xi_autocorr, rbin_mid


def get_wp_autoCorr(samp_df, rmin=None, rmax=None, 
                    log_delta_r=None, nthreads=None, redshift=False):

    nthreads = 32
    if not rmin:
        rmin = 0.1
    if not rmax:
        rmax = 25.11886431509582
    if not pimax:
        pimax = 20.0
    if not log_delta_r:
        log_delta_r = 0.2

    # get line of sight binning.
    rbins = 10**np.arange(np.log10(rmin), np.log10(rmax), log_delta_r)
    rbin_mid = [(rbins[i]+rbins[i+1])/2. for i in range(len(rbins)-1)]

    boxsize = 400  # h^-1 Mpc

    x1 = samp_df['x'].to_numpy()
    y1 = samp_df['y'].to_numpy()
    if redshift:
        z1 = samp_df['sz'].to_numpy()
    else:
        z1 = samp_df['z'].to_numpy()

    weights = np.ones_like(x1)
    wp_ = wp(boxsize, pimax, nthreads, rbins, x1, y1, z1, weights=weights,
             weight_type='pair_product', verbose=True, output_rpavg=True)

    wp_autocorr = [r['wp'] for r in wp_]
    return wp_autocorr, rbin_mid


def get_xi_CrossCorr_test(sampOne_df, sampTwo_df, rmin=None, rmax=None, 
                          log_delta_r=None, nthreads=None, redshift=False):
    """ Cross-correlation functions using a modified version of
    the LS estimator (following Zehavi et al. 2011):
    Xi_cross_corr = (DD1DD2 - DD1RR2 - DD2RR1 + RR1 RR2)/RR1RR2
    Now just to remember:
    DD1 = pair count in first sample (for e.g., halo catalog)
    DD2 = pair count in second sample (for e.g., HI catalog)
    RR2 = pair count in first sample (for e.g., Random Catalog)
    Similarly, RR2 = pair count in second sample
    Now this gets tricky for observations as two smaple can have different
    Selection functions and to have a random sample in the representative
    area of interest we need to create two different random catalog RR1 RR2
    However, in simulations life is bit simple and we can have a common
    Random Catalog.
    Xi_cross_corr = (D1D2 - D1R2 - D2R1 + R1R2)/R1R2
    """

    nthreads = 31
    if not rmin:
        rmin = 0.1
    if not rmax:
        rmax = 30
    if not log_delta_r:
        log_delta_r = 0.1

    # get line of sight binning.
    rbins = 10**np.arange(np.log10(rmin), np.log10(rmax), log_delta_r)
    rbin_mid = [(rbins[i]+rbins[i+1])/2. for i in range(len(rbins)-1)]

    boxsize = 400  # h^-1 Mpc

    x1 = sampOne_df['x'].to_numpy()
    y1 = sampOne_df['y'].to_numpy()
    if redshift:
        z1 = sampOne_df['sz'].to_numpy()
    else:
        z1 = sampOne_df['z'].to_numpy()

    x2 = sampTwo_df['x'].to_numpy()
    y2 = sampTwo_df['y'].to_numpy()
    if redshift:
        z2 = sampTwo_df['sz'].to_numpy()
    else:
        z2 = sampTwo_df['z'].to_numpy()

    N1 = np.int64(len(x1))
    N2 = np.int64(len(x2))
    rand_N1 = np.int64(2*len(x1))
    rand_N2 = np.int64(2*len(x2))

    # random catalog 1
    rx1 = np.random.uniform(0, boxsize, rand_N1)
    ry1 = np.random.uniform(0, boxsize, rand_N1)
    rz1 = np.random.uniform(0, boxsize, rand_N1)

    # random catalog 2
    rx2 = np.random.uniform(0, boxsize, rand_N2)
    ry2 = np.random.uniform(0, boxsize, rand_N2)
    rz2 = np.random.uniform(0, boxsize, rand_N2)

    weights = np.ones_like(x1)
    autocorr = 0
    D1D2 = DD(autocorr, nthreads, rbins, X1=x1, Y1=y1, Z1=z1,
              X2=x2, Y2=y2, Z2=z2, verbose=True, output_ravg=True,
              boxsize=boxsize, periodic=True)

    D1R2 = DD(autocorr, nthreads, rbins, X1=x1, Y1=y1, Z1=z1,
              X2=rx2, Y2=ry2, Z2=rz2, verbose=True, output_ravg=True,
              boxsize=boxsize, periodic=True)

    D2R1 = DD(autocorr, nthreads, rbins, X1=x2, Y1=y2, Z1=z2,
              X2=rx1, Y2=ry1, Z2=rz1, verbose=True, output_ravg=True,
              boxsize=boxsize, periodic=True)

    R1R2 = DD(autocorr, nthreads, rbins, X1=rx1, Y1=ry1, Z1=rz1, X2=rx2, Y2=ry2, Z2=rz2,
              verbose=True, output_ravg=True, boxsize=boxsize, periodic=True)

    cf = convert_3d_counts_to_cf(N1, N2, rand_N1, rand_N2, D1D2=D1D2, D1R2=D1R2, D2R1=D2R1,
                                 R1R2=R1R2)

    return cf 


def get_xi_CrossCorr(sampOne_df, sampTwo_df, rmin=None, rmax=None, 
                     log_delta_r=None, nthreads=None, redshift=False):
    """ Cross-correlation functions using a modified version of
    the LS estimator (following Zehavi et al. 2011):
    Xi_cross_corr = (DD1DD2 - DD1RR2 - DD2RR1 + RR1 RR2)/RR1RR2
    Now just to remember:
    DD1 = pair count in first sample (for e.g., halo catalog)
    DD2 = pair count in second sample (for e.g., HI catalog)
    RR2 = pair count in first sample (for e.g., Random Catalog)
    Similarly, RR2 = pair count in second sample
    Now this gets tricky for observations as two smaple can have different
    Selection functions and to have a random sample in the representative
    area of interest we need to create two different random catalog RR1 RR2
    However, in simulations life is bit simple and we can have a common
    Random Catalog.
    Xi_cross_corr = (D1D2 - D1R - D2R + RR)/RR
    """

    nthreads = 31
    if not rmin:
        rmin = 0.1
    if not rmax:
        rmax = 30
    if not log_delta_r:
        log_delta_r = 0.1

    # get line of sight binning.
    rbins = 10**np.arange(np.log10(rmin), np.log10(rmax), log_delta_r)
    rbin_mid = [(rbins[i]+rbins[i+1])/2. for i in range(len(rbins)-1)]

    boxsize = 400  # h^-1 Mpc

    x1 = sampOne_df['x'].to_numpy()
    y1 = sampOne_df['y'].to_numpy()
    if redshift:
        z1 = sampOne_df['sz'].to_numpy()
    else:
        z1 = sampOne_df['z'].to_numpy()

    x2 = sampTwo_df['x'].to_numpy()
    y2 = sampTwo_df['y'].to_numpy()
    if redshift:
        z2 = sampTwo_df['sz'].to_numpy()
    else:
        z2 = sampTwo_df['z'].to_numpy()

    N1 = np.int64(len(x1))
    N2 = np.int64(len(x2))
    rand_N = np.int64(2*len(x1))

    # random catalog
    rx = np.random.uniform(0, boxsize, rand_N)
    ry = np.random.uniform(0, boxsize, rand_N)
    rz = np.random.uniform(0, boxsize, rand_N)

    weights = np.ones_like(x1)
    autocorr = 0
    D1D2 = DD(autocorr, nthreads, rbins, X1=x1, Y1=y1, Z1=z1,
              X2=x2, Y2=y2, Z2=z2, verbose=True, output_ravg=True,
              boxsize=boxsize, periodic=True)

    D1R = DD(autocorr, nthreads, rbins, X1=x1, Y1=y1, Z1=z1,
             X2=rx, Y2=ry, Z2=rz, verbose=True, output_ravg=True,
             boxsize=boxsize, periodic=True)

    D2R = DD(autocorr, nthreads, rbins, X1=x2, Y1=y2, Z1=z2,
             X2=rx, Y2=ry, Z2=rz, verbose=True, output_ravg=True,
             boxsize=boxsize, periodic=True)

    RR = DD(1, nthreads, rbins, rx, ry, rz, verbose=True, output_ravg=True,
            boxsize=boxsize, periodic=True)

    cf = convert_3d_counts_to_cf(N1, N2, rand_N, rand_N, D1D2=D1D2, D1R2=D1R, D2R1=D2R,
                                 R1R2=RR)

    return cf 


def get_wp_CrossCorr(
        sampOne_df: pd.DataFrame,
        sampTwo_df: pd.DataFrame,
        rmin: Optional[float] = None,
        rmax: Optional[float] = None,
        pimax: Optional[float] = None,
        log_delta_r: Optional[float] = None,
        nthreads: Optional[int] = None,
        redshift: Optional[bool] = False):
    """ Cross-correlation functions using a modified version of
    the LS estimator (following Zehavi et al. 2011):
    Xi_cross_corr = (DD1DD2 - DD1RR2 - DD2RR1 + RR1 RR2)/RR1RR2
    Now just to remember:
    DD1 = pair count in first sample (for e.g., halo catalog)
    DD2 = pair count in second sample (for e.g., HI catalog)
    RR2 = pair count in first sample (for e.g., Random Catalog)
    Similarly, RR2 = pair count in second sample
    Now this gets tricky for observations as two smaple can have different
    Selection functions and to have a random sample in the representative
    area of interest we need to create two different random catalog RR1 RR2
    However, in simulations life is bit simple and we can have a common
    Random Catalog.
    Xi_cross_corr = (D1D2 - D1R - D2R + RR)/RR
    """

    nthreads = 32
    if not rmin:
        rmin = 0.1
    if not rmax:
        rmax = 25.11886431509582
    if not pimax:
        pimax = 20.0
    if not log_delta_r:
        log_delta_r = 0.2

    # get line of sight binning.
    rbins = 10**np.arange(np.log10(rmin), np.log10(rmax), log_delta_r)
    rbin_mid = [(rbins[i]+rbins[i+1])/2. for i in range(len(rbins)-1)]

    boxsize = 400  # h^-1 Mpc

    x1 = sampOne_df['x'].to_numpy()
    y1 = sampOne_df['y'].to_numpy()
    if redshift:
        z1 = sampOne_df['sz'].to_numpy()
    else:
        z1 = sampOne_df['z'].to_numpy()

    x2 = sampTwo_df['x'].to_numpy()
    y2 = sampTwo_df['y'].to_numpy()
    if redshift:
        z2 = sampTwo_df['sz'].to_numpy()
    else:
        z2 = sampTwo_df['z'].to_numpy()

    N1 = np.int64(len(x1))
    N2 = np.int64(len(x2))
    rand_N = np.int64(2*len(x1))

    # random catalog
    rx = np.random.uniform(0, boxsize, rand_N)
    ry = np.random.uniform(0, boxsize, rand_N)
    rz = np.random.uniform(0, boxsize, rand_N)

    weights = np.ones_like(x1)
    autocorr = 0  # for corss correlation
    D1D2 = DDrppi(autocorr, nthreads, pimax, rbins, X1=x1, Y1=y1, Z1=z1,
                  X2=x2, Y2=y2, Z2=z2, verbose=True, output_rpavg=True,
                  boxsize=boxsize, periodic=True)


    D1R = DDrppi(autocorr, nthreads, pimax, rbins, X1=x1, Y1=y1, Z1=z1,
                 X2=rx, Y2=ry, Z2=rz, verbose=True, output_rpavg=True,
                 boxsize=boxsize, periodic=True)

    D2R = DDrppi(autocorr, nthreads, pimax, rbins, X1=x2, Y1=y2, Z1=z2,
                 X2=rx, Y2=ry, Z2=rz, verbose=True, output_rpavg=True,
                 boxsize=boxsize, periodic=True)

    RR = DDrppi(1, nthreads, pimax, rbins, rx, ry, rz, verbose=True, output_rpavg=True,
                boxsize=boxsize, periodic=True)

    wp = convert_rp_pi_counts_to_wp(N1, N2, rand_N, rand_N, D1D2, D1R, D2R, RR,
                                    len(rbins)-1, pimax)

    return wp


def get_2dxi(
        filename: str,
        sampOne_df: pd.DataFrame,
        sampTwo_df: pd.DataFrame,
        rmin: Optional[float] = None,
        rmax: Optional[float] = None,
        pimax: Optional[float] = None,
        sigma_bins: Optional[int] = None,
        nthreads: Optional[int] = None,
        redshift: Optional[bool] = False):
    """ Cross-correlation functions using a modified version of
    the LS estimator (following Zehavi et al. 2011):
    Xi_cross_corr = (DD1DD2 - DD1RR2 - DD2RR1 + RR1 RR2)/RR1RR2
    Now just to remember:
    DD1 = pair count in first sample (for e.g., halo catalog)
    DD2 = pair count in second sample (for e.g., HI catalog)
    RR2 = pair count in first sample (for e.g., Random Catalog)
    Similarly, RR2 = pair count in second sample
    Now this gets tricky for observations as two smaple can have different
    Selection functions and to have a random sample in the representative
    area of interest we need to create two different random catalog RR1 RR2
    However, in simulations life is bit simple and we can have a common
    Random Catalog.
    Xi_cross_corr = (D1D2 - D1R - D2R + RR)/RR
    """

    nthreads = 32
    if not rmin:
        rmin = 0.0
    if not rmax:
        rmax = 20.0
    if not pimax:
        pimax = 20.0
    if not sigma_bins:
        sigma_bins = 20

    # get line of sight binning.
    rbins = np.linspace(rmin, rmax, sigma_bins, endpoint=True)

    boxsize = 400  # h^-1 Mpc

    x1 = sampOne_df['x'].to_numpy()
    y1 = sampOne_df['y'].to_numpy()
    if redshift:
        z1 = sampOne_df['sz'].to_numpy()
    else:
        z1 = sampOne_df['z'].to_numpy()

    x2 = sampTwo_df['x'].to_numpy()
    y2 = sampTwo_df['y'].to_numpy()

    if redshift:
        z2 = sampTwo_df['sz'].to_numpy()
    else:
        z2 = sampTwo_df['z'].to_numpy()

    N1 = np.int64(len(x1))
    N2 = np.int64(len(x2))
    rand_N = np.int64(2*len(x1))

    # random catalog
    rx = np.random.uniform(0, boxsize, rand_N)
    ry = np.random.uniform(0, boxsize, rand_N)
    rz = np.random.uniform(0, boxsize, rand_N)

    autocorr = 0  # for corss correlation
    D1D2 = DDrppi(autocorr, nthreads, pimax, rbins, X1=x1, Y1=y1, Z1=z1,
                  X2=x2, Y2=y2, Z2=z2, verbose=True, output_rpavg=True,
                  boxsize=boxsize, periodic=True)
    rpavg = [r['rpavg'] for r in D1D2]
    pimax_arr = [r['pimax'] for r in D1D2]
    rmin_arr = [r['rmin'] for r in D1D2]
    rmax_arr = [r['rmax'] for r in D1D2]

    D1R = DDrppi(autocorr, nthreads, pimax, rbins, X1=x1, Y1=y1, Z1=z1,
                 X2=rx, Y2=ry, Z2=rz, verbose=True, output_rpavg=True,
                 boxsize=boxsize, periodic=True)

    D2R = DDrppi(autocorr, nthreads, pimax, rbins, X1=x2, Y1=y2, Z1=z2,
                 X2=rx, Y2=ry, Z2=rz, verbose=True, output_rpavg=True,
                 boxsize=boxsize, periodic=True)
    RR = DDrppi(1, nthreads, pimax, rbins, rx, ry, rz, verbose=True, output_rpavg=True,
                boxsize=boxsize, periodic=True)
    xirppi = convert_3d_counts_to_cf(N1, N2, rand_N, rand_N,
                                     D1D2, D1R, D2R, RR)
    f = open(filename + "xi_sigma_rparllel.txt", "w")
    f.write("#sigma_left\tsigma_right\sigma_avg\tr_parallel\txi_rp_pi\n")
    count = 0
    for xi in xirppi:
        f.write("{0:10.6f}\t{1:10.6f}\t{2:10.6f}\t{3:10.6f}\t{4:10.6f}\n".format(
            rmin_arr[count], rmax_arr[count], rpavg[count], pimax_arr[count], xi))
        count += 1
    f.close()




def main_driver()->None:
    # Read User given parameters
    parser = argparse.ArgumentParser()
    parser.add_argument("sigma", type=float,
                        help='Input scatter e.g 0.25')
    parser.add_argument("redshift", type=int,
                        help='if RSD e.g 1 default is 0')

    
    args = parser.parse_args()
    sigma = args.sigma  # input scatter that is needed
    RSD = False
    if args.RSD==1:
        RSD = True

    frac = 0.45
    mstar = 13.25
    beta = 0.66

    dir_name = '/mnt/data1/sandeep/multidark2_data/'\
    'pipeline_MasterCat_alfa70_Mvir/HI_analysis_sigma_{0:0.2f}'.format(sigma)

    dir_name_ = dir_name + "/CrossCorrelationAnalysis/"
    if not os.path.exists(dir_name_):
        os.mkdir(dir_name_)
    if not RSD:
        dir_name_ = dir_name_ + "/real/"
    else:
        dir_name_ = dir_name_ + "/rsd/"
        
    if not os.path.exists(dir_name_):
        os.mkdir(dir_name_)

    # read halo catalog without HI associated with them
    Halo_Cat_fname = dir_name + '/HaloMockCat_SMDPL_f_{0:0.2f}_Mstar_{1:0.2f}'\
        '_beta_{2:0.2f}_scatter_{3:0.2f}.txt'.format(frac, mstar, beta, sigma)
    HaloCat_df = dt.Frame(Halo_Cat_fname).to_pandas()

    HI_Cat_fname = dir_name + '/HIMockCat_alfa70_f_{0:0.2f}_Mstar_{1:0.2f}'\
        '_beta_{2:0.2f}_scatter_{3:0.2f}.txt'.format(frac, mstar, beta, sigma)

    HICat_df = dt.Frame(HI_Cat_fname).to_pandas()
    
    HaloCat_df = pd.concat([HaloCat_df, HICat_df])
    Halo_cutoff = np.array([11.0, 11.5, 12.0, 12.5, 13.0, 13.5, 14.0, 14.5])
    HI_cutoff = np.array([9.4, 9.6, 9.8, 10.0, 10.2])

    for halo_cut in Halo_cutoff:
        halo_data_df = HaloCat_df.loc[HaloCat_df['log10(MHalo)'] >= halo_cut]
        for hi_cut in HI_cutoff:
            if sigma == 0.0:
                hi_data_df = HICat_df[HICat_df['log10(MHI)'] >= hi_cut]
            else:
                hi_data_df = HICat_df[HICat_df['log10(MHI_scatter)'] >= hi_cut]

            fname = dir_name_ + '/' + f'Halo_ge_{halo_cut}_hi_ge_{hi_cut}_'
            
            # Halo, galaxy autocorrelation and Cross-Correlation
            halo_autocorr, rbin_mid = get_xi_autoCorr(halo_data_df, redshift=RSD)
            gal_autocorr, foo = get_xi_autoCorr(hi_data_df, redshift=RSD)
            cross_corr = get_xi_CrossCorr(halo_data_df, hi_data_df, redshift=RSD)
            
            # Save xi auto and cross info
            f = open(fname + "xiCross_diffrandCat.txt", "w")
            f.write("#rbin_mid\txi_cross\txi_halo\txi_gal\tRatio\n")
            count = 0
            for xi in cross_corr:
                Ratio = xi/np.sqrt(halo_autocorr[count]*gal_autocorr[count])
                f.write("{0:10.6f}\t{1:10.6f}\t{2:10.6f}\t{3:10.6f}\t{4:10.6f}\n".format(rbin_mid[count],
                                                                                         xi, halo_autocorr[count],
                                                                                         gal_autocorr[count], Ratio))
                count += 1
            f.close()
            
            cross_corr = get_xi_CrossCorr(halo_data_df, hi_data_df, redshift=RSD)
            f = open(fname + "xiCross.txt", "w")
            f.write("#rbin_mid\txi_cross\txi_halo\txi_gal\tRatio\n")
            count = 0
            for xi in cross_corr:
                Ratio = xi/np.sqrt(halo_autocorr[count]*gal_autocorr[count])
                f.write("{0:10.6f}\t{1:10.6f}\t{2:10.6f}\t{3:10.6f}\t{4:10.6f}\n".format(rbin_mid[count],
                                                                                         xi, halo_autocorr[count],
                                                                                         gal_autocorr[count], Ratio))
                count += 1
            f.close()
            
            # Compute wp of halo, galaxy auto and cross-coreelation
            halo_wp_autocorr, rbin_mid = get_wp_autoCorr(halo_data_df, redshift=RSD)
            gal_wp_autocorr, foo = get_xi_autoCorr(hi_data_df, redshift=RSD)
            wp_cross_corr = get_wp_CrossCorr(halo_data_df, hi_data_df, redshift=RSD)

            fin = open(fname + "wp.txt", "w")
            fin.write("#rbin\trpavg\twp_cross\twp_halo\twp_hi\n")
            count = 0
            for xi in wp_cross_corr:
                Ratio = xi/np.sqrt(halo_autocorr[count]*gal_autocorr[count])
                fin.write("{0:10.6f}\t{1:10.6f}{2:10.6f}{3:10.6f}\t{4:10.6f}\n".format(rbin_mid[count], xi, 
                                                                                       halo_wp_autocorr[count], 
                                                                                       gal_wp_autocorr[count], Ratio))
        count += 1
    fin.close()





if __name__ == "__main__":
    main_driver()
