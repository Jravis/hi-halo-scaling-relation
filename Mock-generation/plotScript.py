import numpy as np
from astropy.cosmology import LambdaCDM
import os
import argparse
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from collections import OrderedDict
from matplotlib.transforms import blended_transform_factory
from typing import Dict, Optional


mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rc('font', weight='bold')
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Ubuntu'
mpl.rcParams['font.monospace'] = 'Ubuntu Mono'
mpl.rcParams['axes.labelweight'] = 'bold'

mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'
mpl.rcParams['legend.numpoints'] = 1

#set some plotting parameters
plt.style.use("classic")
lnstyles = OrderedDict(
    [('solid',               (0, ())),
     ('loosely dotted',      (0, (1, 10))),
     ('dotted',              (0, (1, 5))),
     ('densely dotted',      (0, (1, 1))),

     ('loosely dashed',      (0, (5, 10))),
     ('dashed',              (0, (5, 5))),
     ('densely dashed',      (0, (5, 1))),

     ('loosely dashdotted',  (0, (3, 10, 1, 10))),
     ('dashdotted',          (0, (3, 5, 1, 5))),
     ('densely dashdotted',  (0, (3, 1, 1, 1))),

     ('loosely dashdotdotted', (0, (3, 10, 1, 10, 1, 10))),
     ('dashdotdotted',         (0, (3, 5, 1, 5, 1, 5))),
     ('densely dashdotdotted', (0, (3, 1, 1, 1, 1, 1)))])



#---------------- Global Values ---------------------------

guo_dir_name='../../HI-Halo_scatter_relation/HI_mock_cat/clust_data/'
dirc ='/mnt/data1/sandeep/my_all_xanadu_codes/my_mesh_npairs/GFastCorr/examples/'
guo_dm_rp = np.genfromtxt(dirc+"wp_guo.txt", usecols=0) 
guo_dm_wp = np.genfromtxt(dirc+"wp_guo.txt", usecols=1) 

Halo_cutoff = np.array([11.0, 11.5, 12.0, 12.5, 13.0, 13.5, 14.0, 14.5])
HI_cutoff = np.array([9.4, 9.6, 9.8, 10.0, 10.2])


def plot_corr(plot_name:str, plot_vars_dict:Dict, plot_wp=False)->None:
    """This module generate a plot for given name
    Args:
        plot_name [str]: Complete path name of the plot we want to save with
        plot_vars [Dict]: Dictionary of variable we want to use to plot
        plot_wp [Optional[bool]]
    Return:
        None
    """

    ravg = plot_vars_dict['rbin_mid']
    halo_cut = plot_vars_dict['halo_cut']
    hi_cut   = plot_vars_dict['hi_cut']
    if not plot_wp:
        halo_autocorr = plot_vars_dict['xi_halo']
        gal_autocorr = plot_vars_dict['xi_gal']
        CrossCorr = plot_vars_dict['xi_cross']
    else:
        halo_autocorr = plot_vars_dict['wp_halo']
        gal_autocorr = plot_vars_dict['wp_gal']
        CrossCorr = plot_vars_dict['wp_cross']


    props = dict(boxstyle='round', facecolor='none', linewidth=1.8)
    fig = plt.figure(1, figsize=(8, 6))
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0])
    ax1.plot(ravg, CrossCorr, dashes=[3,3,10,3] , linewidth=2.0, color='r', label=r'$\rm M_{halo}-M_{HI}$')
    ax1.plot(ravg, halo_autocorr, linestyle='--', linewidth=2.0, color='teal', label=r'$\rm M_{halo}')
    ax1.plot(ravg, gal_autocorr, dashes=[2, 2, 2, 2, 10, 2], color='b', linewidth=2.0, label=r'$\rm M_{HI}$')

    text = r"""
    Mhalo $\ge$ {}
    MHI $\ge${}
    """.format(halo_cut, hi_cut)

    ax1.text(0.5, 10, text, fontsize=18, bbox=props, ha='left', va='bottom') 
    if not plot_wp:
        ax1.set_ylabel(r'$\rm \xi_{r}(r) [h^{-1} Mpc]$', fontsize=22, fontweight='bold')
    else: 
        ax1.plot(ravg, gal_autocorr, dashes=[2, 2, 2, 2, 10, 2], color='k', linewidth=2.0, label=r'$\rm DM$')
        ax1.set_ylabel(r'$\rm w_{p}(r_{p}) [h^{-1} Mpc]$', fontsize=22, fontweight='bold')

    ax1.set_xscale("log")
    ax1.set_yscale("log")
    ax1.set_ylim(1, 1000)
    ax1.set_yticks([1, 10, 100])
    ax1.minorticks_on()
    plt.setp(ax1.get_xticklabels(),visible=False)
    ax1.tick_params(axis='both', which='minor', length=5,
          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.2)
    ax1.spines['left'].set_linewidth(1.2)
    ax1.spines['top'].set_linewidth(1.2)
    ax1.spines['right'].set_linewidth(1.2)

    plt.tight_layout()
    fig.savefig(plot_name + '.png', bbox_inches='tight', dpi=600, edgecolor='none', frameon='False')
    plt.close()
    


def main_driver()->None:
    """_summary_
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("sigma", type=float,
                        help='Input scatter e.g 0.25')
    parser.add_argument("redshift", type=int,
                        help='if RSD e.g 1 default is 0')

    args = parser.parse_args()
    sigma = args.sigma  # input scatter that is needed
    rsd = False

    if args.RSD==1:
        rsd = True

    plot_mode = 'xi' # can be wp
    dir_name = '/mnt/data1/sandeep/multidark2_data/'\
    'pipeline_MasterCat_alfa70_Mvir/HI_analysis_sigma_{0:0.2f}'.format(sigma)

    dir_name_ = dir_name + "/CrossCorrelationAnalysis/"

    if not rsd:
        dir_name_ = dir_name_ + "/real/"
    else:
        dir_name_ = dir_name_ + "/rsd/"
    if not os.path.exists(dir_name_ + 'clust_plots'):
        os.mkdir(dir_name_ + 'clust_plots')

    dir_name_ = dir_name + "/CrossCorrelationAnalysis/clust_plots/"
    PlotVar = dict()
    for halo_cut in Halo_cutoff:
        for hi_cut in HI_cutoff:
            PlotVar['halo_cut'] = halo_cut
            PlotVar['hi_cut'] = hi_cut
            PlotVar['rbin_mid']  = np.genfromtxt(fname + "xiCross_diffrandCat.txt", usecols=0)

            if plot_mode=='xi':
                fname = dir_name_ + '/' + f'xi_Halo_ge_{halo_cut}_hi_ge_{hi_cut}_'
                PlotVar['xi_cross']  = np.genfromtxt(fname + "xiCross_diffrandCat.txt", usecols=1)
                PlotVar['xi_halo']   = np.genfromtxt(fname + "xiCross_diffrandCat.txt", usecols=2)
                PlotVar['xi_gal']  = np.genfromtxt(fname + "xiCross_diffrandCat.txt", usecols=3)
                plot_corr(fname, PlotVar)
            else:
                fname = dir_name_ + '/' + f'wp_Halo_ge_{halo_cut}_hi_ge_{hi_cut}_'
                PlotVar['wp_cross']  = np.genfromtxt(fname + "xiCross_diffrandCat.txt", usecols=1)
                PlotVar['wp_halo']   = np.genfromtxt(fname + "xiCross_diffrandCat.txt", usecols=2)
                PlotVar['wp_gal']  = np.genfromtxt(fname + "xiCross_diffrandCat.txt", usecols=3)
                plot_corr(fname, PlotVar, plot_wp=True)



if __name__ == '__main__':
    main_driver()