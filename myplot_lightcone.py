#################################################################
#                                                               #
# Copyright (c) 2019, Sandeep Rana                              #
# used Alvaro Orsi base code                                    #
#################################################################



from __future__ import print_function, division
import numpy as np
import sys
import time
import matplotlib as mpl
from matplotlib.transforms import Affine2D
import mpl_toolkits.axisartist.floating_axes as floating_axes
import numpy as np
import mpl_toolkits.axisartist.angle_helper as angle_helper
from matplotlib.projections import PolarAxes
from mpl_toolkits.axisartist.grid_finder import (FixedLocator, MaxNLocator,
                                                 DictFormatter)

import matplotlib.pyplot as plt
from matplotlib import pyplot, transforms
from astropy.cosmology import FlatLambdaCDM

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Ubuntu'
mpl.rcParams['font.monospace'] = 'Ubuntu Mono'
mpl.rcParams['axes.labelweight'] = 'bold'

mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'

import palettable

#set some plotting parameters
plt.style.use("classic")


OmegaMatter = 0.3
H_0 = 0.7*100.
cosmo = FlatLambdaCDM(H0=H_0, Om0=OmegaMatter)

#font = {'family' : 'STIXGeneral',

#        'size'   : 18}

#matplotlib.rc('font', **font)

#plt.style.use("classic")


#---------------------------------------------------------------------

def setup_axes3(fig, rect, ra_min, ra_max, czmin, czmax):

    """

    Sometimes, things like axis_direction need to be adjusted.

    """

    # rotate a bit for better orientation
    tr_rotate = Affine2D().translate(-90, 0)

    # scale degree to radians

    tr_scale = Affine2D().scale(np.pi/180., 1.)
    tr = tr_rotate + tr_scale + PolarAxes.PolarTransform()

    grid_locator1 = angle_helper.LocatorHMS(4)
    tick_formatter1 = angle_helper.FormatterHMS()
    grid_locator2 = MaxNLocator(3)

    # Specify theta limits in degrees

    ra0, ra1 = 7.5*15, 16.7*15
    #ra0, ra1 = ra_min, ra_max

    # Specify radial limits
    #cz0, cz1 = 0, 15500
    cz0, cz1 = 0, czmax

    grid_helper = floating_axes.GridHelperCurveLinear(

        tr, extremes=(ra0, ra1, cz0, cz1),
        grid_locator1=grid_locator1,
        grid_locator2=grid_locator2,
        tick_formatter1=tick_formatter1,
        tick_formatter2=None)



    ax1 = floating_axes.FloatingSubplot(fig, rect, grid_helper=grid_helper)

    fig.add_subplot(ax1)



    # adjust axis

    ax1.axis["left"].set_axis_direction("bottom")
    ax1.axis["right"].set_axis_direction("top")



    ax1.axis["bottom"].set_visible(False)

    ax1.axis["top"].set_axis_direction("bottom")

    ax1.axis["top"].toggle(ticklabels=True, label=True)

    ax1.axis["top"].major_ticklabels.set_axis_direction("top")

    ax1.axis["top"].label.set_axis_direction("top")


    ax1.axis["left"].label.set_text(r"cz [km s$^{-1}$]")

    #ax1.axis["top"].label.set_text(r"$\alpha_{1950}$")



    # create a parasite axes whose transData in RA, cz

    aux_ax = ax1.get_aux_axes(tr)

    aux_ax.patch = ax1.patch  # for aux_ax to have a clip path as in ax

    ax1.patch.zorder = 0.9  # but this has a side effect that the patch is

    # drawn twice, and possibly over some other

    # artists. So, we decrease the zorder a bit to

    # prevent this.

    return ax1, aux_ax



def main():

  from astropy import units as u
  from astropy.coordinates import SkyCoord
  from astropy import constants as const


  ra  = np.genfromtxt('/mnt/data2/saili/guovol_alfa70/code1_alfa70.dat', usecols=1)
  dec = np.genfromtxt('/mnt/data2/saili/guovol_alfa70/code1_alfa70.dat', usecols=2)
  z_cmb   = np.genfromtxt('/mnt/data2/saili/guovol_alfa70/code1_alfa70.dat', usecols=12)


  print(z_cmb)    
  from mpl_toolkits.mplot3d import Axes3D  
  from matplotlib.collections import LineCollection



  #c = SkyCoord(ra=ra*u.degree, dec=dec*u.degree) 
  
  fig = plt.figure(1, figsize=(8, 6))

  ax2, aux_ax2 = setup_axes3(fig, 111, np.min(ra), 
                             np.max(ra), np.min(z_cmb), 
                             np.max(z_cmb))

  aux_ax2.scatter(ra, z_cmb, s=1, lw=1, c='b', label=r'$\rm{\alpha.70}$')
#  plt.legend(frameon=False, ncol=1, fontsize=14, loc=1, bbox_to_anchor=(0.8, 1.1) )
  plt.tight_layout()
  plt.savefig("ALFA70_NGC_footprint.png", dpi=600)
  
  plt.show()
  

if __name__ == "__main__":
    main()


