from __future__ import print_function, division
import numpy as np
import sys
import os
import time
from astropy.cosmology import LambdaCDM
import datatable as dt
from Corrfunc.theory.xi import xi as xi_corr
import Corrfunc
from Corrfunc.theory.wp import wp
from Corrfunc.theory import DDrppi
from Corrfunc.utils import convert_rp_pi_counts_to_wp
from astropy.io import ascii
import argparse
from astropy.cosmology import LambdaCDM
import pdb
import tqdm


#---------------- Global Values ---------------------------

np.random.seed(1234)
# Multidark 2 cosmology hmf model
OmegaMatter = 0.307115
OmegaLambda = 0.692885
H0 = 0.6777*100.
OmegaBaryon = 0.048206
new_model   = LambdaCDM(H0 = 0.6777*100, Om0 = OmegaMatter, 
			Tcmb0=2.73, Ob0= OmegaBaryon, Ode0=OmegaLambda)

#-----------------------------------------------------------
nthreads= 32
rmin  = 0.1
rmax  = 25.11886431509582
log_delta_r = 0.2
rbins = 10**np.arange(np.log10(rmin), np.log10(rmax), log_delta_r)
rbin_mid = np.zeros(12)
for i in range(len(rbins)-1):
    rbin_mid[i] = (rbins[i]+rbins[i+1])/2. 

boxsize=400 # h^-1 Mpc
redshift=0.0
pimax = 20.0
def wp_corr(posData):
   
    wp_sample = np.zeros(12)
    x = posData[:, 0]
    y = posData[:, 1]
    z = posData[:, 2]
    weights = np.ones_like(x)
    results = wp(boxsize, pimax, nthreads, rbins, x, y, z, weights=weights, 
                 weight_type='pair_product', verbose=True, output_rpavg=True, isa='fastest')
                 
    count=0
    for r in results:
        wp_sample[count]=r['wp']
        count+=1
    #f.close()
    return wp_sample


def wp_corr_pair(x, y, z, rand_x, rand_y, rand_z):
    """
    """
    wp_sample = np.zeros(12)
    weights = np.ones_like(x)
    autocorr=1
    DD_count = DDrppi(autocorr,nthreads, pimax, rbins, x, y, z, periodic=False)
                 
    autocorr=0
    DR_count = DDrppi(autocorr,nthreads, pimax, rbins, x, y, z, 
                X2=rand_x, Y2=rand_y, Z2=rand_z, periodic=False)
    autocorr=1
    RR_count = DDrppi(autocorr,nthreads, pimax, rbins, rand_x, rand_y, rand_z, periodic=False)
    N = len(x)
    wpp = convert_rp_pi_counts_to_wp(N, N, 5*N, 5*N, DD_count, DR_count, DR_count, RR_count, 12, pimax)

    return wpp

#---------------- Global Values ---------------------------

guo_dir_name='../../HI-Halo_scatter_relation/HI_mock_cat/clust_data/'
cutoffList = np.array([9.4, 9.6, 9.8, 10.0, 10.2])
cutoff_name = np.array(['9pt4', '9pt6', '9pt8', '10pt0', '10pt2'])
sigmaList = [0.00, 0.15, 0.20, 0.25]  

delta_sample = boxsize/2.0
Jackknife_sample = np.arange(0, boxsize, delta_sample) 
Nsample_corrd = []
for i in Jackknife_sample:
    for j in Jackknife_sample:
        for k in Jackknife_sample:
            Nsample_corrd.append([i, j, k])

delta_sample = 180.0

for i in range(5):
    for j in range(4):
        print('Running for gt_%s_sigma_%0.2f'%(cutoff_name[i], sigmaList[j]))
        
        fname ="real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], sigmaList[j])
        X = np.genfromtxt('../'+fname, usecols=0, skip_header=1)
        inp_data = np.zeros((len(X), 3), dtype=np.float64)

        inp_data[:, 0] = X
        inp_data[:, 1] = np.genfromtxt('../'+fname, usecols=1, skip_header=1)
        inp_data[:, 2] = np.genfromtxt('../'+fname, usecols=2, skip_header=1)
        
        wp_parent = wp_corr(inp_data)

        random_data = np.zeros((5*len(X), 3), dtype=np.float64)
        random_data[:, 0] = np.random.uniform(0, boxsize, len(X)*5)
        random_data[:, 1] = np.random.uniform(0, boxsize, len(X)*5)
        random_data[:, 2] = np.random.uniform(0, boxsize, len(X)*5)
            
        Jack_wp_corr = np.zeros((len(Nsample_corrd), len(rbin_mid)), dtype=np.float64)
        Jack_wp_corr_log = np.zeros((len(Nsample_corrd), len(rbin_mid)), dtype=np.float64)
        
        dir_name = "./Subsample_2Cube_180Mpchinv/Jack_smaple_corr_gt_%s_sigma_%0.2f"%(cutoff_name[i], sigmaList[j])
        if not os.path.exists(dir_name):
            os.mkdir(dir_name)
        
        pos_dir_name = "./Subsample_2Cube_180Mpchinv/corrd"
        if not os.path.exists(pos_dir_name):
            os.mkdir(pos_dir_name)

        #for nn in tqdm.tqdm(range(len(Nsample_corrd)), position=0, leave=True):
        for nn in range(len(Nsample_corrd)):
            
            exclude_sample = Nsample_corrd[nn]
            indx = ( (exclude_sample[0]+delta_sample) >=inp_data[:, 0]) & ( exclude_sample[0] <inp_data[:, 0])
            indy = ( (exclude_sample[1]+delta_sample) >=inp_data[:, 1]) & ( exclude_sample[1] <inp_data[:, 1])
            indz = ( (exclude_sample[2]+delta_sample) >=inp_data[:, 2]) & ( exclude_sample[2] <inp_data[:, 2])
            
            index = (indx & indy & indz)
            sample_data = inp_data[index, :]
            x_sample = sample_data[:, 0]
            y_sample = sample_data[:, 1]
            z_sample = sample_data[:, 2]
            
            indx = ( (exclude_sample[0]+delta_sample) >=random_data[:, 0]) & ( exclude_sample[0] <random_data[:, 0])
            indy = ( (exclude_sample[1]+delta_sample) >=random_data[:, 1]) & ( exclude_sample[1] <random_data[:, 1])
            indz = ( (exclude_sample[2]+delta_sample) >=random_data[:, 2]) & ( exclude_sample[2] <random_data[:, 2])
            
            index = (indx & indy & indz)
            sample_data = random_data[index, :]
            x_random = sample_data[:, 0]
            y_random = sample_data[:, 1]
            z_random = sample_data[:, 2]
            
            if cutoff_name[i]=='9pt8':
                fname1 ="./Subsample_2Cube_180Mpchinv/corrd/pos_smaple_corr_gt_%s_sigma_%0.2f_%d.txt"%(cutoff_name[i], sigmaList[j], nn)
                table = [x_sample, y_sample, z_sample]             
                names = ["#x", "y", 'z']
                Formats = {'#x':'%2.14f', 'y':'%2.14f', 'z':'%2.14f'}
                ascii.write(table, fname1, names=names, delimiter='\t', formats=Formats, overwrite=True)



            test_wp =  wp_corr_pair(x_sample, y_sample, z_sample, x_random, y_random, z_random)
            Jack_wp_corr[nn, :] =  np.nan_to_num(test_wp)
            test_wp_ = np.log10(test_wp)
            Jack_wp_corr_log[nn, :] =  np.nan_to_num(test_wp_)

            fname1 ="./Subsample_2Cube_180Mpchinv/Jack_smaple_corr_gt_%s_sigma_%0.2f/wp_corr_%d.txt"%(cutoff_name[i], sigmaList[j], nn)
            table = [rbin_mid, Jack_wp_corr[nn, :]]             
            names = ["#rbin", "wp_jack"]
            Formats = {'#rbin':'%2.14f', 'wp_jack':'%2.14f'}
            ascii.write(table, fname1, names=names, delimiter='\t', formats=Formats, overwrite=True)

        mean_wp = np.mean(Jack_wp_corr, axis=0)
        err     = np.std(Jack_wp_corr, axis=0)
        
        mean_wp_log = np.mean(Jack_wp_corr_log, axis=0)
        err_log     = np.std(Jack_wp_corr_log, axis=0)
        
        fname1 ="./Subsample_2Cube_180Mpchinv/Jack_wp_real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], sigmaList[j])
        table = [rbin_mid, mean_wp, mean_wp_log, np.log10(wp_parent), err, err_log]             
        names = ["#rbin", "wp_jack", "log10(wp_jack)", 'log10(wp_parent)', "err", "log10(err)"]
        Formats = {'#rbin':'%2.14f', 'wp_jack':'%2.14f','log10(wp_jack)':'%2.14f' ,'log10(wp_parent)':'%2.14f', 'err':'%2.14f', 'log10(err)':'%2.14f'}
        ascii.write(table, fname1, names=names, delimiter='\t', formats=Formats, overwrite=True)


    







