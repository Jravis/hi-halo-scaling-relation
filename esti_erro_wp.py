#####################################################
#Copyright (c) 2019, Sandeep Rana                   #
# This routine needs Python 3                       #
# This routine is to compute the effect of          #
# scatter into HI-halo abundace matching relation   #
# Following Behroozi 2010 and Ren 2019.             #
#This routine is for creating mock and computing    #
#clustering. Here we explore paramter space         #
#and it's effect on HI clustering with and without  #
#the scatter.                                       #
                                                    #                                                   
#####################################################

"""
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""




from __future__ import print_function, division
import numpy as np
import sys
import time
from scipy import integrate
from scipy.interpolate import InterpolatedUnivariateSpline as _spline
from astropy.cosmology import LambdaCDM
import argparse
from Corrfunc.theory.xi import xi as xi_corr
import Corrfunc
from Corrfunc.theory.wp import wp
from astropy.io import ascii
import palettable
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec


mpl.rcParams['figure.figsize'] = (8,6)
mpl.rcParams['font.size'] = 18
mpl.rc('text', usetex=True)
mpl.rc('font', weight='bold')
mpl.rcParams['mathtext.fontset'] = 'custom'
mpl.rcParams['mathtext.rm'] = 'Bitstream Vera Sans'
mpl.rcParams['mathtext.it'] = 'Bitstream Vera Sans:italic'
mpl.rcParams['mathtext.bf'] = 'Bitstream Vera Sans:bold'

mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Ubuntu'
mpl.rcParams['font.monospace'] = 'Ubuntu Mono'
mpl.rcParams['axes.labelweight'] = 'bold'

mpl.rcParams['xtick.direction']='in'
mpl.rcParams['ytick.direction']='in'
mpl.rcParams['legend.numpoints'] = 1

#set some plotting parameters
plt.style.use("classic")


#---------------- Global Values ---------------------------

# Multidark 2 cosmology hmf model
OmegaMatter = 0.307115
OmegaLambda = 0.692885
H0 = 0.6777*100.
OmegaBaryon = 0.048206
new_model   = LambdaCDM(H0 = 0.6777*100, Om0 = OmegaMatter, 
			Tcmb0=2.73, Ob0= OmegaBaryon, Ode0=OmegaLambda)

#-----------------------------------------------------------


def wp_corr(x, y, z, mHI_mock, vz, sigma, fname):
   
    cutoff      = np.array([9.4, 9.6, 9.8, 10.0, 10.2])
    cutoff_name = np.array(['9pt4', '9pt6', '9pt8', '10pt0', '10pt2'])
    nthreads= 32
    rmin  = 0.1
    rmax  = 25.11886431509582
    log_delta_r = 0.2
    rbins = 10**np.arange(np.log10(rmin), np.log10(rmax), log_delta_r)

    rbin_mid = np.zeros(12)
    for i in range(len(rbins)-1):
        rbin_mid[i] = (rbins[i]+rbins[i+1])/2. 

    boxsize=400 # h^-1 Mpc
    redshift=0.0
    H_z = new_model.H(redshift).value
    HubbleParam=0.6777
    #SMDPL is in uniths of Mpch^-1
    boost_factor = np.sqrt(1.+ redshift)*1./H_z
    pimax = 20.0
    
    for i in range(len(cutoff)): 
        keep = (mHI_mock>cutoff[i])
        x1 = x[keep]
        y1 = y[keep]
        z1 = z[keep]
        #z_rsd = z[keep]+vz[keep]*boost_factor*HubbleParam
        #z_rsd= z_rsd%boxsize
        weights = np.ones_like(x1)   


        fname1 ="../pipeline_result_data_alfa70/clust_data/real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], sigma)
        table = [x1, y1, z1]             
        names = ['X', 'Y', 'Z']        
        Formats = {'X':'%2.14f', 'Y':'%2.14f', 'Z':'%2.14f'}
        ascii.write(table, fname1, names=names, delimiter='\t', formats=Formats, overwrite=True)

    #    results = wp(boxsize, pimax, nthreads, rbins, x1, y1, z_rsd, weights=weights, 
        results = wp(boxsize, pimax, nthreads, rbins, x1, y1, z1, weights=weights, 
                     weight_type='pair_product', verbose=True, output_rpavg=True)
                     
        f = open(fname+"real_gt_%s_sigma_%0.2f_SMDPL.txt"%(cutoff_name[i], sigma),"w")
        f.write("#rbin\trpavg\twp\tnpairs\tweightavg\n")
        count=0
        for r in results:
            f.write("{0:10.6f}\t{1:10.6f}\t{2:10.6f}\t{3:10d}\t{4:10.6f}\n".
            format(rbin_mid[count], r['rpavg'], r['wp'], r['npairs'], r['weightavg']))
            count+=1
        f.close()


